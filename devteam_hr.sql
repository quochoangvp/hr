-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2016 at 02:03 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.5.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devteam_hr`
--
CREATE DATABASE IF NOT EXISTS `devteam_hr` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `devteam_hr`;

-- --------------------------------------------------------

--
-- Table structure for table `cv`
--

DROP TABLE IF EXISTS `cv`;
CREATE TABLE `cv` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `jobcat_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `countview` int(11) NOT NULL,
  `attachments` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `cv`
--

TRUNCATE TABLE `cv`;
--
-- Dumping data for table `cv`
--

INSERT INTO `cv` (`id`, `employee_id`, `title`, `body`, `jobcat_id`, `created_at`, `updated_at`, `countview`, `attachments`) VALUES
(1, 2, 'Nhân Viên Kinh Doanh', '<p>- Cấp bậc hiện tại: Nh&acirc;n vi&ecirc;n<br />- Cấp bậc mong muốn: Nh&acirc;n vi&ecirc;n<br />- Nơi l&agrave;m việc: Hồ Ch&iacute; Minh, B&igrave;nh Dương<br />- Số năm kinh nghiệm: 2 năm<br />- Tr&igrave;nh độ cao nhất: Cao đẳng<br />- Mức lương mong muốn: 5-7 triệu<br />- Nhu cầu l&agrave;m việc: Đang t&igrave;m việc v&agrave; sẵn s&agrave;ng cho c&ocirc;ng việc mới</p>', 6, '2016-04-17 05:17:14', '2016-04-17 05:17:57', 7, '292992393.docx');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `fullname` varchar(40) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthday` date NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `employee`
--

TRUNCATE TABLE `employee`;
--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `fullname`, `gender`, `birthday`, `email`, `password`, `phone`, `address`) VALUES
(1, 'Nguyễn Văn B', 1, '1990-04-10', 'nguyenvanb@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '0987654321', 'Việt Nam'),
(2, 'Nguyễn Văn A', 1, '1991-04-16', 'nguyenvana@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '0987654322', 'Việt Nam'),
(3, 'Vũ Thị A', 2, '1992-03-20', 'vuthua@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '0987654213', 'Việt Nam'),
(4, 'zzzzz', 1, '0000-00-00', 'zzzz@gmail.com', '3adb41e5530b9c55452dd9bfb4d5bb0b', '09876543', '76543'),
(5, 'aaaa', 1, '2016-04-12', 'aaaa@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1325295', '4fufhuh'),
(6, 'Abc', 2, '2016-05-22', 'abc@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '98765432', '765sfsfj');

-- --------------------------------------------------------

--
-- Table structure for table `employer`
--

DROP TABLE IF EXISTS `employer`;
CREATE TABLE `employer` (
  `id` int(11) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `coname` varchar(120) NOT NULL,
  `cologo` varchar(100) NOT NULL,
  `codetails` text NOT NULL,
  `coaddress` varchar(255) NOT NULL,
  `cowebsite` varchar(40) NOT NULL,
  `cocontact` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `employer`
--

TRUNCATE TABLE `employer`;
--
-- Dumping data for table `employer`
--

INSERT INTO `employer` (`id`, `email`, `password`, `coname`, `cologo`, `codetails`, `coaddress`, `cowebsite`, `cocontact`) VALUES
(1, 'contact@ansinhmed.com', 'e10adc3949ba59abbe56e057f20f883e', 'Công Ty Cổ Phần Đầu Tư và Phát Triển Y tTế An Sinh', '570cc0c2adc41_1460453570.png', 'Kể từ khi ra đời 2010, với sự nỗ lực không ngừng để đem đến khách hàng những sản phẩm chất lượng, đặc biệt là dịch vụ kỹ thuật uy tín. Đến nay, An Sinh đã có hàng nghìn khách hàng trải rộng khắp các địa phương ở khu vực Miền trung. Ngoài ra An Sinh còn phát triển thành công ở các thị trường như Hà Nội, Thái Bình, Nam Định, Hưng Yên...\r\n\r\nAn Sinh có 3 thế mạnh muốn được chia sẻ để khách hàng biết tới\r\n1- Năng lực triển khai toàn diện khi xây dựng cơ sở mới:\r\nAn Sinh Có thể cung cấp trọn gói 100% thiết bị và vật tư với giá cạnh tranh, khi xây dựng mới hoặc phục vụ hoạt động cho các cơ sở quy mô từ nhỏ đến lớn.\r\n- Chúng tôi đã trúng thầu và thực hiện thành công nhiều dự án cung ứng từ 80 - 100% tất cả thiết bị và vật tư để xây dựng mới các phòng khám đa khoa hoặc bệnh viện quy mô lớn.\r\n- Với các thiết bị: máy CT-Scanner, máy X-Q, máy nội soi, máy xét nghiệm...từ cao cấp đến cơ bản\r\n- Ngoài ra An Sinh có thể giúp khách hàng tham khảo và tư vấn để tối ưu hiệu quả khi đầu tư xây dựng và phát triển các bệnh viện, cơ sở y tế tư nhân\r\n\r\n2- Dịch vụ kỹ thuật chất lượng: \r\nVới đội ngũ kỹ sư được đào tạo bài bản, chuyên nghiệp và nhiều kinh nghiệm. TRUNG TÂM DỊCH VỤ KỸ THUẬT THIẾT BỊ Y TẾ AN SINH cam kết đáp ứng hầu hết các yêu cầu về sữa chữa và bảo dưỡng thiết bị cho các cơ sở y tế.\r\n​Ngoài việc đáp ứng về dịch vụ cho các trang thiết bị do Cty lắp đặt, An Sinh thường xuyên sữa chữa và bảo dưỡng các thiết bị cao cấp và phức tạp của đơn vị như: máy MRI, CT-Scanner, máy thở, X-Quang số, siêu âm... ( Click: Xem thêm các dịch vụ kỹ thuật của An Sinh )\r\n\r\n3- Hàng hóa phong phú cung ứng thường xuyên:\r\nAn Sinh sẵn có hầu hết các hóa chất, vật tư, dụng cụ...thường dùng trong hoạt động hàng ngày của các cở sở y tế.\r\nNgoài ra, với các mặt hàng chuyên dụng như: dụng cụ phẫu thuật mở chuyên sâu, dụng cụ phẫu thuật nội soi, can thiệp, ​vật liệu cấy ghép - thay thế...chúng tôi cũng đáp ứng tốt theo nhu cầu của đơn vị\r\n​ ​- Với tư nhân: ​\r\nHiện tại, An Sinh cũng đang cung ứng gần 100% vật tư cho các đơn vị như: Bv Tâm Đức, BV Phúc Thịnh, BV Trí Đức và hầu hết các phòng khám tư nhân trên địa bàn Thanh Hóa.\r\n​- Trong công lập: An Sinh đã cung ứng cho hẫu hết các cơ sở y tế từ tuyến tỉnh, tuyến huyện ở khu vực Miền Trung. Đặc biệt là Thanh Hóa.\r\n\r\nAn Sinh xin chân thành cảm ơn sự quan tâm và ủng hộ của quý khách trong suốt thời gian qua!\r\nTin ở chúng tôi như bạn muốn khách hàng tin mình.\r\nTrăm lời nói hay của nhà cung cấp không bằng một lời nói thật của khách hàng.', '296 Hải Thượng Lãn Ông - P. Đông Vệ - Tp. Thanh Hóa', 'http://ansinhmed.com/', '0378888296'),
(2, 'info@maylocnuocpacific.vn', 'e10adc3949ba59abbe56e057f20f883e', 'Công ty TNHH thiết bị và công nghệ TEKCOM', '', 'TEKCOM là một trong số các nhà nhập khẩu và phân phối các sản phẩm máy lọc nước và linh phụ kiện thay thế của những tập đoàn lớn đến từ Mỹ, Malaysia, Đài Loan. Công ty chúng tôi không ngừng nghiên cứu, phát triển các sản phẩm dịch vụ chất lượng cao, mang lại sự hài lòng vượt lên cả sự mong muốn của khách hàng, bên cạnh những nỗ lực nhằm tạo ra một sản phẩm khác biệt về chất lượng, dịch vụ, chúng tôi luôn chú trọng quan tâm đến sự phát triển của các thành viên trong Công ty, Nhà phân phối, các đại lý, cộng tác viên, đặc biệt là quý khách hàng. \r\n\r\nTekcom với trách nhiệm mang tới cho khách hàng sản phẩm máy lọc nước RO, tư vấn giải pháp và chuyển giao công nghệ hệ thống lọc, xử lý nước công nghiệp cao dân dụng và công nghiệp, đảm bảo chất lượng đáp ứng tối đa yêu cầu xử lý nguồn nước của quý khách hàng. Đó cũng là lời cam kết về chất lượng mà Tekcom đang làm, sẽ làm. \r\n\r\nTEKCOM luôn theo đuổi sự hoàn thiện về công nghệ thông qua những thay đổi của môi trường, nguồn nước tại Việt Nam, mà chúng tôi có được, từ đó nghiên cứu tìm tòi sản phẩm mới phù hợp hơn đáp ứng yêu cầu khắt khe của khách hàng, \r\n\r\nTEKCOM luôn luôn đặt mình vào vị trí của quý khách hàng để tư vấn giải pháp, phát triển công nghệ tiên tiến giúp khách hàng tiếp cận, thấu hiểu công nghệ dựa trên khả năng của đơn vị mình từ đó vận dụng tối đa nguồn lực sẵn có duy trì và phát triển lợi ích của công nghệ mà chúng tôi mang lại. \r\nHiện nay để mở rộng hoạt động sản xuất kinh doanh, Chúng tôi đang có nhu cầu tuyển dụng các vị trí, vị trí kinh doanh sản phẩm máy lọc nước, Tekcom là môi trường làm việc lý tưởng cho những ai muốn phát triển nghành nghề và khả năng của mình, Công ty chúng tôi luôn chào đón các bạn về làm việc.', 'Số 2 Thạch Cầu, Long Biên, Hà Nội', 'maylocnuocpacific.vn', '0915754075'),
(3, 'contact@datvangkinhdo.vn', '24fe6cad8161a91938dae62ec9cda252', 'Công Ty CP Tư Vấn Và Đầu Tư Đất Vàng Kinh Đô', '54116025be306_1410424869.png', 'Công ty CP Tư Vấn & Đầu Tư Đất Vàng Kinh Đô là công ty chuyên hoạt động trong lĩnh vực đầu tư xây dựng, kinh doanh BĐS, Tài chính, Nhà hàng....Với đội ngũ nhân viên chuyên nghiệp, tận tình trong công việc. \r\n\r\nCông ty chúng tôi luôn cố gắng tạo ra một môi trường làm việc năng động, chuyên nghiệp với định hướng xây dưng thương hiệu mạnh, uy tín mang tính chất lâu dài. \r\nChúng tôi đã và đang tiến hành đầu tư xây dựng thành công một số dự án chung cư mini, chung cư thương mại, cho thuê văn phòng cao cấp trên địa bàn các quận Hoàng Mai, Hai Bà Trưng, Cầu Giấy, Từ Liêm, Phát triển chuỗi nhà hàng tại Hai Bà Trưng, Kinh doanh tài chính, đáo nợ... \r\n\r\nVới định hướng tạo dựng một doanh nghiệp có thương hiệu mạnh, uy tín, chuyên nghiệp chúng tôi đang rất cần sự đóng góp sức lực, tài lực của các bạn trẻ có hoài bão.', 'Tầng 2, Tòa nhà Lũng Lô, 209 Trung Kính Cầu Giấy, TP.HN', 'datvangkinhdo.vn', '043296333'),
(4, 'zzzz@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'zzz.zzz', 'V-Rap.png', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>zz z zz zzz zzzz z z</p>\r\n</body>\r\n</html>', 'zzz.zzz zzz', 'zzz.com', 'zzz zz zzz'),
(5, 'z@a.x', 'fbade9e36a3f36d3d676c1b808451dd7', 'z', 'V-Rap.png', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>', 'zzz.zzz zzz', 'zzz.com', 'z'),
(6, 'z@a.x', 'fbade9e36a3f36d3d676c1b808451dd7', 'z', 'V-Rap.png', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>', 'zzz.zzz zzz', 'zzz.com', 'z'),
(7, 'zzzz@gmail.com', 'f3abb86bd34cf4d52698f14c0da1dc60', 'z', 'V-Rap.png', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>z</p>\r\n</body>\r\n</html>', 'z', 'z', 'z'),
(8, 'z@a.com', '02c425157ecd32f259548b33402ff6d3', 'zzz', 'V-Rap.png', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>xzzzz</p>\r\n</body>\r\n</html>', 'zzzz', 'zz', 'zz'),
(9, 'z@a.comz', 'f3abb86bd34cf4d52698f14c0da1dc60', 'zzza', 'V-Rap.png', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>zz</p>\r\n</body>\r\n</html>', 'zzz.zzz zzz', 'zzzz', 'zzz');

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
CREATE TABLE `job` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `jobcat_id` int(11) NOT NULL,
  `deadline` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `apply_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `job`
--

TRUNCATE TABLE `job`;
--
-- Dumping data for table `job`
--

INSERT INTO `job` (`id`, `employer_id`, `title`, `body`, `jobcat_id`, `deadline`, `created_at`, `updated_at`, `apply_num`) VALUES
(1, 3, 'Cửa Hàng Trưởng Kiêm Phiên Dịch', '&lt;!DOCTYPE html&gt;\r\n&lt;html&gt;\r\n&lt;head&gt;\r\n&lt;/head&gt;\r\n&lt;body&gt;\r\n&lt;div class=&quot;col-xs-4 offset20 push-right-20&quot;&gt;\r\n&lt;ul class=&quot;no-style&quot;&gt;\r\n&lt;li class=&quot;m-b-5&quot;&gt;&lt;strong&gt;Mức lương:&lt;/strong&gt; 12-15 triệu&lt;/li&gt;\r\n&lt;li class=&quot;m-b-5&quot;&gt;&lt;strong&gt;Kinh nghiệm:&lt;/strong&gt; 2 năm&lt;/li&gt;\r\n&lt;li class=&quot;m-b-5&quot;&gt;&lt;strong&gt;Tr&amp;igrave;nh độ:&lt;/strong&gt; Cao đẳng&lt;/li&gt;\r\n&lt;li class=&quot;m-b-5&quot;&gt;&lt;strong&gt;Tỉnh/Th&amp;agrave;nh phố:&lt;/strong&gt; &lt;a class=&quot;text-primary&quot; title=&quot;Việc l&amp;agrave;m H&amp;agrave; Nội&quot; href=&quot;http://www.timviecnhanh.com/viec-lam-ha-noi-2.html&quot;&gt;Việc l&amp;agrave;m H&amp;agrave; Nội&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;m-b-5&quot;&gt;&lt;strong&gt;Ng&amp;agrave;nh nghề:&lt;/strong&gt; &lt;a class=&quot;text-primary&quot; title=&quot;B&amp;aacute;n h&amp;agrave;ng&quot; href=&quot;http://www.timviecnhanh.com/viec-lam-ban-hang-c10.html&quot;&gt;B&amp;aacute;n h&amp;agrave;ng&lt;/a&gt;, &lt;a class=&quot;text-primary&quot; title=&quot;Bi&amp;ecirc;n dịch/Phi&amp;ecirc;n dịch&quot; href=&quot;http://www.timviecnhanh.com/viec-lam-bien-dich-phien-dich-c14.html&quot;&gt;Bi&amp;ecirc;n dịch/Phi&amp;ecirc;n dịch&lt;/a&gt;, &lt;a class=&quot;text-primary&quot; title=&quot;Quản l&amp;yacute; điều h&amp;agrave;nh&quot; href=&quot;http://www.timviecnhanh.com/viec-lam-quan-ly-dieu-hanh-c44.html&quot;&gt;Quản l&amp;yacute; điều h&amp;agrave;nh&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col-xs-4 offset20&quot;&gt;\r\n&lt;ul class=&quot;no-style&quot;&gt;\r\n&lt;li class=&quot;m-b-5&quot;&gt;&lt;strong&gt;Số lượng tuyển dụng:&lt;/strong&gt; 2&lt;/li&gt;\r\n&lt;li class=&quot;m-b-5&quot;&gt;&lt;strong&gt;Giới t&amp;iacute;nh:&lt;/strong&gt; Kh&amp;ocirc;ng y&amp;ecirc;u cầu&lt;/li&gt;\r\n&lt;li class=&quot;m-b-5&quot;&gt;&lt;strong&gt;T&amp;iacute;nh chất c&amp;ocirc;ng việc:&lt;/strong&gt; Giờ h&amp;agrave;nh ch&amp;iacute;nh&lt;/li&gt;\r\n&lt;li class=&quot;m-b-5&quot;&gt;&lt;strong&gt;H&amp;igrave;nh thức l&amp;agrave;m việc:&lt;/strong&gt; Nh&amp;acirc;n vi&amp;ecirc;n ch&amp;iacute;nh thức&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;Phi&amp;ecirc;n dịch tiếng Phồn thể, tiếng Đ&amp;agrave;i Loan sang tiếng Việt v&amp;agrave; ngược lại khi cần thiết&lt;br /&gt;- X&amp;acirc;y dựng, tổ chức, thực hiện c&amp;aacute;c chiến lược, kế hoạch kinh doanh của nh&amp;agrave; h&amp;agrave;ng theo mục ti&amp;ecirc;u kinh doanh ngắn hạn, trung hạn, d&amp;agrave;i hạn;&lt;br /&gt;- Điều h&amp;agrave;nh, phối hợp nh&amp;acirc;n sự đảm bảo sự vận h&amp;agrave;nh ho&amp;agrave;n hảo trong nh&amp;agrave; h&amp;agrave;ng. Hỗ trợ c&amp;aacute;c bộ phận, khu vực trong nh&amp;agrave; h&amp;agrave;ng;&lt;br /&gt;- Duy tr&amp;igrave;, kiểm so&amp;aacute;t ti&amp;ecirc;u chuẩn về chất lượng, vệ sinh an to&amp;agrave;n thực phẩm, PCCC.. theo đ&amp;uacute;ng ti&amp;ecirc;u chuẩn của nh&amp;agrave; h&amp;agrave;ng đề ra;&lt;br /&gt;- X&amp;acirc;y dựng c&amp;aacute;c chương tr&amp;igrave;nh khuyến mại theo từng thời điểm trong năm để th&amp;uacute;c đẩy doanh thu nh&amp;agrave; h&amp;agrave;ng;&lt;br /&gt;- Giải quyết c&amp;aacute;c c&amp;aacute;c thắc mắc, ph&amp;agrave;n n&amp;agrave;n của kh&amp;aacute;ch h&amp;agrave;ng;&lt;br /&gt;- L&amp;ecirc;n lịch l&amp;agrave;m việc v&amp;agrave; ph&amp;acirc;n c&amp;ocirc;ng c&amp;ocirc;ng việc, thời gian l&amp;agrave;m việc cho to&amp;agrave;n bộ nh&amp;acirc;n vi&amp;ecirc;n nh&amp;agrave; h&amp;agrave;ng v&amp;agrave; gi&amp;aacute;m s&amp;aacute;t việc thực hiện đ&amp;uacute;ng;&lt;br /&gt;- Tuyển chọn, đ&amp;agrave;o tạo v&amp;agrave; ph&amp;aacute;t triển đội ngũ nh&amp;acirc;n vi&amp;ecirc;n phục vụ kh&amp;aacute;ch h&amp;agrave;ng một c&amp;aacute;ch chuy&amp;ecirc;n nghiệp. &lt;br /&gt;- Kiểm so&amp;aacute;t mức độ tồn kho v&amp;agrave; c&amp;aacute;c order h&amp;agrave;ng của c&amp;aacute;c bộ phận trong nh&amp;agrave; h&amp;agrave;ng.&lt;br /&gt;- Kiểm so&amp;aacute;t, gi&amp;aacute;m s&amp;aacute;t v&amp;agrave; thực hiện chỉ ti&amp;ecirc;u doanh số của nh&amp;agrave; h&amp;agrave;ng.&lt;br /&gt;- B&amp;aacute;o c&amp;aacute;o cuối ca, tuần, th&amp;aacute;ng bao gồm c&amp;aacute;c nội dung: Nh&amp;acirc;n sự, thực phẩm, doanh thu, hoạt động...&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/body&gt;\r\n&lt;/html&gt;', 6, '2016-05-31', '2016-05-03 14:03:55', '2016-05-03 14:03:55', 3),
(2, 3, 'Nhân Viên Kinh Doanh Bất Động Sản Không Yêu Cầu Ki', '&lt;!DOCTYPE html&gt;\r\n&lt;html&gt;\r\n&lt;head&gt;\r\n&lt;/head&gt;\r\n&lt;body&gt;\r\n&lt;table&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;width-13&quot;&gt;&lt;strong&gt;M&amp;ocirc; tả&lt;/strong&gt;&lt;/td&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;+ Bạn được ho&amp;agrave;n to&amp;agrave;n l&amp;agrave;m chủ c&amp;ocirc;ng việc của m&amp;igrave;nh.&lt;br /&gt;+ Tư vấn b&amp;aacute;n h&amp;agrave;ng ĐỘC QUYỀN DO CH&amp;Iacute;NH C&amp;Ocirc;NG TY X&amp;Acirc;Y DỰNG. &lt;br /&gt;+ Tự chọn b&amp;aacute;n Bất Động Sản m&amp;agrave; m&amp;igrave;nh y&amp;ecirc;u th&amp;iacute;ch&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;width-13&quot;&gt;&lt;strong&gt;Y&amp;ecirc;u cầu&lt;/strong&gt;&lt;/td&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Để c&amp;oacute; thể l&amp;agrave;m việc n&amp;agrave;y, anh chị cần c&amp;oacute;: &lt;br /&gt;- SỨC KHỎE + TR&amp;Iacute; TUỆ+ C&amp;Ocirc;NG CỤ L&amp;Agrave;M VIỆC (xe m&amp;aacute;y, laptop, điện thoại).&lt;br /&gt;- Tốt nghiệp từ Trung cấp, Cao đẳng, Đại học, Thạc sĩ, Tiến sĩ.&lt;br /&gt;- L&amp;agrave;m việc to&amp;agrave;n thời gian với Bất động sản.&lt;br /&gt;- X&amp;aacute;c định r&amp;otilde; sự nghiệp l&amp;agrave; ti&amp;ecirc;n quyết cho cuộc đời.&lt;br /&gt;Y&amp;Ecirc;U CẦU KH&amp;Aacute;C Ch&amp;uacute;ng t&amp;ocirc;i chỉ tuyển th&amp;ecirc;m đ&amp;uacute;ng 10 nh&amp;acirc;n sự chuy&amp;ecirc;n nghiệp b&amp;aacute;n h&amp;agrave;ng độc quyền của c&amp;ocirc;ng ty. Một m&amp;ocirc;i trường Bất Động Sản chuy&amp;ecirc;n nghiệp, đo&amp;agrave;n kết l&amp;agrave; sức mạnh, gắn b&amp;oacute; như gia đ&amp;igrave;nh, &amp;iacute;t cạnh tranh v&amp;agrave; quan trọng Ban L&amp;atilde;nh Đạo sẽ đ&amp;agrave;o tạo bạn đến khi bạn c&amp;oacute; kết quả.&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;width-13&quot;&gt;&lt;strong&gt;Quyền lợi&lt;/strong&gt;&lt;/td&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;- Lương cố định từ 2-4 triệu/th&amp;aacute;ng&lt;br /&gt;- Thưởng, hoa hồng cao, (thu nhập c&amp;oacute; thể l&amp;ecirc;n đến từ 10 - 50 triệu)&lt;br /&gt;- Du lịch 1 năm 2 lần, được đ&amp;oacute;ng Bảo hiểm, được tổ chức sinh nhật,&amp;hellip;&lt;br /&gt;- Được tiếp cận v&amp;agrave; l&amp;agrave;m việc với quy tr&amp;igrave;nh m&amp;ocirc;i giới Bất động sản theo chuẩn Thế giới.&lt;br /&gt;- Cơ hội bứt ph&amp;aacute; trong sự nghiệp.&lt;br /&gt;Cơ hội gi&amp;agrave;u c&amp;oacute;.&lt;br /&gt;+ Cơ hội giao lưu.&lt;br /&gt;+ Cơ hội ho&amp;agrave;n thi&amp;ecirc;n bản th&amp;acirc;n.&lt;br /&gt;+ Cơ hội ph&amp;aacute;t huy khả năng v&amp;agrave; tiến nhanh tới cấp quản l&amp;yacute;. &lt;br /&gt;+ Được ph&amp;ograve;ng marketing c&amp;ocirc;ng ty hỗ trợ t&amp;igrave;m kiếm kh&amp;aacute;ch h&amp;agrave;ng&lt;br /&gt;+ Cầm tay chỉ việc 100% đến khi c&amp;oacute; kết quả doanh thu doanh số&lt;br /&gt;+ Được tiến nhanh tới cấp Quản l&amp;yacute; trong C&amp;ocirc;ng ty.&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/body&gt;\r\n&lt;/html&gt;', 6, '2016-05-28', '2016-05-03 14:03:06', '2016-05-03 14:03:06', 0),
(3, 1, 'Kỹ Thuật Điện, Điện Tử', '&lt;!DOCTYPE html&gt;\r\n&lt;html&gt;\r\n&lt;head&gt;\r\n&lt;/head&gt;\r\n&lt;body&gt;\r\n&lt;div class=&quot;desjob-company&quot;&gt;\r\n&lt;h4&gt;M&amp;Ocirc; TẢ C&amp;Ocirc;NG VIỆC&lt;/h4&gt;\r\n&lt;p&gt;Lắp đặt, sửa chữa, bảo tr&amp;igrave; c&amp;aacute;c trang thiết bị y tế: m&amp;aacute;y si&amp;ecirc;u &amp;acirc;m, m&amp;aacute;y chụp X-Quang, m&amp;aacute;y x&amp;eacute;t nghiệm v&amp;agrave; thiết bị văn ph&amp;ograve;ng như: m&amp;aacute;y t&amp;iacute;nh, m&amp;aacute;y in...&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;u&gt;Quyền lợi:&lt;/u&gt;&lt;/strong&gt;&lt;/p&gt;\r\n&lt;div class=&quot;job_more_detail&quot;&gt;- Lương cứng : 4 triệu (chưa kể phụ cấp v&amp;agrave; thưởng).&lt;br /&gt;- Được hưởng c&amp;aacute;c quyền lợi theo quy định của ph&amp;aacute;p luật.&lt;/div&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;desjob-company&quot;&gt;\r\n&lt;h4&gt;Y&amp;Ecirc;U CẦU C&amp;Ocirc;NG VIỆC&lt;/h4&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;u&gt;Số năm kinh nghiệm:&lt;/u&gt;&lt;/strong&gt; Dưới 1 năm&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;u&gt;Y&amp;ecirc;u cầu bằng cấp:&lt;/u&gt;&lt;/strong&gt; Trung cấp&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;u&gt;Y&amp;ecirc;u cầu giới t&amp;iacute;nh:&lt;/u&gt;&lt;/strong&gt; Nam&lt;/p&gt;\r\n&lt;p&gt;Nhiệt t&amp;igrave;nh, gắn b&amp;oacute; l&amp;acirc;u d&amp;agrave;i. Ưu ti&amp;ecirc;n người c&amp;oacute; kinh nghiệm v&amp;agrave; hiểu biết về y tế, CNTT&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;u&gt;Hồ sơ bao gồm:&lt;/u&gt;&lt;/strong&gt;&lt;/p&gt;\r\n&lt;div class=&quot;job_more_detail&quot;&gt;Theo quy định&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/body&gt;\r\n&lt;/html&gt;', 3, '2016-05-27', '2016-05-03 14:03:31', '2016-05-03 14:03:31', 7),
(4, 1, 'Nhân Viên Kinh Doanh', '&lt;!DOCTYPE html&gt;\r\n&lt;html&gt;\r\n&lt;head&gt;\r\n&lt;/head&gt;\r\n&lt;body&gt;\r\n&lt;div class=&quot;desjob-company&quot;&gt;\r\n&lt;h4&gt;M&amp;Ocirc; TẢ C&amp;Ocirc;NG VIỆC&lt;/h4&gt;\r\n&lt;p&gt;- Kinh doanh mặt h&amp;agrave;ng m&amp;aacute;y m&amp;oacute;c, thiết bị y tế, vật tư ti&amp;ecirc;u hao cho c&amp;aacute;c ph&amp;ograve;ng kh&amp;aacute;m, bệnh viện v&amp;agrave; c&amp;aacute;c cửa h&amp;agrave;ng chuy&amp;ecirc;n doanh.&lt;br /&gt;(sẽ trao đổi cụ thể khi phỏng vấn)&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;u&gt;Quyền lợi:&lt;/u&gt;&lt;/strong&gt;&lt;/p&gt;\r\n&lt;div class=&quot;job_more_detail&quot;&gt;- Lương cứng 3 triệu + % doanh thu + thưởng, ....&lt;br /&gt;- Được hưởng đầy đủ c&amp;aacute;c quyền lợi theo quy định của ph&amp;aacute;p luật (BHXH + BHYT,...) v&amp;agrave; theo ch&amp;iacute;nh s&amp;aacute;ch của c&amp;ocirc;ng ty.&lt;br /&gt;- L&amp;agrave;m việc 8h/ng&amp;agrave;y, 6ng&amp;agrave;y/tuần (nghỉ chủ nhật), nghỉ c&amp;aacute;c ng&amp;agrave;y lễ tết.&lt;br /&gt;- Được tham gia c&amp;aacute;c kh&amp;oacute;a đ&amp;agrave;o tạo bổ trợ cho c&amp;ocirc;ng việc miễn ph&amp;iacute;, được tham gia tham quan du lịch h&amp;agrave;ng năm theo ch&amp;iacute;nh s&amp;aacute;ch của c&amp;ocirc;ng ty.&lt;br /&gt;- Được l&amp;agrave;m việc trong m&amp;ocirc;i trường năng động v&amp;agrave; trẻ trung.&lt;br /&gt;- Được tăng lương định kỳ theo kết quả c&amp;ocirc;ng việc.&lt;br /&gt;- Ưu ti&amp;ecirc;n những người c&amp;oacute; kinh nghiệm l&amp;agrave;m việc tại c&amp;aacute;c c&amp;ocirc;ng ty kinh doanh về thiết bị y tế.&lt;/div&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;desjob-company&quot;&gt;\r\n&lt;h4&gt;Y&amp;Ecirc;U CẦU C&amp;Ocirc;NG VIỆC&lt;/h4&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;u&gt;Số năm kinh nghiệm:&lt;/u&gt;&lt;/strong&gt; 1 năm&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;u&gt;Y&amp;ecirc;u cầu bằng cấp:&lt;/u&gt;&lt;/strong&gt; Cao đẳng&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;u&gt;Y&amp;ecirc;u cầu giới t&amp;iacute;nh:&lt;/u&gt;&lt;/strong&gt; Nam&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;u&gt;Y&amp;ecirc;u cầu độ tuổi:&lt;/u&gt;&lt;/strong&gt; 25 - 29 tuổi&lt;/p&gt;\r\n&lt;p&gt;- C&amp;oacute; mong muốn gắn b&amp;oacute; l&amp;acirc;u d&amp;agrave;i c&amp;ugrave;ng c&amp;ocirc;ng ty.&lt;br /&gt;- Ưu ti&amp;ecirc;n con em xứ Thanh mong muốn trở về l&amp;agrave;m việc tại qu&amp;ecirc; hương.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;u&gt;Hồ sơ bao gồm:&lt;/u&gt;&lt;/strong&gt;&lt;/p&gt;\r\n&lt;div class=&quot;job_more_detail&quot;&gt;- Bộ hồ sơ chuẩn đầy đủ (chỉ cần nộp hồ sơ ph&amp;ocirc; t&amp;ocirc;, khi được nhận sẽ ho&amp;agrave;n thiền hồ sơ sau)&lt;br /&gt;- Gọi điện trước khi đến.&lt;/div&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;desjob-company&quot;&gt;\r\n&lt;h4&gt;Y&amp;Ecirc;U CẦU HỒ SƠ&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;/body&gt;\r\n&lt;/html&gt;', 6, '2016-05-31', '2016-05-03 14:03:06', '2016-05-03 14:03:06', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jobcate`
--

DROP TABLE IF EXISTS `jobcate`;
CREATE TABLE `jobcate` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `jobcate`
--

TRUNCATE TABLE `jobcate`;
--
-- Dumping data for table `jobcate`
--

INSERT INTO `jobcate` (`id`, `name`) VALUES
(1, 'Lập trình viên PHP'),
(2, 'Kế toán'),
(3, 'Kỹ sư cơ khí'),
(4, 'Thiết kế thời trang'),
(5, 'Bất động sản'),
(6, 'Kinh doanh');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `migration`
--

TRUNCATE TABLE `migration`;
--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1460451385),
('m130524_201442_init', 1460451446);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `user`
--

TRUNCATE TABLE `user`;
--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', '_MO14GIlLN73YBKxAtHdVOrhas8B1Yka', '$2y$13$qVNqK.gUBAzm4Horum83l.4p3BNRC5FREHqxBjorSlluFWKyWLX7q', NULL, 'admin@gmail.com', 10, 1460451457, 1460451457);

-- --------------------------------------------------------

--
-- Table structure for table `vg`
--

DROP TABLE IF EXISTS `vg`;
CREATE TABLE `vg` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `vg`
--

TRUNCATE TABLE `vg`;
--
-- Dumping data for table `vg`
--

INSERT INTO `vg` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Chuyển hướng sự nghiệp giữa đường đời', '<p><em>Bạn c&oacute; biết Eric Cantona? Huyền thoại b&oacute;ng đ&aacute; người Ph&aacute;p n&agrave;y đ&atilde; từ gi&atilde; m&ocirc;n thể thao vua khi mới 30 tuổi, đang ở đỉnh cao phong độ, với l&yacute; do &ldquo;đ&atilde; mất niềm đam m&ecirc; thi đấu&rdquo;, d&ugrave; b&oacute;ng đ&aacute; đ&atilde; đem lại cho anh vinh quang v&agrave; tiền bạc. Rời bỏ thể thao, anh theo đuổi niềm đam m&ecirc; mới - nghệ thuật thứ 7 - với vai tr&ograve; l&agrave; diễn vi&ecirc;n, đạo diễn, nh&agrave; sản xuất phim v&agrave; đ&atilde; đạt được th&agrave;nh c&ocirc;ng nhất định: bộ phim &ldquo;Looking for Eric&rdquo; do anh sản xuất v&agrave; đ&oacute;ng vai ch&iacute;nh được lọt v&agrave;o v&ograve;ng đề cử phim hay nhất giải C&agrave;nh cọ v&agrave;ng Li&ecirc;n hoan phim Cannes 2009.</em></p>\r\n<p>C&oacute; thể bạn sẽ cho rằng v&igrave; Eric đ&atilde; qu&aacute; gi&agrave;u rồi n&ecirc;n anh mới bỏ nghề dễ d&agrave;ng như thế. Vậy t&ocirc;i lại đưa ra một trường hợp kh&aacute;c: nữ văn sỹ J.K. Rowling của si&ecirc;u phẩm Harry Potter. B&agrave; đam m&ecirc; viết s&aacute;ch v&agrave; lu&ocirc;n ấp ủ ước mơ tạo n&ecirc;n một tuyệt t&aacute;c, nhưng phải đến gần 10 năm sau, v&agrave;o năm 1997, ở lứa tuổi trung ni&ecirc;n, sau khi chấp nhận v&ocirc; số c&ocirc;ng việc tạm bợ, b&agrave; mới ho&agrave;n th&agrave;nh tập đầu của bộ tiểu thuyết lừng danh.<img class="img_boder img_left_detail" title="" src="http://advice.vietnamworks.com/files/imagecache/thumbnail_258x208/files/CHUYEN-HUONG.jpg" alt="" />&nbsp;B&acirc;y giờ b&agrave; được t&ocirc;n vinh l&agrave; &ldquo;nh&agrave; văn Anh c&ograve;n sống vĩ đại nhất&rdquo; v&agrave; cũng l&agrave; người phụ nữ gi&agrave;u nhất nước Anh.</p>\r\n<p>Trải qua nhiều th&aacute;ng ng&agrave;y b&igrave;nh lặng với c&ocirc;ng việc v&agrave; thu nhập ổn định, nay muốn thay đổi nghề nghiệp theo đam m&ecirc; hay v&igrave; bất cứ l&yacute; do n&agrave;o kh&aacute;c, ch&uacute;ng ta thường e ngại kh&ocirc;ng biết con đường ph&iacute;a trước sẽ ra sao. Eric hay Rowling ch&iacute;nh l&agrave; nguồn động vi&ecirc;n lớn lao đối với những ai đang muốn thay đổi sự nghiệp ở giữa đường đời. H&atilde;y tự hỏi bạn c&oacute; chấp nhận l&agrave;m kẻ l&agrave;ng nh&agrave;ng hay kh&aacute;t khao trở th&agrave;nh người đạt tới đỉnh cao trong nghề nghiệp? Một khi muốn l&agrave;m lại th&igrave; d&ugrave; muộn vẫn c&ograve;n hơn kh&ocirc;ng. Sau đ&acirc;y l&agrave; những bước bạn n&ecirc;n l&agrave;m nếu quyết định thay đổi:</p>\r\n<p><strong>1. X&aacute;c định nguy&ecirc;n nh&acirc;n muốn thay đổi nghề nghiệp</strong><br />Tại sao bạn muốn thay đổi nghề nghiệp? V&igrave; buồn ch&aacute;n, v&igrave; nghề đang l&agrave;m kh&ocirc;ng ph&ugrave; hợp khả năng, chưa bao giờ y&ecirc;u th&iacute;ch c&ocirc;ng việc ấy, lương/vị tr&iacute; qu&aacute; thấp so với năng lực, kh&ocirc;ng c&oacute; triển vọng thăng tiến, v&igrave; thấy ng&agrave;nh nghề kh&aacute;c hấp dẫn hơn&hellip; Bạn cũng cần&nbsp;suy x<span style="color: #333333;">&eacute;t kỹ</span>, liệu mong muốn thay đổi nghề của bạn chỉ l&agrave; nhất thời trong một ph&uacute;t gi&acirc;y bồng bột, hoặc bị stress do &aacute;p lực c&ocirc;ng việc&hellip;, hay bạn đ&atilde; ấp ủ mong muốn thay đổi từ l&acirc;u.</p>\r\n<p><strong>2. X&aacute;c định r&otilde; ng&agrave;nh nghề ph&ugrave; hợp với m&igrave;nh</strong><br />Bạn muốn thay đổi hẳn c&ocirc;ng việc n&agrave;y để chuyển sang một nghề mới, hay muốn tự lập c&ocirc;ng ty ri&ecirc;ng? Bạn thuộc kiểu người n&agrave;o, nghệ sỹ hay thực tiễn, nghi&ecirc;n cứu&hellip;? Bởi ứng với mỗi kiểu người l&agrave; một nghề nghiệp ph&ugrave; hợp. Sau khi x&aacute;c định r&otilde; những điều tr&ecirc;n, bạn sẽ biết m&igrave;nh cần chọn c&ocirc;ng việc g&igrave;. Hẳn nhi&ecirc;n chọn việc theo&nbsp;đa<span style="color: #333333;">m m&ecirc;</span>&nbsp;lu&ocirc;n l&agrave; gợi &yacute; đầu ti&ecirc;n, nhưng đam m&ecirc; phải gắn liền với năng lực. Bạn kh&ocirc;ng thể đ&ograve;i theo nghề b&aacute;o chỉ v&igrave; thấy nghề n&agrave;y th&uacute; vị, được đi nhiều, tiếp x&uacute;c nhiều&hellip; trong khi bạn kh&ocirc;ng c&oacute; khả năng viết tốt; bạn cũng kh&ocirc;ng thể muốn lập c&ocirc;ng ty ri&ecirc;ng chỉ để bằng bạn b&egrave; khi điều kiện về vốn hoặc năng lực l&atilde;nh đạo của bạn kh&ocirc;ng thể đ&aacute;p ứng.</p>\r\n<p>Việc x&aacute;c định r&otilde; c&ocirc;ng việc ph&ugrave; hợp rất quan trọng với bạn, bởi đ&acirc;y l&agrave; mấu chốt để bạn c&oacute; thể th&agrave;nh c&ocirc;ng khi bạn kh&ocirc;ng c&ograve;n trẻ v&agrave; chuẩn bị bắt đầu lại từ đầu. Để việc x&aacute;c định được chi tiết v&agrave; khoa học, bạn c&oacute; thể lập ra một danh s&aacute;ch c&aacute;c ng&agrave;nh nghề ph&ugrave; hợp với khả năng v&agrave; ước muốn của bạn. Thường th&igrave; bạn n&ecirc;n tham khảo &yacute; kiến của gia đ&igrave;nh v&agrave; bạn b&egrave;, tuy nhi&ecirc;n kh&ocirc;ng phải khi n&agrave;o họ cũng đ&uacute;ng, bởi họ sẽ tư duy theo logic th&ocirc;ng thường, trong khi một &ldquo;kẻ nổi loạn&rdquo; muốn thay đổi sự nghiệp đ&atilde; ổn định như bạn sẽ thi&ecirc;n về hướng đột ph&aacute;. Quyết định cuối c&ugrave;ng vẫn thuộc về bạn!</p>\r\n<p><strong>3. Bổ sung kỹ năng ph&ugrave; hợp cho c&ocirc;ng việc mới</strong><br />Việc trau dồi&nbsp;kỹ năng c<span style="color: #333333;">ho c&ocirc;ng việc mới</span>&nbsp;cần được bắt đầu từ trước khi bạn thay đổi nghề nghiệp. Nếu bạn chọn một nghề ho&agrave;n to&agrave;n mới, bạn sẽ phải học v&agrave; học rất nhiều để c&oacute; được kỹ năng chuy&ecirc;n m&ocirc;n đ&aacute;p ứng đủ cho c&ocirc;ng việc. Những kh&oacute;a học bồi dưỡng kỹ năng thường kh&ocirc;ng chiếm nhiều thời gian v&agrave; chi ph&iacute;, nhưng ch&uacute;ng sẽ gi&uacute;p &iacute;ch cho bạn rất nhiều, như cập nhật lượng kiến thức chuy&ecirc;n m&ocirc;n về c&ocirc;ng việc, t&iacute;ch lũy được kinh nghiệm trong nghề của những người đi trước, gia nhập mạng lưới nghề nghiệp từ lớp học&hellip; D&ugrave; từng l&agrave; si&ecirc;u sao thể thao, Eric Cantona đ&atilde; phải tham gia nhiều kh&oacute;a đ&agrave;o tạo diễn xuất v&agrave; đạo diễn mới c&oacute; được th&agrave;nh c&ocirc;ng bước đầu trong nghệ thuật thứ 7 sau nhiều lời ch&ecirc; bai. Kh&ocirc;ng chỉ c&oacute; đam m&ecirc;, J. K. Rowling vốn rất giỏi văn học, tiếng La tinh, tiếng Hy Lạp v&agrave; tiếng Ph&aacute;p, đ&atilde; tham khảo nhiều tư liệu cổ để c&oacute; thể viết n&ecirc;n Harry Potter đầy hấp dẫn.</p>\r\n<p><strong>4. Tạo bước đệm trước khi ch&iacute;nh thức &ldquo;l&agrave;m lại từ đầu&rdquo;</strong><br />Bạn c&oacute; thể vừa học vừa l&agrave;m như một &ldquo;thợ tập sự&rdquo; trước khi ch&iacute;nh thức tham gia c&ocirc;ng việc mới. H&atilde;y t&igrave;m hiểu kỹ lưỡng lĩnh vực bạn quan t&acirc;m từ c&aacute;c nguồn như Internet, bạn b&egrave; người quen, học hỏi từ những người đ&atilde; hoạt động l&acirc;u năm trong lĩnh vực đ&oacute; để tham khảo kinh nghiệm của họ. Bạn cũng c&oacute; thể t&igrave;m một c&ocirc;ng việc b&aacute;n thời gian, hoặc tham gia tự nguyện những dự &aacute;n c&oacute; li&ecirc;n quan để c&oacute; th&ecirc;m kinh nghiệm cho c&ocirc;ng việc mới. Một khi bạn thấy tự tin với những g&igrave; m&igrave;nh đ&atilde; nắm bắt v&agrave; chuẩn bị, ấy l&agrave; l&uacute;c bạn c&oacute; thể tiến h&agrave;nh c&ocirc;ng việc mới m&agrave; bạn mong chờ.</p>\r\n<p>Mọi thử th&aacute;ch đều c&oacute; c&aacute;i gi&aacute; của n&oacute;, nếu chọn c&aacute;ch sống b&igrave;nh lặng, sẽ chẳng ai ch&ecirc; tr&aacute;ch bạn, c&ograve;n nếu đủ đam m&ecirc;, tự tin v&agrave; ki&ecirc;n nhẫn, bạn h&atilde;y chọn c&aacute;ch thay đổi để vươn l&ecirc;n. Con c&aacute; nếu chỉ tung tăng trong một c&aacute;i ao nhỏ th&igrave; d&ugrave; cố hết sức cũng chỉ được vẫy v&ugrave;ng trong chiếc ao nhỏ m&agrave; th&ocirc;i, nhưng nếu n&oacute; được thả v&agrave;o d&ograve;ng s&ocirc;ng th&igrave; một ng&agrave;y n&agrave;o đ&oacute;, n&oacute; c&oacute; thể bơi ra biển lớn.</p>', '2016-04-17 11:17:47', '2016-04-17 12:17:16'),
(2, 'Trắc nghiệm - Công việc hiện tại có phù hợp?', '<p>Một&nbsp;c&ocirc;ng việc <span style="color: #333333;">ph&ugrave; hợp</span>&nbsp;l&agrave; nơi t&agrave;i năng của bạn tỏa s&aacute;ng, bạn sẽ l&agrave;m việc với hiệu suất cao nhất v&agrave; với một tinh thần lạc quan, thoải m&aacute;i. C&ocirc;ng việc đ&oacute; cũng phải ph&ugrave; hợp với t&iacute;nh c&aacute;ch, sở th&iacute;ch, kỹ năng v&agrave; những gi&aacute; trị m&agrave; bạn tin tưởng, gi&uacute;p bạn ho&agrave;n th&agrave;nh những mục ti&ecirc;u trong cuộc sống v&agrave; mang đến cho bạn sự tự h&agrave;o về bản th&acirc;n.</p>\r\n<p>Cũng như khi t&igrave;m cho m&igrave;nh người bạn đời l&yacute; tưởng, một cộng việc l&yacute; tưởng cần đến sự tương đồng giữa hai b&ecirc;n ở nhiều mức độ kh&aacute;c nhau. Nếu bạn đang băn khoăn về c&ocirc;ng việc hiện tại, h&atilde;y trả lời ngay những c&acirc;u hỏi trắc nghiệm sau để c&oacute; c&aacute;i nh&igrave;n kh&aacute;ch quan hơn về c&ocirc;ng việc của m&igrave;nh.&nbsp; &nbsp;</p>\r\n<p><strong>Những dấu hiệu của một c&ocirc;ng việc kh&ocirc;ng ph&ugrave; hợp</strong><br />Bạn biết m&igrave;nh đ&atilde; chọn sai việc nếu:&nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;C&ocirc;ng việc của bạn chỉ đơn thuần l&agrave; một phương tiện kiếm sống.<br />&bull;&nbsp;&nbsp; &nbsp;Đ&oacute; l&agrave; c&ocirc;ng việc m&agrave; bạn nhận l&agrave;m 10 năm về trước, khi kh&ocirc;ng c&ograve;n sự lựa chọn n&agrave;o kh&aacute;c.<br />&bull;&nbsp;&nbsp; &nbsp;Bố mẹ bạn v&ocirc; c&ugrave;ng h&atilde;nh diện, nhưng bạn th&igrave; ch&aacute;n ngấy đến tận cổ.<br />&bull;&nbsp;&nbsp; &nbsp;C&ocirc;ng việc khiến bạn buồn ch&aacute;n, l&agrave;m tổn thương l&ograve;ng tự trọng của bạn, l&agrave;m suy giảm những gi&aacute; trị ri&ecirc;ng v&agrave; cản trở sự ho&agrave;n thiện nh&acirc;n c&aacute;ch của bạn.<br />&bull;&nbsp;&nbsp; &nbsp;Bạn chỉ mong sớm đến ng&agrave;y cuối tuần.</p>\r\n<p><img class="img_boder img_left_detail" title="TRẮC NGHIỆM - C&Ocirc;NG VIỆC HIỆN TẠI C&Oacute; PH&Ugrave; HỢP?" src="http://advice.vietnamworks.com/files/imagecache/thumbnail_258x208/files/climbing-ladder.jpg" alt="TRẮC NGHIỆM - C&Ocirc;NG VIỆC HIỆN TẠI C&Oacute; PH&Ugrave; HỢP?" /></p>\r\n<p><strong>Đ&aacute;nh gi&aacute; t&igrave;nh trạng c&ocirc;ng việc</strong><br />Sau đ&acirc;y l&agrave; một bảng c&acirc;u hỏi gi&uacute;p bạn đ&aacute;nh gi&aacute; t&igrave;nh trạng c&ocirc;ng việc hiện tại của m&igrave;nh. Bạn h&atilde;y đ&aacute;nh dấu &radic; v&agrave;o c&acirc;u bạn thấy đ&uacute;ng (v&agrave; đừng để &yacute; đến chữ c&aacute;i đứng sau mỗi c&acirc;u).<br />&bull;&nbsp;&nbsp; &nbsp;Bạn mong chờ được đi l&agrave;m mỗi ng&agrave;y. (S)&nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;Sếp đối xử với bạn một c&aacute;ch c&ocirc;ng bằng v&agrave; t&ocirc;n trọng. (S)&nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;Bạn chỉ mong chờ đến ng&agrave;y cuối tuần v&agrave; bất cứ ng&agrave;y n&agrave;o kh&ocirc;ng phải l&agrave;m việc. (G)&nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;Bạn thấy m&igrave;nh c&oacute; gi&aacute; trị v&agrave; được tr&acirc;n trọng v&igrave; những đ&oacute;ng g&oacute;p của m&igrave;nh cho c&ocirc;ng việc. (S)<br />&bull;&nbsp;&nbsp; &nbsp;Bạn cảm thấy nơi l&agrave;m việc thật độc hại. (G)<br />&bull;&nbsp;&nbsp; &nbsp;Trong c&ocirc;ng việc, bạn được l&agrave; ch&iacute;nh m&igrave;nh v&agrave; kh&ocirc;ng phải lo lắng về việc bị đ&aacute;nh gi&aacute;. (S)<br />&bull;&nbsp;&nbsp; &nbsp;Bạn được cập nhật mọi th&ocirc;ng tin về t&igrave;nh h&igrave;nh hoạt động của c&ocirc;ng ty. (S)&nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;Sếp thường xuy&ecirc;n thảo luận với bạn, đem lại cho bạn cơ hội ph&aacute;t triển v&agrave; thăng tiến trong nghề nghiệp. (S)<br />&bull;&nbsp;&nbsp; &nbsp;Bạn được khen ngợi v&igrave; những nỗ lực của m&igrave;nh. (S)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;C&ocirc;ng việc k&iacute;ch th&iacute;ch khả năng tr&iacute; tuệ v&agrave; tư duy của bạn. (S)<br />&bull;&nbsp;&nbsp; &nbsp;Bạn thấy m&igrave;nh đang đ&oacute;ng g&oacute;p một phần hữu &iacute;ch cho x&atilde; hội. (S)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;Bạn được đền đ&aacute;p xứng đ&aacute;ng cho c&ocirc;ng việc của m&igrave;nh. (S)<br />&bull;&nbsp;&nbsp; &nbsp;Bạn thấy m&igrave;nh thường xuy&ecirc;n mơ tưởng về một c&ocirc;ng việc mới. (G)&nbsp;&nbsp;&nbsp; &nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;Bạn nhận thấy c&ocirc;ng việc hiện tại l&agrave; bước ph&aacute;t triển tự nhi&ecirc;n cho ch&iacute;nh con người thật của m&igrave;nh. (S)<br />&bull;&nbsp;&nbsp; &nbsp;Bạn tự thấy m&igrave;nh th&agrave;nh c&ocirc;ng. (S)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;Bạn thấy m&igrave;nh như bị mắc kẹt trong vị tr&iacute; hiện tại. (G)&nbsp;&nbsp;&nbsp; &nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;Bạn thấy m&igrave;nh ho&agrave;n to&agrave;n kiểm so&aacute;t con đường sự nghiệp. (S)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;Bạn đang l&agrave;m việc bằng tất cả năng lực của m&igrave;nh. (S)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;C&ocirc;ng việc hiện tại ảnh hưởng ti&ecirc;u cực đến những người xung quanh bạn. (G)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<br />&bull;&nbsp;&nbsp; &nbsp;Bạn khao kh&aacute;t được thử sức ở một lĩnh vực kh&aacute;c mới mẻ hơn. (G)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>\r\n<p>Giờ th&igrave; h&atilde;y đếm số &ldquo;S&rdquo; v&agrave; &ldquo;G&rdquo; tại những c&acirc;u m&agrave; bạn đ&atilde; chọn. &ldquo;S&rdquo; l&agrave; &ldquo;Stay&rdquo; (Ở) c&ograve;n &ldquo;G&rdquo; l&agrave; &ldquo;Go&rdquo; (Đi). Bảng c&acirc;u hỏi n&agrave;y l&agrave; một thước đo đ&aacute;ng tin cậy để bạn đ&aacute;nh gi&aacute; mức độ ph&ugrave; hợp của&nbsp;c&ocirc;ng <span style="color: #333333;">việc</span>&nbsp;hiện tại. Bạn chọn c&agrave;ng nhiều c&acirc;u &ldquo;G&rdquo; th&igrave; đ&atilde; đến l&uacute;c bạn cần suy nghĩ nghi&ecirc;m t&uacute;c về một sự thay đổi, sự thay đổi t&iacute;ch cực!</p>', '2016-04-17 12:17:34', '2016-04-17 12:17:34'),
(3, 'Làm “sếp” ở công ty nhỏ hay làm “lính” ở công ty lớn?', '<p><strong>Th&ocirc;ng thường, chức vụ c&agrave;ng cao thu nhập c&agrave;ng nhiều. Tuy nhi&ecirc;n, c&oacute; những trường hợp vị tr&iacute; cao kh&ocirc;ng hề tỉ lệ thuận với mức lương, đơn giản v&igrave; l&agrave;m sếp ở c&ocirc;ng ty nhỏ lương thưởng chưa chắc đ&atilde; bằng một &ldquo;ch&uacute; l&iacute;nh&rdquo; ở c&ocirc;ng ty lớn. Bạn sẽ chọn g&igrave; giữa quyền lực v&agrave; thu nhập?</strong></p>\r\n<p>Hẳn kh&ocirc;ng &iacute;t người đ&atilde; hơn một lần băn khoăn lựa chọn việc l&agrave;m sếp ở c&ocirc;ng ty nhỏ hay l&agrave;m &ldquo;l&iacute;nh&rdquo; ở c&ocirc;ng ty lớn. B&ecirc;n cạnh quyền lực hay thu nhập, sự lựa chọn của bạn sẽ c&ograve;n mang lại những gi&aacute; trị thực tế kh&aacute;c.</p>\r\n<p><strong>&ldquo;Sếp&rdquo; của &ldquo;Người t&iacute; hon&rdquo;</strong></p>\r\n<p>D&ugrave; l&agrave; sếp, lương cũng như c&aacute;c khoản trợ cấp kh&aacute;c của bạn ở c&ocirc;ng ty &ldquo;t&iacute; hon&rdquo; c&oacute; thể sẽ thấp hơn nh&acirc;n vi&ecirc;n c&aacute;c c&ocirc;ng ty lớn do doanh thu v&agrave; lợi nhuận thấp. Hơn nữa, một số doanh nghiệp nhỏ thường thiếu bề d&agrave;y kinh nghiệm v&agrave; cơ cấu chặt chẽ, điều n&agrave;y kh&oacute; đảm bảo cho bạn một c&ocirc;ng việc ổn định. Cũng phải chấp nhận khả năng bạn c&oacute; thể bị &ldquo;k&eacute;m thế&rdquo; hơn sếp hoặc thậm ch&iacute;, nh&acirc;n vi&ecirc;n ở những c&ocirc;ng ty lớn.</p>\r\n<p><img class="img_boder img_left_detail" title="" src="http://advice.vietnamworks.com/files/imagecache/thumbnail_258x208/files/Lam-sep-cong-ty-nho-hay-lam-linh-cong-ty-lon.jpg" alt="" /></p>\r\n<p>Tuy nhi&ecirc;n, theo một g&oacute;c nh&igrave;n kh&aacute;c, khi l&agrave;m việc cho c&aacute;c doanh nghiệp nhỏ, bạn sẽ đảm tr&aacute;ch nhiều nhiệm vụ kh&aacute;c nhau, c&oacute; cơ hội chứng tỏ năng lực bản th&acirc;n. Khi l&agrave;m sếp, bạn sẽ được chủ động giải quyết vấn đề lớn nhỏ của c&ocirc;ng ty v&agrave; nắm trong tay quyền điều h&agrave;nh cả đội ngũ nh&acirc;n vi&ecirc;n (d&ugrave; &iacute;t). Ngo&agrave;i ra, bạn sẽ t&iacute;ch lũy được nhiều kỹ năng mới trong c&ocirc;ng việc, cũng như c&oacute; m&ocirc;i trường ph&ugrave; hợp để r&egrave;n luyện phương ph&aacute;p quản l&yacute; v&agrave; khả năng l&atilde;nh đạo.</p>\r\n<p><strong>&ldquo;L&iacute;nh&rdquo; của &ldquo;G&atilde; khổng lồ&rdquo;</strong></p>\r\n<p>T&iacute;nh ổn định, c&aacute;c khoản thu nhập tương đối tốt, c&ugrave;ng rất nhiều cơ hội để bạn chứng tỏ khả năng, ph&aacute;t triển sự nghiệp của m&igrave;nh &ndash; đấy l&agrave; những ưu điểm khi bạn &ldquo;đầu qu&acirc;n&rdquo; cho một &ldquo;g&atilde; khổng lồ&rdquo; n&agrave;o đấy. Bạn cũng sẽ học hỏi được rất nhiều về sự chuy&ecirc;n nghiệp trong c&aacute;ch tổ chức c&ocirc;ng việc ở một c&ocirc;ng ty lớn. Ngo&agrave;i ra, l&agrave; nh&acirc;n vi&ecirc;n của một c&ocirc;ng ty &ldquo;h&agrave;ng hiệu&rdquo; kể cũng &ldquo;oai&rdquo;!</p>\r\n<p>Tuy nhi&ecirc;n, mặt tr&aacute;i của sự &ldquo;oai phong&rdquo; ch&iacute;nh l&agrave; mức độ &aacute;p lực c&ocirc;ng việc kh&aacute; cao. Quỹ thời gian nghỉ ngơi qu&yacute; b&aacute;u của bạn sẽ nhanh ch&oacute;ng bị r&uacute;t gọn khi bạn &ldquo;tối mắt tối mũi&rdquo; quay v&ograve;ng với lịch l&agrave;m việc d&agrave;y đặc, đ&oacute; l&agrave; chưa kể việc đ&ocirc;i l&uacute;c bạn c&ograve;n phải đương đầu với sự cạnh tranh kh&aacute; gay gắt từ đồng nghiệp.</p>\r\n<p><strong>Bạn sẽ chọn l&agrave;m g&igrave;?</strong></p>\r\n<p>&ldquo;Sếp&rdquo; ở c&ocirc;ng ty nhỏ v&agrave; &ldquo;l&iacute;nh&rdquo; ở c&ocirc;ng ty lớn, vị tr&iacute; n&agrave;o<strong> n&agrave;o ph&ugrave; hợp với bạn nhất? Điều đ&oacute; phụ thuộc v&agrave;o t&iacute;nh c&aacute;ch, khả năng, sở th&iacute;ch v&agrave; kinh nghiệm của bạn.</strong></p>\r\n<p>Nếu l&agrave;m sếp, bạn sẽ c&oacute; uy quyền hơn, nhưng đồng thời tr&aacute;ch nhiệm cũng sẽ nặng nề hơn. &ldquo;L&agrave;m sếp kh&oacute; đấy, phải đ&acirc;u chuyện đ&ugrave;a&rdquo;, bởi những quyết định bạn đưa ra ảnh hưởng trực tiếp đến sự vận h&agrave;nh của c&ocirc;ng ty. Do đ&oacute;, để trở th&agrave;nh người sếp giỏi, bạn cần phải c&oacute; thực lực, kiến thức rộng, khả năng ph&acirc;n t&iacute;ch, đ&aacute;nh gi&aacute; sắc b&eacute;n v&agrave; tầm nh&igrave;n xa.</p>\r\n<p>Ngược lại, c&oacute; thể hiện tại bạn chọn c&aacute;ch &ldquo;ẩn m&igrave;nh&rdquo; trong c&ocirc;ng ty lớn để thu thập kinh nghiệm cho nấc thang mới &ndash; trở th&agrave;nh sếp cho c&ocirc;ng ty của ri&ecirc;ng bạn, hoặc củng cố khả năng l&atilde;nh đạo ở c&ocirc;ng ty nhỏ, rồi bất ngờ một ng&agrave;y đẹp trời, bạn đứng v&agrave;o vị tr&iacute; chủ chốt tại một &ldquo;g&atilde; khổng lồ&rdquo; danh tiếng. Những bước đệm ấy l&agrave; một phần rất quan trọng trong tiến tr&igrave;nh sự nghiệp của bạn.<br />&nbsp;<br />Điều quan trọng hơn cả kh&ocirc;ng phải l&agrave; chuyện l&agrave;m sếp hay &ldquo;l&iacute;nh&rdquo; m&agrave; l&agrave; chọn c&ocirc;ng việc ph&ugrave; hợp nhất v&agrave; bạn thật sự đam m&ecirc; để qua đ&oacute;, bạn c&oacute; thể ph&aacute;t huy hết &ldquo;nội lực&rdquo;. Một khi đ&atilde; x&aacute;c định r&otilde; con đường của m&igrave;nh, bạn hẳn sẽ kh&ocirc;ng c&ograve;n vướng bận bởi nỗi băn khoăn chọn lựa giữa &ldquo;l&iacute;nh&rdquo; v&agrave; sếp.</p>', '2016-04-17 12:17:02', '2016-04-17 12:17:02'),
(4, 'Chuỗi cung ứng tối ưu - Chi phí tối thiểu, hiệu quả tối đa', '<p><em>Với xu thế to&agrave;n cầu h&oacute;a như hiện nay, việc một đ&ocirc;i gi&agrave;y mang thương hiệu Mỹ như Nike hay Adidas nhưng lại được sản xuất ở Việt Nam đ&atilde; l&agrave; chuyện qu&aacute; đỗi b&igrave;nh thường. Tuy nhi&ecirc;n, chắc chắn cho đến nay nhiều người vẫn chưa biết r&otilde; h&agrave;nh tr&igrave;nh m&agrave; một đ&ocirc;i gi&agrave;y như thế đ&atilde; trải qua để đến với người ti&ecirc;u d&ugrave;ng. H&agrave;nh tr&igrave;nh đ&oacute; l&agrave; sự phối hợp của rất nhiều kh&acirc;u, từ nh&agrave; cung cấp nguy&ecirc;n phụ liệu, c&aacute;c nh&agrave; m&aacute;y gia c&ocirc;ng tr&ecirc;n khắp thế giới, c&aacute;c đơn vị vận chuyển đến c&aacute;c trung t&acirc;m ph&acirc;n phối, c&aacute;c cửa hiệu b&aacute;n sỉ, b&aacute;n lẻ&hellip; L&agrave;m c&aacute;ch n&agrave;o để sự phối hợp n&agrave;y được tiến h&agrave;nh su&ocirc;n sẻ, vừa tiết kiệm chi ph&iacute; tối đa vừa đem lại nhiều lợi &iacute;ch nhất cho c&aacute;c b&ecirc;n li&ecirc;n quan?</em></p>\r\n<p>&nbsp;</p>\r\n<p>Đ&oacute; l&agrave; vấn đề m&agrave; c&aacute;c chuy&ecirc;n vi&ecirc;n quản l&yacute; chuỗi cung ứng (Supply Chain) &ndash; một trong những ng&agrave;nh nghề &ldquo;n&oacute;ng&rdquo; nhất ở Việt Nam hiện nay &ndash; lu&ocirc;n trăn trở, đặc biệt trong bối cảnh kinh tế suy giảm như thời gian qua.</p>\r\n<p><strong>Quản l&yacute; chuỗi cung ứng v&agrave; quản l&yacute; logistics &ndash; Tưởng một m&agrave; hai</strong><br />Cho đến nay, nhiều người vẫn nhầm tưởng quản l&yacute; chuỗi cung ứng v&agrave; quản l&yacute; logistics l&agrave; một n&ecirc;n d&ugrave;ng hai thuật ngữ n&agrave;y thay thế cho nhau. Thật ra, quản l&yacute; logistic hay quản l&yacute; hậu cần chỉ li&ecirc;n quan đến c&ocirc;ng việc quản l&yacute; về mặt kho b&atilde;i, vận chuyển, giao nhận v&agrave; ph&acirc;n phối h&agrave;ng h&oacute;a. C&ograve;n quản l&yacute; chuỗi cung ứng l&agrave; việc quản l&yacute; cả một hệ thống bao gồm ph&aacute;t triển sản phẩm, sản xuất, mua b&aacute;n, tồn kho, ph&acirc;n phối v&agrave; c&aacute;c hoạt động hậu cần. N&oacute;i c&aacute;ch kh&aacute;c, hậu cần chỉ l&agrave; một th&agrave;nh tố của chuỗi cung ứng.</p>\r\n<p><strong>Chuỗi cung ứng tối ưu - Chi ph&iacute; thấp, hiệu quả cao</strong><br />Theo c&aacute;c chuy&ecirc;n gia trong ng&agrave;nh, chuỗi cung ứng tối ưu l&agrave; chuỗi cung ứng vận h&agrave;nh nhịp nh&agrave;ng, c&oacute; khả năng đ&aacute;p ứng nhu cầu kh&aacute;ch h&agrave;ng ở mức cao nhất với chi ph&iacute; vận h&agrave;nh thấp nhất. Đồng thời, n&oacute; phải c&oacute; hệ thống th&ocirc;ng tin được tổ chức khoa học v&agrave; cập nhật thường xuy&ecirc;n để gi&uacute;p c&aacute;c bộ phận phối hợp ăn &yacute; với nhau nhằm phản ứng nhanh nhạy với những biến động thường xuy&ecirc;n v&agrave; li&ecirc;n tục của m&ocirc;i trường kinh doanh. Một chuỗi cung ứng tối ưu sẽ gi&uacute;p doanh nghiệp thu h&uacute;t th&ecirc;m nhiều kh&aacute;ch h&agrave;ng, gia tăng thị phần, tiết kiệm chi ph&iacute;, từ đ&oacute; gia tăng doanh thu v&agrave; lợi nhuận. Ch&iacute;nh nhờ quản l&yacute; chuỗi cung ứng hiệu quả m&agrave; Wal-Mart mới c&oacute; thể trở th&agrave;nh c&ocirc;ng ty h&agrave;ng đầu thế giới trong lĩnh vực b&aacute;n lẻ.</p>\r\n<p><strong>Chuy&ecirc;n vi&ecirc;n quản l&yacute; chuỗi cung ứng &ndash; Họ l&agrave;<br />ai? </strong><br />C&aacute;c chuy&ecirc;n vi&ecirc;n quản l&yacute; chuỗi cung ứng lu&ocirc;n l&agrave; một trong những người bận rộn nhất trong doanh nghiệp. C&ocirc;ng việc của họ đ&ocirc;i l&uacute;c c&oacute; thể bắt đầu từ tờ mờ s&aacute;ng v&agrave; chỉ kết th&uacute;c khi m&agrave;n đ&ecirc;m đ&atilde; bu&ocirc;ng xuống. Hết dự b&aacute;o v&agrave; lập kế hoạch nhu cầu nguy&ecirc;n vật liệu cho sản xuất, mua h&agrave;ng, vận tải, ph&acirc;n phối; lựa chọn, l&agrave;m việc, thương thuyết với c&aacute;c nh&agrave; cung cấp; điều phối hoạt động của từng bộ phận v&agrave; truyền th&ocirc;ng trong hệ thống, họ lại quay sang theo d&otilde;i, cải tiến hệ thống th&ocirc;ng tin (b&aacute;o c&aacute;o, kế hoạch&hellip;) v&agrave; quản l&yacute; rủi ro. Đặc biệt, nếu c&oacute; sự cố ph&aacute;t sinh (chẳng hạn đo&agrave;n xe vận tải kh&ocirc;ng thể đến đ&uacute;ng hẹn do thi&ecirc;n tai) th&igrave; chuy&ecirc;n vi&ecirc;n quản l&yacute; chuỗi cung ứng phải c&oacute; mặt để giải quyết ngay d&ugrave; l&uacute;c đ&oacute; c&oacute; l&agrave; nửa đ&ecirc;m hay ng&agrave;y nghỉ cuối tuần.</p>\r\n<p>Ch&iacute;nh v&igrave; phải g&aacute;ch v&aacute;c nhiều trọng tr&aacute;ch như vậy n&ecirc;n chuy&ecirc;n vi&ecirc;n quản l&yacute; chuỗi cung ứng cần nắm rất vững kiến thức về quản l&yacute; chuỗi cung ứng như c&aacute;c hệ thống MRP, MRPII, JIT &hellip;, tầm quan trọng của mối quan hệ giữa c&aacute;c bộ phận trong chuỗi, phương ph&aacute;p điều phối hoạt động, quản l&yacute; sự thay đổi, quản l&yacute; rủi ro &hellip; Ngo&agrave;i ra, họ c&ograve;n cần c&oacute; kỹ năng quản l&yacute; c&ocirc;ng việc tốt, ra quyết định, thu thập ph&acirc;n t&iacute;ch th&ocirc;ng tin, đ&agrave;m ph&aacute;n, &yacute; thức tr&aacute;ch nhiệm cao, khả năng l&agrave;m việc theo nh&oacute;m v&agrave; theo hệ thống để đạt được mục ti&ecirc;u chung.</p>\r\n<p><strong>Quản l&yacute; chi ph&iacute; trong chuỗi cung ứng&ndash; Kh&ocirc;ng phải chỉ &ldquo;cắt&rdquo; l&agrave; được! </strong><br />Khi nền kinh tế suy giảm, khả năng điều chỉnh v&agrave; thay đổi qui m&ocirc; của c&aacute;c hoạt động trong chuỗi cung ứng cũng như việc tiết kiệm chi ph&iacute; rất quan trọng. Quản l&yacute; v&agrave; tiết kiệm chi ph&iacute; hiệu quả sẽ g&oacute;p phần l&agrave;m giảm gi&aacute; th&agrave;nh sản phẩm, từ đ&oacute; l&agrave;m tăng sức cạnh tranh cho doanh nghiệp. Tr&aacute;ch nhiệm của chuy&ecirc;n vi&ecirc;n quản l&yacute; chuỗi cung ứng cũng v&igrave; thế m&agrave; trở n&ecirc;n nặng nề hơn.</p>\r\n<p>Tuy nhi&ecirc;n, khi cắt giảm chi ph&iacute;, ch&uacute;ng ta cần ph&acirc;n biệt giữa &ldquo;cắt&rdquo; (cutting) v&agrave; &ldquo;giảm&rdquo; (reducing). Hiện nay, nhiều nh&agrave; quản l&yacute; xem cắt v&agrave; giảm chi ph&iacute; l&agrave; một. Đ&acirc;y l&agrave; một sai lầm lớn v&igrave; nếu cứ &ldquo;cắt&rdquo; một c&aacute;ch v&ocirc; tội vạ, đ&aacute;nh đồng tất cả c&aacute;c chi ph&iacute; theo kiểu &ldquo;c&aacute; m&egrave; một lứa&rdquo; th&igrave; sẽ dẫn tới sự đ&igrave;nh trệ hoạt động v&agrave; giảm lợi nhuận của doanh nghiệp. Tuy chi ph&iacute; l&agrave; yếu tố tối quan trọng trong việc x&acirc;y dựng một chuỗi cung ứng hiệu quả nhưng mức độ đ&aacute;p ứng nhu cầu kh&aacute;ch h&agrave;ng về thời gian, số lượng v&agrave; chất lượng vẫn l&agrave; yếu tố quan trọng nhất. Home Depot, nh&agrave; b&aacute;n lẻ h&agrave;ng trang tr&iacute; nội thất h&agrave;ng đầu của Mỹ, đ&atilde; từng chủ trương cắt giảm chi ph&iacute; bằng c&aacute;ch sa thải những kỹ sư nhiều kinh nghiệm để thay bằng những người l&agrave;m việc b&aacute;n thời gian hoặc &iacute;t kinh nghiệm. Tuy nhi&ecirc;n, chỉ sau một thời gian ngắn, do kh&aacute;ch h&agrave;ng phản ứng về chất lượng phục vụ c&agrave;ng l&uacute;c c&agrave;ng đi xuống, Home Depot đ&atilde; buộc phải điều chỉnh lại ch&iacute;nh s&aacute;ch của m&igrave;nh.</p>\r\n<p>Ch&iacute;nh v&igrave; thế, muốn quản l&yacute; chi ph&iacute; trong chuỗi cung ứng hiệu quả, đầu ti&ecirc;n chuy&ecirc;n vi&ecirc;n quản l&yacute; chuỗi cung ứng cần x&aacute;c định r&otilde; đ&acirc;u l&agrave; chi ph&iacute; n&ecirc;n tiếp tục duy tr&igrave;, đ&acirc;u l&agrave; chi ph&iacute; cần giảm v&agrave; đ&acirc;u l&agrave; chi ph&iacute; phải cắt. Kế tiếp, họ cần thực hiện c&aacute;c biện ph&aacute;p mạnh mẽ v&agrave; cương quyết nhằm cắt (hoặc giảm) những chi ph&iacute; cần thiết th&ocirc;ng qua việc cải tiến qui tr&igrave;nh để giảm thiểu những kh&acirc;u thừa v&agrave; điều phối hoạt động của chuỗi cung ứng hợp l&yacute; hơn. Chẳng hạn, họ c&oacute; thể &aacute;p dụng ch&iacute;nh s&aacute;ch tồn kho an to&agrave;n (safety stock) để duy tr&igrave; tồn kho ở mức hợp l&yacute; m&agrave; kh&ocirc;ng phải hy sinh chất lượng dịch vụ. B&ecirc;n cạnh đ&oacute;, chuy&ecirc;n vi&ecirc;n quản l&yacute; chuỗi cung ứng cũng cần tăng cường khả năng lập kế hoạch v&agrave; dự b&aacute;o để phản ứng kịp thời với c&aacute;c biến động cũng như n&acirc;ng cao chất lượng hệ thống th&ocirc;ng tin của chuỗi để gi&uacute;p c&aacute;c bộ phận phối hợp ăn &yacute; với nhau hơn.</p>\r\n<p><strong>Chuy&ecirc;n vi&ecirc;n quản l&yacute; chuỗi cung ứng - Học để vươn đến đỉnh cao sự nghiệp</strong><br />Quản l&yacute; chuỗi cung ứng l&agrave; một ng&agrave;nh nghề đầy biến động. V&igrave; thế, muốn đạt đến v&agrave; trụ vững ở đỉnh cao của nghề quản l&yacute; chuỗi cung ứng, bạn cần thường xuy&ecirc;n cập nhật kiến thức qua c&aacute;c kh&oacute;a đ&agrave;o tạo tổng qu&aacute;t về Quản l&yacute; Chuỗi cung ứng, c&aacute;c kh&oacute;a học chuy&ecirc;n s&acirc;u về từng bộ phận của chuỗi như <em>Quản l&yacute; mua h&agrave;ng, Quản l&yacute; v&agrave; kiểm so&aacute;t tồn kho, Hoạch định nguồn lực doanh nghiệp</em>&hellip;cũng như c&aacute;c kh&oacute;a học năng cao kỹ năng như kỹ năng truyền th&ocirc;ng, thuơng thuyết, l&agrave;m việc nh&oacute;m &hellip;</p>\r\n<p>Khi đ&atilde; vững kiến thức nền tảng, họ c&oacute; thể học tiếp c&aacute;c chương tr&igrave;nh n&acirc;ng cao như Thạc sĩ về Quản l&yacute; C&ocirc;ng nghiệp hay Quản l&yacute; Chuỗi cung ứng. Đương nhi&ecirc;n, nếu muốn cập nhật kiến thức li&ecirc;n tục như thế, bạn sẽ phải bỏ ra một khoản chi ph&iacute; đ&aacute;ng kể. Tuy nhi&ecirc;n, những g&igrave; bạn nhận được từ nghề n&agrave;y cũng rất xứng đ&aacute;ng. Mức lương h&agrave;ng th&aacute;ng của bạn sẽ dao động từ 500 - 4000 USD. Con đường thăng tiến của bạn cũng sẽ rất s&aacute;ng sủa. Bằng chứng l&agrave; nhiều CEO của c&aacute;c c&ocirc;ng ty lớn hiện nay c&oacute; xuất th&acirc;n từ ng&agrave;nh quản l&yacute; chuỗi cung ứng!</p>\r\n<p>C&aacute;ch đ&acirc;y hơn 2000 năm, Alexander Đại đế - một trong những nh&agrave; chiến lược qu&acirc;n sự vĩ đại nhất trong lịch sử nh&acirc;n loại - đ&atilde; từng tuy&ecirc;n bố &ldquo;Người l&agrave;m hậu cần cần biết rằng nếu chiến dịch của t&ocirc;i thất bại, họ sẽ l&agrave; người đầu ti&ecirc;n bị x&eacute;t xử&rdquo;. Điều n&agrave;y đ&atilde; cho thấy tầm quan trọng của hậu cần (tiền th&acirc;n của chuỗi cung ứng) đối với việc gầy dựng cơ nghiệp của c&aacute;c danh tướng ng&agrave;y xưa. Hiện nay, c&aacute;c doanh nh&acirc;n kh&ocirc;ng phải đụng đến binh đao như Alexander Đại đế. Tuy nhi&ecirc;n, kh&ocirc;ng phải v&igrave; thế m&agrave; cuộc chiến của họ tr&ecirc;n thương trường k&eacute;m khốc liệt hơn. Chỉ cần quản l&yacute; chuỗi cung ứng k&eacute;m hiệu quả, khiến gi&aacute; th&agrave;nh sản phẩm cao hơn đối thủ cạnh tranh một ch&uacute;t l&agrave; đủ để doanh nghiệp lao đao, đặc biệt khi kinh tế suy giảm. Ch&iacute;nh v&igrave; thế, họ lu&ocirc;n cần những chuy&ecirc;n vi&ecirc;n quản l&yacute; chuỗi cung ứng t&agrave;i năng.</p>', '2016-04-17 12:17:28', '2016-04-17 12:17:28'),
(5, 'Gọi tên Thành Công', '<p><em>Bạn c&ograve;n nhớ ng&agrave;y lễ tốt nghiệp đại học kh&ocirc;ng? Khi t&ecirc;n bạn được xướng l&ecirc;n v&agrave; thầy hiệu trưởng trao cho bạn tấm bằng tốt nghiệp qu&yacute; gi&aacute;, &iacute;t nhất bốn năm trời r&ograve;ng r&atilde;, bao cố gắng nỗ lực v&agrave; kết quả l&agrave; đ&acirc;y.</em></p>\r\n<p>Bằng đại học - &ldquo;tấm v&eacute;&rdquo; để bạn tự tin bước v&agrave;o đời! Cầm tấm bằng tr&ecirc;n tay, bạn nghĩ đến một việc l&agrave;m mơ ước, bạn sẽ mặc bộ đồ c&ocirc;ng sở lịch l&atilde;m, ngồi trước m&aacute;y vi t&iacute;nh, bạn email trao đổi với một kh&aacute;ch h&agrave;ng tiềm năng, l&ograve;ng đầy quyết t&acirc;m mang về hợp đồng lớn cho c&ocirc;ng ty&hellip;</p>\r\n<p>Ng&agrave;y 31 th&aacute;ng 10 năm&hellip; Đ&oacute; l&agrave; ng&agrave;y bạn kh&ocirc;ng thể n&agrave;o qu&ecirc;n v&agrave; kh&ocirc;ng bao giờ muốn qu&ecirc;n! Bạn nhận được th&aacute;ng lương đầu ti&ecirc;n của sự nghiệp! Bước ch&acirc;n vội v&atilde; đến nơi c&oacute; m&aacute;y ATM gần c&ocirc;ng ty nhất, l&ograve;ng n&ocirc;n nao v&agrave; tay run run nhấn số mật m&atilde;. Bạn nhẩm t&iacute;nh th&aacute;ng lương n&agrave;y sẽ chia th&agrave;nh 3 phần: phần biếu ba mẹ, phần dẫn hai đứa bạn th&acirc;n nhất đi ăn mừng v&agrave; phần cho những dự định tương lai. Vừa về đến nh&agrave;, ba mẹ đ&atilde; l&agrave;m sẵn một bữa cơm đầy những m&oacute;n bạn th&iacute;ch! Mẹ n&oacute;i: &ldquo;Ch&uacute;c mừng con!&rdquo;&hellip;</p>\r\n<p>Năm 31 tuổi, bạn dồn hết tr&iacute; lực triển khai một dự &aacute;n kinh doanh lớn sắp tới. Bước đầu, bạn đ&atilde; thỏa thuận th&agrave;nh c&ocirc;ng v&agrave; nắm trong tay một mạng lưới ph&acirc;n phối tốt. Bạn c&oacute; niềm tin m&atilde;nh liệt về kết quả của dự &aacute;n n&agrave;y. C&otilde; lẽ, v&igrave; đồng h&agrave;nh c&ugrave;ng bạn l&agrave; một nh&oacute;m l&agrave;m việc giỏi giang v&agrave; rất ăn &yacute;. C&ograve;n g&igrave; hơn khi bạn c&oacute; những người cộng sự tuyệt vời!<br /><br /><strong>Mỗi khoảnh khắc th&agrave;nh c&ocirc;ng, một sắc th&aacute;i cảm x&uacute;c</strong><br />Cuộc đời l&agrave; một chuỗi tiếp nối của những h&agrave;nh tr&igrave;nh. Tr&ecirc;n những h&agrave;nh tr&igrave;nh đ&oacute;, bạn đ&atilde; c&oacute; biết bao nhi&ecirc;u trải nghiệm tuyệt đẹp. Đ&oacute; l&agrave; những gi&acirc;y ph&uacute;t theo bạn suốt cuộc đời! Những gi&acirc;y ph&uacute;t l&agrave;m n&ecirc;n bạn - bạn của th&agrave;nh c&ocirc;ng v&agrave; hạnh ph&uacute;c!</p>\r\n<p>VietnamWorks muốn c&ugrave;ng bạn n&acirc;ng niu những khoảnh khắc v&agrave; cảm x&uacute;c th&agrave;nh c&ocirc;ng đ&oacute; với cuộc thi &ldquo;Mỗi khoảnh khắc , một sắc th&aacute;i cảm x&uacute;c&rdquo;. Như bạn Trần Hữu Tấn (Copywriter) đ&atilde; chọn bức ảnh khoảnh khoắc Tấn v&agrave; một người bạn được chọn v&agrave;o chung kết một cuộc thi quảng c&aacute;o. Với Tấn bức ảnh như lưu lại &ldquo;một kỷ niệm đẹp, một trong những chiến thắng đầu ti&ecirc;n trong sự nghiệp quảng c&aacute;o của m&igrave;nh&rdquo;.</p>\r\n<p>C&ograve;n với bạn Mai Văn Cảnh (Nh&acirc;n vi&ecirc;n triển khai ứng dụng phần mềm) khi tham gia chương tr&igrave;nh đ&atilde; chia sẻ &ldquo;Qua trang web VietnamWorks t&ocirc;i đ&atilde; t&igrave;m được việc l&agrave;m l&yacute; tưởng cho m&igrave;nh. V&agrave; t&ocirc;i tin rằng cũng c&oacute; rất nhiều c&aacute;c bạn kh&aacute;c giống như t&ocirc;i. Với t&ocirc;i hiện tại l&agrave; th&agrave;nh c&ocirc;ng bước đầu. T&ocirc;i rất vui v&igrave; điều đ&oacute;&rdquo;.</p>\r\n<p>H&atilde;y chia sẻ những khoảnh khắc th&agrave;nh c&ocirc;ng của bạn . Đơn giản, v&igrave; bạn xứng đ&aacute;ng với điều đ&oacute;!</p>', '2016-04-17 12:17:04', '2016-04-17 12:17:04'),
(6, 'Mẹo nhỏ giúp bạn vững bước trong nghề nghiệp', '<p>1. C&ocirc;ng khai cho sếp v&agrave; đồng nghiệp biết khả năng v&agrave; tr&igrave;nh độ của bạn. Tự tin với năng lực bản th&acirc;n cũng khiến sếp v&agrave; những người trong cơ quan đ&aacute;nh gi&aacute; cao năng lực của bạn.<br /><br />2.Tận t&acirc;m k&egrave;m cặp v&agrave; n&acirc;ng đỡ &ldquo;đ&agrave;n em&rdquo; v&agrave; những nh&acirc;n vi&ecirc;n mới, chưa c&oacute; kinh nghiệm tại cơ quan. Đừng nghĩ bảo thủ cho đi nghĩa l&agrave; mất, kiến thức v&agrave; l&ograve;ng tốt l&agrave; thứ cho đi c&agrave;ng nhiều ta nhận lại c&agrave;ng lớn. V&agrave; c&aacute;i qu&yacute; nhất m&agrave; bạn nhận được l&agrave; cảm t&igrave;nh của sếp v&agrave; mối quan hệ tốt đẹp của c&aacute;c đồng nghiệp.<br /><br />3. Hy sinh cho c&ocirc;ng việc, sẵn s&agrave;n l&agrave;m th&ecirc;m giờ nếu cần: H&atilde;y l&agrave;m việc bằng l&ograve;ng nhiệt t&igrave;nh v&agrave; tinh thần tr&aacute;ch nhiệm cao. Khi c&ocirc;ng việc qu&aacute; nhiều, đừng ngại phải ở lại cơ quan l&agrave;m th&ecirc;m giờ hoặc giải quyết c&ocirc;ng việc ở nh&agrave;. Đừng vội đ&ograve;i hỏi c&ocirc;ng lao trong việc l&agrave;m th&ecirc;m n&agrave;y cho đến khi n&agrave;o năng lực của bạn được sếp c&ocirc;ng nhận.<br /><br />4. Đừng chơi hay l&agrave;m những việc v&ocirc; bổ khi cơ quan c&oacute; thời gian rảnh. H&atilde;y tận dụng thời gian n&agrave;y để học tập, n&acirc;ng cao kiến thức bản th&acirc;n. Sếp sẽ rất h&agrave;i l&ograve;ng nếu như trong l&uacute;c rảnh bạn thường xuy&ecirc;n bổ t&uacute;c kiến thức chuy&ecirc;n m&ocirc;n, ngoại ngữ...<br /><br />5. Đừng tỏ ra ki&ecirc;u ngạo khi bạn được đ&aacute;nh gi&aacute; cao hay đề bạt. Điều đ&oacute; kh&ocirc;ng c&oacute; nghĩa l&agrave; bạn kh&ocirc;ng &yacute; thức được địa vị mới của m&igrave;nh. Bạn cần để mọi người &yacute; thức được điều đ&oacute; những h&atilde;y cố gắng duy tr&igrave; mối quan hệ th&acirc;n thiện với đồng nghiệp trước kia. V&igrave; ch&iacute;nh sự y&ecirc;u qu&yacute; của đồng nghiệp v&agrave; thủ trưởng l&agrave; cơ hội để bạn thăng tiến l&ecirc;n nấc thang cao hơn tr&ecirc;n con đường c&ocirc;ng danh sự nghiệp của m&igrave;nh.</p>', '2016-04-17 12:17:33', '2016-04-17 12:17:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cv`
--
ALTER TABLE `cv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `gender` (`gender`);

--
-- Indexes for table `employer`
--
ALTER TABLE `employer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_id` (`employer_id`),
  ADD KEY `jobcat_id` (`jobcat_id`);

--
-- Indexes for table `jobcate`
--
ALTER TABLE `jobcate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `vg`
--
ALTER TABLE `vg`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cv`
--
ALTER TABLE `cv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `employer`
--
ALTER TABLE `employer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jobcate`
--
ALTER TABLE `jobcate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vg`
--
ALTER TABLE `vg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

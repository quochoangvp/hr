<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "employer".
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $coname
 * @property string $cologo
 * @property string $codetails
 * @property string $coaddress
 * @property string $cowebsite
 * @property string $cocontact
 */
class Employer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'coname', 'codetails', 'coaddress', 'cowebsite', 'cocontact'], 'required'],
            [['codetails'], 'string'],
            [['email', 'password', 'cowebsite'], 'string', 'max' => 40],
            [['coname'], 'string', 'max' => 120],
            // [['cologo'], 'string', 'max' => 100],
            [['coaddress', 'cocontact'], 'string', 'max' => 255],
            [['cologo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'STT',
            'email' => 'Email',
            'password' => 'Mật khẩu',
            'coname' => 'Tên công ty',
            'cologo' => 'Logo công ty',
            'codetails' => 'Thông tin về công ty',
            'coaddress' => 'Địa chỉ công ty',
            'cowebsite' => 'Website',
            'cocontact' => 'Liên hệ',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->cologo->saveAs(Yii::getAlias('@frontend/web/images/uploads/' . $this->cologo->baseName . '.' . $this->cologo->extension));
            $this->cologo = $this->cologo;
            return true;
        } else {
            return false;
        }
    }
}

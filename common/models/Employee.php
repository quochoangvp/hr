<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property integer $id
 * @property string $fullname
 * @property integer $gender
 * @property string $birthday
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string $address
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname', 'gender', 'birthday', 'email', 'password', 'phone', 'address'], 'required'],
            [['gender'], 'integer'],
            [['birthday'], 'safe'],
            [['fullname', 'email'], 'string', 'max' => 40],
            [['password'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 11],
            [['address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Họ tên',
            'gender' => 'Giới tính',
            'birthday' => 'Ngày sinh',
            'email' => 'Email',
            'password' => 'Mật khẩu',
            'phone' => 'Số điện thoại',
            'address' => 'Địa chỉ',
        ];
    }
}

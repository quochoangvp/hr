<?php

namespace common\models;

use Yii;
use common\models\Jobcate;
use common\models\Employer;

/**
 * This is the model class for table "job".
 *
 * @property integer $id
 * @property integer $employer_id
 * @property string $title
 * @property integer $jobcat_id
 * @property string $deadline
 * @property string $created_at
 * @property string $updated_at
 * @property integer $apply_num
 */
class Job extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employer_id', 'title', 'body', 'jobcat_id', 'deadline'], 'required'],
            [['employer_id', 'jobcat_id', 'apply_num'], 'integer'],
            [['deadline', 'created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['body'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employer_id' => 'Nhà tuyển dụng',
            'title' => 'Tiêu đề',
            'body' => 'Nội dung',
            'jobcat_id' => 'Ngành nghề',
            'deadline' => 'Ngày hết hạn',
            'created_at' => 'Ngày đăng',
            'updated_at' => 'Cập nhật gần đây',
            'apply_num' => 'Số ứng viên ứng tuyển',
        ];
    }

    public function getJobCate()
    {
        return $this->hasOne(Jobcate::className(), ['id' => 'jobcat_id']);
    }

    public function getEmployer()
    {
        return $this->hasOne(Employer::className(), ['id' => 'employer_id']);
    }
}

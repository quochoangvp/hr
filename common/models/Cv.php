<?php

namespace common\models;

use Yii;
use common\models\Employee;
use common\models\Jobcate;

/**
 * This is the model class for table "cv".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property string $title
 * @property string $body
 * @property integer $jobcat_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $countview
 * @property integer $attachments
 */
class Cv extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cv';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'title', 'body', 'jobcat_id'], 'required'],
            [['employee_id', 'jobcat_id', 'countview'], 'integer'],
            [['body'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['attachments'], 'file', 'skipOnEmpty' => true]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Ứng viên',
            'title' => 'Tiêu đề',
            'body' => 'Nội dung',
            'jobcat_id' => 'Ngành nghề',
            'created_at' => 'Ngày đăng',
            'updated_at' => 'Cập nhật cuối',
            'countview' => 'Lượt xem',
            'attachments' => 'Tệp đính kèm',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->attachments->saveAs(Yii::getAlias('@frontend/web/documents/uploads/' . $this->attachments->baseName . '.' . $this->attachments->extension));
            $this->attachments = $this->attachments;
            return true;
        } else {
            return false;
        }
    }

    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    public function getJobCate()
    {
        return $this->hasOne(Jobcate::className(), ['id' => 'jobcat_id']);
    }
}

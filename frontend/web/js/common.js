var csrfToken = $('meta[name="csrf-token"]').attr("content");
$(document).ready(function() {
    $('.datepicker').datepicker();
	$('#menunav a').each(function(index, value) {
		if ($(this).prop('href') === window.location.href) {
			$(this).addClass('active');
		}
	});

	$('#employerLoginForm').submit(function(e) {
	    $('.err_register').html('').hide();
	    e.preventDefault();
	    form = $(this);
	    var url = form.attr('action');
	    $.post(url, form.serialize() + '&_csrf=' + csrfToken, function(rs) {
            if(rs.status) {
                 location.reload();
            } else {
                $('.err_register').html('<p>Email hoặc mật khẩu không đúng!</p>').show();
            }
        }, 'json');
	});

	$('#employerRegisterForm').submit(function(e) {
	    $('.err_register').html('').hide();
	    e.preventDefault();
	    form = $(this);
	    var url = form.attr('action');
	    var data = new FormData(form[0]);
        data.append('file', $('[name="Employer[cologo]"]')[0].files[0]);
        data.append('_csrf', csrfToken);
	    $.ajax({
        	url: url,
        	type: 'POST',
        	dataType: 'json',
        	data: data,
        	processData: false,
        	contentType: false
        })
        .done(function(rs) {
        	if (!rs.status) {
                $('.err_register').html(rs.message).show();
            } else {
                alert(rs.message);
                window.location.href = rs.url;
            }
        })
        .fail(function() {
        	alert('Có lỗi xảy ra, không thể đăng ký thông tin doanh nghiệp');
        })
        .always(function() {
        	console.log("complete");
        });
	});

	$('#employeeLoginForm').submit(function(e) {
	    $('.err_register').html('').hide();
	    e.preventDefault();
	    form = $(this);
	    var url = form.attr('action');
	    $.post(url, form.serialize() + '&_csrf=' + csrfToken, function(rs) {
            if(rs.status) {
                 window.location.href = rs.url;
            } else {
                $('.err_register').html('<p>Email hoặc mật khẩu không đúng!</p>').show();
            }
        }, 'json');
	});

	$('#addNewJob').submit(function(e) {
	    e.preventDefault();
	    form = $(this);
        var url = form.attr('action');
        $.post(url, form.serialize() + '&_csrf=' + csrfToken, function(rs) {
            if(rs.status) {
                $('.success_register').html('<p>Đăng tin thành công, <a href="' + rs.url  + '">xem tin</a></p>').show();
                form.trigger('reset');
                tinyMCE.activeEditor.setContent('');
            } else {
                $('.err_register').html('<p>Không thể đăng tin, xin vui lòng thử lại!').show();
            }
        }, 'json');
	});
});

function doSearch (url) {
	var cateId = $('select[name="category"]').val();
	var keyword = $('input[name="keyword"]').val();
	window.location.href = url + '?keyword=' + keyword + '&category=' + cateId;
}
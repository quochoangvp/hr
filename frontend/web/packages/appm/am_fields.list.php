<?php session_start();?>
<?php include("connect.php")?>
<?php
# - check if fields are retrieved
$id = @$_REQUEST["tableid"];
$fnum = $oCls->GetFieldValue("select 1 from am_fields where tableid=$id");
if (intval($fnum)<1) header("Location: am_tables.read.php?tableid=$id");
# -
$a = @$_REQUEST["a"];
$filter = @$_REQUEST["filter"];
if ($filter!=""){
	$_SESSION["am_fields_filter"] = $filter;
} 
if ($a=="U"){
	# update
	$aflddesc = @$_REQUEST["flddesc"];
	$afldid = @$_REQUEST["fldid"];
	$aclist = @$_REQUEST["clist"]; if (!is_array($aclist)) $aclist = array();
	$acview = @$_REQUEST["cview"]; if (!is_array($acview)) $acview = array();
	$acedit = @$_REQUEST["cedit"]; if (!is_array($acedit)) $acedit = array();
	$k = 0;
	foreach($afldid as $v){
		$fList["fldid"]	= $v;
		$fList["flddesc"] = $aflddesc[$k];
		$fList["fldlist"] = (in_array($v, $aclist))?1:0;
		$fList["fldview"] = (in_array($v, $acview))?1:0;
		$fList["fldedit"] = (in_array($v, $acedit))?1:0;
		$oConn->Replace("am_fields", $fList, "fldid", true);
		$k++;
	}
}//end update
if ($a=="M"){
	$o = @$_REQUEST["o"];
	$f = @$_REQUEST["fldid"];
	$tmprs = $oConn->Execute("select fldorder, tableid from am_fields where fldid=$f");
	if (!$tmprs) die("Column not found !");
	if ($tmprs->RecordCount()<1) die("Field not found !");
	$tab = $tmprs->fields["tableid"];
	$order = $tmprs->fields["fldorder"]; 
	$count = $oCls->GetFieldValue("select count(*) from am_fields where tableid=".$tab);
	if ($o=="up"){
		if ($order>1){
			$order--;
			$pf = $oCls->GetFieldValue("select fldid from am_fields where fldorder=$order and tableid=$tab");
			$sql1 = "update am_fields set fldorder=fldorder-1 where fldid=$f";
			$sql2 = "update am_fields set fldorder=fldorder+1 where fldid=$pf";
			$oConn->Execute($sql1);
			$oConn->Execute($sql2);
		}
	}// end move up
	if ($o=="dn"){
		if ($order<$count){
			$order++;
			$pf = $oCls->GetFieldValue("select fldid from am_fields where fldorder=$order and tableid=$tab");
			$sql1 = "update am_fields set fldorder=fldorder+1 where fldid=$f";
			$sql2 = "update am_fields set fldorder=fldorder-1 where fldid=$pf";
			$oConn->Execute($sql1);
			$oConn->Execute($sql2);
		}
	}// end move down
}
?>
<?php
function getFLDS($_tab, $frm){
	global $oConn; 
	$t = new XTemplate($frm);
	$tmprs = $oConn->Execute("select * from am_tables where tableid=$_tab");
	$sql = "select * from am_fields where tableid=$_tab";
	$filter = "";
	switch (@$_SESSION["am_fields_filter"]){
		case "": $filter = ""; break;
		case "all": $filter = ""; break;
		case "edit": $filter = "fldedit=1"; break;
		case "view": $filter = "fldview=1"; break;
		case "list": $filter = "fldlist=1"; break;
	}
	if ($filter!="") $sql.= " AND ".$filter;
	$sql.= " order by fldorder";
	$rs = $oConn->Execute($sql);
	$uparr = "<img border=0 src='images/uparr.jpg' alt='Move Up'>";
	$dnarr = "<img border=0 src='images/downarr.jpg' alt='Move Down'>";
	$max = $rs->RecordCount(); $num=0;
	while (!$rs->EOF){
		$row = $rs->fields;
		if (strpos($row["fldoptions"], "NOTNULL")>0) $row["fldname"].= "&nbsp;&nbsp;<font color=lightgrey size=1>(NOTNULL)</font>";
		$order = $row["fldorder"];
		$fldval = $row["fldid"];
		$t->assign("am_fields", $row);
		$img = "<input type='checkbox' name='clist[]' value='$fldval'";		
		$t->assign("list", ($row["fldlist"]==1)?$img." checked>":$img.">");

		$img = "<input type='checkbox' name='cview[]' value='$fldval'";		
		$t->assign("view", ($row["fldview"]==1)?$img." checked>":$img.">");

		$img = "<input type='checkbox' name='cedit[]' value='$fldval'";		
		$t->assign("edit", ($row["fldedit"]==1)?$img." checked>":$img.">");
		# Now show icon to move field up and down
		$move = "";
		if ($order>1){
			$move.="<a href=\"am_fields.list.php?a=M&o=up&fldid=$fldval\">".$uparr."</a>";
		} else $move.=$uparr;
		if ($order<$max){
			$move.="<a href=\"am_fields.list.php?a=M&o=dn&fldid=$fldval\">".$dnarr."</a>";
		} else $move.=$dnarr;
		$t->assign("move", $move);
		$tdclass = "tdlight"; if ($num%2 == 0) $tdclass = "tdgrey";
		$t->assign("tdclass", $tdclass);
		$t->parse("main.fields");
		$rs->MoveNext(); $num++;
	}
	$t->assign("m_tableid", $_tab);
	$t->assign("m_tablename", $tmprs->fields["tablename"]);
	$t->assign("m_dbname", $tmprs->fields["dbname"]);
	$rs->Close(); $tmprs->Close();
	$t->parse("main");
	$rs->Close();
	return $t->text("main");
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<?php 
$id = @$_REQUEST["tableid"];
if ($id=="") {
	$id = @$_SESSION["am_tableid"];
} else {
	$_SESSION["am_tableid"] = $id;
}
if ($id=="") die("No table selected !");
$frm = "forms/field.list.htm";
echo getFLDS($id, $frm);
?>
</body>
</html>

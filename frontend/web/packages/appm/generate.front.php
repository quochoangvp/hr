<?php session_start();?>
<?php include("connect.php")?>
<?php include("clsGenerate.php")?>
<?php 
$tables = @$_REQUEST["tables"];
if ($tables=="") die("No table selected !");
if (!is_array($tables)){
	$tables = explode(",", $tables);
}
if (count($tables)<1) header("Location: am_tables.list.php");
$dbn = @$_SESSION["am_dbname"]; if ($dbn=="") header("Location: index.php");
$dbrs = $oConn->Execute("select * from am_dbs where dbname='$dbn'");
$dir = $dbrs->fields["directory"]; if ($dir=="") $dir = $dbn;
$a = @$_REQUEST["a"];
if ($a=="G"){
	$dir = strtolower(@$_REQUEST["dir"]); 
	if (in_array($dir, array("adodb", "scripts", "template", "appmaker"))) die("Invalid directory !");
	$cls = new AMGenerate($oConn, $dbn);
	$cls->mode = "front-end";
	$cls->directory = $dir;
	$pth = dirname(dirname(__FILE__));
	@mkdir($pth."/".$dir); @mkdir($pth."/".$dir."/forms");
	$pth.="/".$dir;
	$cls->savedpath = $pth;
	foreach($tables as $id){
		$cls->generateListForm($id);
		$cls->generateListPage($id);
	
		$cls->generateViewForm($id);
		$cls->generateViewPage($id);
	
		$cls->generateEditForm($id);
		$cls->generateEditPage($id);
	
		$cls->generateAddPage($id);
		$cls->generateDeletePage($id);
	}
	$cls->generateCommonPage($tables);
	# update directory
	$oConn->Execute("update am_tables set directory='$dir' where tableid in (".implode(",", $tables).")");
	# now generate master page
	$tabs = implode(",", $tables);
	$rs = $oConn->Execute("select * from am_tables where dbname='$dbn' and (mastertable<>'' or mastertable2<>'' or mastertable3<>'') and tableid in ($tabs)");
	$tabs = "";
	while (!$rs->EOF){
		$row = $rs->fields;
		$tabs.=($row["mastertable"]!="") ? $row["mastertable"]."," : "";
		$tabs.=($row["mastertable2"]!="") ? $row["mastertable2"]."," : "";
		$tabs.=($row["mastertable3"]!="") ? $row["mastertable3"]."," : "";
		$rs->MoveNext();
	}
	if ($tabs!="") $tabs = substr($tabs, 0, strlen($tabs)-1);
	$rs->Close();
	if ($tabs!=""){
		$tabs = "'".str_replace(",", "','", $tabs)."'";
		$rs = $oConn->Execute("select tableid from am_tables where dbname='$dbn' and tablename in ($tabs)");
		while (!$rs->EOF){
			$tabid = $rs->fields["tableid"];
			$cls->generateListForm($tabid, "master");
			$cls->generateMasterPage($tabid);
			$rs->MoveNext();
		}
		$rs->Close();
	}
	# end of master table
}//end if $a=="G"
# get previous directory
$tab1 = $tables[0];
$_dir = $oCls->GetFieldValue("select directory from am_tables where tableid=$tab1");
if ($_dir!="") $dir = $_dir;
if (is_array($tables)){
	$tables = implode(",", $tables);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Generate code for back-end</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<?php if ($a=="G"){?>
<center><h4>Generation done ! <a href="am_tables.list.php">Go back</a>.</h4></center>
<?php 
} else {
?>
<h3>&nbsp;GENERATE FOR FRONT-END</h3>

<form action="generate.front.php?a=G" method="post" name="frmGen" id="frmGen">
  <table width="100%"  border="0" cellspacing="2" cellpadding="2">
    <tr>
      <td align="right" valign="top" nowrap style="border-bottom:1px dotted lightgrey">Tables to be generated:</td>
      <td style="border-bottom:1px dotted lightgrey">
<?php
$tabrs = $oConn->Execute("select * from am_tables where tableid in ($tables)");
while(!$tabrs->EOF){
	$tab = $tabrs->fields["tablename"];
	echo "+ <b>".$tab."</b><br>";
	$tabrs->MoveNext();
}
$tabrs->Close();
?>	  
	  &nbsp;</td>
    </tr>
    <tr>
      <td width="22%" align="right">Directory:</td>
      <td width="78%"><input name="dir" type="text" id="dir" value="<?php echo $dir;?>" size="30">
      <input name="tables" type="hidden" id="tables" value="<?php echo $tables;?>"></td>
    </tr>
    <tr>
      <td align="right">&nbsp;</td>
      <td><input type="submit" name="Submit" value="Generate Now"></td>
    </tr>
  </table>
</form>
<?php 
}
?>
</body>
</html>

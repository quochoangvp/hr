<!-- BEGIN: main --><?php 
session_start();
include("connect.php");
$table = "{table}"; //change to your table 
$form = "{form}"; //change to your form name
$flds = "{fields}"; //fields being requested separated by comma
$pfld = "{pkey}"; //primary key field
$returnPage = "{returnpage}";//return page after updating
$dateflds = "{dateflds}"; //fields of date to check
$numflds = "{numflds}"; //fields of number to check
$editorflds = "{editorflds}";
$ruleflds = array({ruleflds}); // fld=>NOTNULL
$fileflds = array({fileflds}); // fld=>array(fldtype, filetype, filelocation)
$menuArr = array({menu});//menus in form (combo boxes)
$staticMenu = array({staticmenu});
$checkboxes = "{checkboxes}";
$quote="{quote}";
$defaultValueArr = array({defaultvalues});//default values on update
$multiselectArr = array({multiselect}); //multi selection controls
$fieldList = array();
$msg = "";

$a = @$_REQUEST["a"]; 
if ($a == "U"){ //form submitted
	# request form fields
	$flds = str_replace(";", ",", $flds);
	$fldArr = explode(",", $flds);
	for ($i=0;$i<count($fldArr); $i++){//request fields
		$fld = $fldArr[$i];
		if (!in_array($fld, array_keys($fileflds))){
			$req = @$_REQUEST[$fld]; if (is_array($req)) $req = implode(",", $req);
			$fieldList[$fld] = $req;
		}	
	}//end for request fields
	# primary key
	if ($fieldList[$pfld]=="") $fieldList[$pfld] = $oConn->GenID(); //primary key value
	# foreign key
	$fkey = @$_SESSION[$table."_foreignkey"];
	if ($fkey!="" && !in_array($fkey, $fldArr)) $fieldList[$fkey] = @$_SESSION[$table."_foreignkey_value"];
	# file fields
	foreach($fileflds as $fld=>$arr){
		$file = @$_FILES[$fld];
		if ($file["tmp_name"]!="" && $file["tmp_name"]!="none"){
			$loc = $arr["filelocation"]."/".$fieldList[$pfld]."_".$file["name"];
			$oCls->makeDir($arr["filelocation"]);
			move_uploaded_file($file["tmp_name"], $oCls->appPath()."/".$loc);
			$fieldList[$fld] = $loc;
			@unlink($file["tmp_name"]);
		}//end if has file uploaded
	}//end of foreach fileflds
	if ($numflds<>""){//check numeric fields
		$fldArr = explode(",", $numflds);
		foreach($fldArr as $fld){
			if (!is_numeric($fieldList[$fld])) $fieldList[$fld] = 0;
		}
	}//
	
	if ($dateflds<>""){//check date fields
		$fldArr = explode(",", $dateflds);
		foreach($fldArr as $fld) $fieldList[$fld] = $oCls->formatDate($fieldList[$fld]);
	}//
	# default values
	foreach($defaultValueArr as $k=>$v){
		if (@$_REQUEST[$k]==""){
			$fieldList[$k] = $oCls->getDefaultValue($v);
		}
	}
	# Now update table
	if ($oCls->Insert($table, $fieldList)){
		header("Location: $returnPage");
		exit;
	} else {
		$msg = "Can not insert data: ".$oCls->ErrorMsg();
		$fieldList[$pfld] = "";
	}
}//end if form submitted

//show form to enter data
$tpl = new XTemplate($form);
# if fieldList exists
if (sizeof($fieldList)>0) $tpl->assign($table, $fieldList);
# for comboboxes
foreach($menuArr as $k=>$v){//generate menus
	$rs = $oConn->Execute($v);
	if ($rs) {
		$dval = ""; 
		# if $k is in default value list
		if (in_array($k, array_keys($defaultValueArr))){
			$dval = $oCls->getDefaultValue($defaultValueArr[$k]);
		}	
		# if $k is a foreign key
		if ($k==@$_SESSION[$table."_foreignkey"] && @$_SESSION[$table."_foreignkey_value"]!="") {
			$dval = $_SESSION[$table."_foreignkey_value"];
		} else {
			if (@$fieldList[$k]!="") $dval = $fieldList[$k];
		}
		$tpl->assign($k, $oCls->genMenuRs($k, $rs, $dval));
		$rs->Close();
	}	
}
# for static menus
foreach($staticMenu as $k=>$vArr){//generate menus
	$dval = ""; if (@$fieldList[$k]!="") $dval = $fieldList[$k];
	$tpl->assign($k, $oCls->genMenuKey($k, $vArr, $dval, true));
}

# for checkboxes
$checkArr = explode(",", $checkboxes);
foreach($checkArr as $v) $tpl->assign($v, $oCls->genCheckbox($v, "1", @$fieldList[$v]));

# multi selections
foreach($multiselectArr as $k=>$sql){
	$dval = ""; if (@$fieldList[$k]!="") $dval = $fieldList[$k];
	$rs = $oConn->Execute($sql);
	if ($rs) $tpl->assign($k, $oCls->genMultiCheckboxes($k, $rs, $dval));
} //end for multi selections

# form action
$tpl->assign("form_action", @$_SERVER['PHP_SELF']."?a=U");
# dateflds and numflds
$tpl->assign("dateflds", $dateflds);
$tpl->assign("numflds", $numflds);
# not null fields
$nnflds = "";
foreach($ruleflds as $fld=>$v){
	if ($v=="NOTNULL") $nnflds.=$fld.",";
}
if ($nnflds!="") $nnflds = substr($nnflds, 0, strlen($nnflds)-1);
$tpl->assign("nnflds", $nnflds);
# labels
$tpl->assign("common", $commonLabels);
$tpl->assign("labels", $oCls->loadTabLabels($table));

# render the page
{HEADER}

if ($msg!="") {
	echo $oCls->genAlert($msg);
}
# parse and out
$tpl->parse("main");
$tpl->out("main");
if ($editorflds!=""){
	require_once($oCls->appPath()."/packages/ckeditor/ckeditor.php");
	$CKEditor = new CKEditor();
	$CKEditor->basePath = $oCls->basePath()."/packages/ckeditor/";
	$editorArr = explode(",", $editorflds);
	foreach($editorArr as $editorid){
		$CKEditor->replace($editorid);
	}//
}//

{FOOTER}
?>
<!-- END: main -->
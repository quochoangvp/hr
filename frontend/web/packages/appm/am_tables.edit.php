<?php session_start();?>
<?php include("connect.php");?>
<?php
$key = @$_REQUEST["tableid"]; //key parameter
?>
<?php
$table = "am_tables"; //change to your table 
$form = "forms/am_tables.edit.htm"; //change to your form name
$flds = "tableid,ordernum,tabledesc,fldorder,ordertype,haslist,hasedit,hasview,hasdelete,";
$flds.= "haslocked,mastertable,masterpkey,mastertable2,masterpkey2,mastertable3,masterpkey3"; //fields being requested separated by comma
$pfld = "tableid"; //primary key field
$dateflds = ""; //fields of date to check
$numflds = "hasedit,hasview,haslist,hasdelete,haslocked,ordernum"; //fields of number to check
$array = "am_tables";//change to array name in form
$menuArr = array(
	"mastertable"=>"select tablename, tablename from am_tables where dbname='".@$_SESSION["am_dbname"]."' order by tablename",
	"masterpkey"=>"select fldname, fldname from am_fields where tableid=$key order by fldname",
	"mastertable2"=>"select tablename, tablename from am_tables where dbname='".@$_SESSION["am_dbname"]."' order by tablename",
	"masterpkey2"=>"select fldname, fldname from am_fields where tableid=$key order by fldname",
	"mastertable3"=>"select tablename, tablename from am_tables where dbname='".@$_SESSION["am_dbname"]."' order by tablename",
	"masterpkey3"=>"select fldname, fldname from am_fields where tableid=$key order by fldname"
);//menus in form (combo boxes)
$returnPage = "am_tables.list.php";
# $oConn->debug=true;
$a = @$_REQUEST["a"];
if ($a == "U"){ //form submitted
	$flds = str_replace(";", ",", $flds);
	$fldArr = explode(",", $flds);
	$fieldList = array();
	for ($i=0;$i<count($fldArr); $i++){//request fields
		$fieldList[$fldArr[$i]] = @$_REQUEST[$fldArr[$i]];
	}
	
	if ($numflds<>""){//check numeric fields
		$fldArr = explode(",", $numflds);
		for ($i=0;$i<count($fldArr); $i++){
			if ($fieldList[$fldArr[$i]]=="") $fieldList[$fldArr[$i]] = 0;			
			if (!is_numeric($fieldList[$fldArr[$i]])) $fieldList[$fldArr[$i]] = 0;
		}
	}
	
	if ($dateflds<>""){//check date fields
		$fldArr = explode(",", $dateflds);
		for ($i=0;$i<count($fldArr); $i++){
			$fieldList[$fldArr[$i]] = str_replace("'", "", $oConn->DBDate($oCls->formatDate($fieldList[$fldArr[$i]])));
		}
	}
	//Now generate SQL command and then execute
	if ($oConn->Replace($table, $fieldList, $pfld, true)){
		header("Location: $returnPage");
		exit;
	} else {
		die("Can not update data: ".$oConn->ErrorMsg());
	}
}//end if form submitted
?>
<?php include("header.inc.php")?>
<?php
if ($key=="") die("No table selected !");
//show form to enter data
$mainrs = $oConn->Execute("select * from ".$table." where ".$pfld."='".$key."'");
$tpl = new XTemplate($form);
$row = $mainrs->fields;
if ($dateflds<>""){//check date fields
	$fldArr = explode(",", $dateflds);
	for ($i=0;$i<count($fldArr); $i++){
		$row[$fldArr[$i]] = $oCls->userDate($row[$fldArr[$i]]);
	}
}//end if dateflds	
$tpl->assign($array, $row);
foreach($menuArr as $k=>$v){//generate menus
	$rs = $oConn->Execute($v);
	if ($rs){
		$tpl->assign($k, $rs->GetMenu2($k, $mainrs->fields[$k]));
		$rs->Close();
	}
}
# update Check
$checks = array("haslist", "hasedit", "hasview", "hasdelete", "haslocked");
foreach($checks as $v){
	if ($row[$v]==1) $tpl->assign($v."check", " checked");
}
# update Menu
if ($row["ordertype"]=="desc") $tpl->assign("descselected", " selected");
$tpl->parse("main");
$tpl->out("main");
$mainrs->Close();
?>
<?php include("footer.inc.php")?>
<!-- BEGIN: main --><?php 
session_start();
include("connect.php");

//-request/get parameters here
$table = "{table}";
$pfld = "{pkey}";
$detailTables = array({detailTables});
$deleteDetails = true; # change to false if not to delete detail tables
$form = "forms/confirm.delete.htm";
$returnPage = "{returnpage}";//return page after deleting
$quote="{quote}";
$key = @$_REQUEST["key"];
if (is_array($key)) $key = implode(",", $key);
if ($key=="") {
	header("Location: ".$returnPage); exit;
}
$a = @$_REQUEST["a"];
if ($a=="D"){//confirm delete
	$delArr = explode(",", $key);
	for ($i=0;$i<count($delArr);$i++){
		# delete row from table
		$cond = $pfld."=".$quote.$delArr[$i].$quote;
		$oCls->Delete($table, $cond);
		# delete rows from detail tables
		if ($deleteDetails){
			foreach($detailTables as $tab=>$fkey){
				$cond = $fkey."=".$quote.$delArr[$i].$quote;
				$oCls->Delete($tab, $cond);
			}//end delete detail tables
		}//end if delete detail
	}//end for loop by keys
	header("Location: ".$returnPage); exit;
}

//-display content here
$tpl = new XTemplate($form);
$tpl->assign("key", $key);
$tpl->assign("form_action", @$_SERVER['PHP_SELF']."?a=D");
# labels
$tpl->assign("common", $commonLabels);
$tpl->assign("labels", $oCls->loadTabLabels($table));
# parse and out
{HEADER}
$tpl->parse("main"); $tpl->out("main");
{FOOTER}
?>
<!-- END: main -->
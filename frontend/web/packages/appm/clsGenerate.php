<?php 
class AMGenerate extends Common{
	var $conn;
	var $dbname;
	var $dbtype;
	var $dbdesc;
	var $directory;
	var $labels = array(
		"label_page"=>"{common.page}",
		"label_delete"=>"{common.deleteselected}",
		"label_selected"=>"{common.selected}",
		"label_view"=>"{common.view}",
		"label_edit"=>"{common.edit}",
		"label_detail"=>"{common.detail}",
		"label_save"=>"{common.save}",
		"label_add"=>"{common.add}",
		"label_goback"=>"{common.goback}",
		"label_cancel"=>"{common.cancel}"
		);
	var $srcMarks = array("(--", "--)", "[-", "-]");
	var $dstMarks = array("<!--", "-->", "{", "}");
	var $savedpath = "";
	var $apppath="";
	var $controlWidth = "40";
	var $pageSize = 20;
	var $defaultTarget = "_____frameContent";
	var $mode = "back-end";
	var $dot = "<font color=red size=3><b>&middot;</b></font>";
	function AMGenerate($c=false, $db=""){
		$this->dbname = $db;
		if ($c) {
			$this->conn = $c;
			if ($db!="") {
				$this->getDirectory($db);
			}	
		} //end if connected	
		$this->apppath = dirname(dirname(__FILE__));
	}//end of constructor
	function loadLabels(){
		$fileContents = @file_get_contents("labels.inc");
		if ($fileContents=="") return;
		$lArr = explode("\n", $fileContents);
		foreach($lArr as $v){
			$vArr = explode("=", $v);
			if (count($vArr)==2){
				$this->labels[trim($vArr[0])] = trim($vArr[1]);
			}
		}//end for
	}//end of loadLabels
	function saveToFile($ss, $fn){
		$h = fopen($fn, "w");
		if ($h){
			fwrite($h, $ss);
			fclose($h);
			return true;
		}
		return false;
	}//end of savetofile
	function getDirectory($dbn){
		$dtmp = $this->conn->Execute("select directory, dbdesc, dbtype from am_dbs where dbname='$dbn'");
		$this->directory = $dtmp->fields["directory"];
		$this->dbdesc = $dtmp->fields["dbdesc"];
		$this->dbtype = $dtmp->fields["dbtype"];
		if ($this->directory=="") $this->directory = $dbn;
		$dtmp->Close();
		return $this->directory;
	}
	function generateListForm($tabid, $filemode=""){
		$frm = "forms/page.list.htm"; if ($filemode=="master") $frm = "forms/master.list.htm";
		$tabrs = $this->conn->Execute("select * from am_tables where tableid=$tabid");
		if (!$tabrs) return "";
		$fldrs = $this->conn->Execute("select * from am_fields where tableid=$tabid and fldlist=1 order by fldorder");
		if (!$fldrs) return "";
		if ($fldrs->RecordCount()<1) {
			$fldrs->Close();
			return "";
		}
		if ($tabrs->fields["haslist"]!=1) return;
		# check if editable
		$flddelete = ($tabrs->fields["hasdelete"]==1);
		$fldview = ($tabrs->fields["hasview"]==1);
		$fldedit = ($tabrs->fields["hasedit"]==1);
		$tabname = $tabrs->fields["tablename"];
		$tabpkey = $tabrs->fields["tablepkey"];
		if ($tabpkey=="" || strpos($tabpkey, ",")) {
			$fldedit = false;
			$fldview = false;
			$flddelete = false;
		}
		if ($this->mode=="front-end" || $filemode=="master"){
			$fldedit = false;
			$flddelete = false;
		}
		# check if has detail tables
		$detailrs = $this->conn->Execute("select * from am_tables where dbname='".$this->dbname."' and (mastertable='".$tabname."' or mastertable2='".$tabname."' or mastertable3='".$tabname."')");
		$details_desc=""; $details_link = "";
		while (!$detailrs->EOF){
			# details-desc
			$details_desc.="<th class='thlist'>";
			$details_desc.="&nbsp;</th>";
			# details-link
			$details_link.="<td class='tddetail' align='center' width='10%' nowrap>";
			$fkey = $detailrs->fields["masterpkey"];
			if ($detailrs->fields["mastertable2"]==$tabname) $fkey = $detailrs->fields["masterpkey2"];
			if ($detailrs->fields["mastertable3"]==$tabname) $fkey = $detailrs->fields["masterpkey3"];
			$ext = ($this->mode=="front-end") ? "" : ".list";
			$details_link.="<a href=\"../".$this->directory."/".$detailrs->fields["tablename"].$ext.".php?".$fkey."=";
			$details_link.="{".$tabname.".".$tabpkey."}&master=$tabname"."."."$tabpkey\">";
			$details_link.="{common.".$detailrs->fields["tablename"]."}";
			$details_link.="</a></td>";
			$detailrs->MoveNext();
		}//end while detailrs
		# form the table
		$xt = new XTemplate($frm);
		while (!$fldrs->EOF){
			$xt->assign("am_fields", $fldrs->fields);
			$xt->parse("main.am_flddesc");
			$fldrs->MoveNext();
		}
		if ($filemode=="") $xt->assign("details_desc", $details_desc);
		$fldrs->MoveFirst();
		while (!$fldrs->EOF){
			$xt->assign("am_fields", $fldrs->fields);
			# if has delete
			if ($flddelete){
				$check = "<input type='checkbox' name='key[]' value='{".$tabname.".".$tabpkey."}'>";
				$xt->assign("check", $check);
			} else $xt->assign("check", $this->dot);
			# if has view
			if ($fldview){
				$view = "<a href='../".$this->directory."/$tabname.view.php?key={".$tabname.".".$tabpkey."}'>".$this->labels["label_view"]."</a>";
				$xt->assign("view", $view);
			} else $xt->assign("view", "");
			# if has edit
			if ($fldedit){
				$edit = "<a href='../".$this->directory."/$tabname.edit.php?key={".$tabname.".".$tabpkey."}'>".$this->labels["label_edit"]."</a>";
				$xt->assign("edit", $edit);
			} else $xt->assign("edit", "");
			$xt->parse("main.am_fldname");
			$fldrs->MoveNext();
		}//end while fldrs
		# links if have
		if ($filemode==""){
			$xt->assign("details_link", $details_link);
		}
		$fldrs->Close();
		# some controls
		if ($flddelete){
			$btn = "<input type='button' value='".$this->labels["label_delete"]."'";
			$btn.= " class='btn' onClick=\"javascript:this.form.action='../".$this->directory."/$tabname.delete.php';this.form.submit();\">";
			$xt->assign("button_delete", $btn);
		}
		if ($fldedit){
			$btn = "[<a href='../".$this->directory."/$tabname.add.php'>".$this->labels["label_add"]."</a>]";
			$xt->assign("button_add", $btn);
		}
		$xt->assign("tablename", $tabrs->fields["tablename"]);
		$xt->assign("tabledesc", $tabrs->fields["tabledesc"]);
		$xt->assign("tablepkey", $tabrs->fields["tablepkey"]);
		if ($this->mode=="front-end") {
			$xt->assign("form_action", "../".$this->directory."/".$tabname.".php");
		} else {
			$xt->assign("form_action", "../".$this->directory."/".$tabname.".list.php");
		}
		# some labels
		foreach ($this->labels as $k=>$v){
			$xt->assign($k, $v);
		}
		$xt->parse("main");
		$ret = $xt->text("main");

		# marks
		$ret = str_replace($this->srcMarks, $this->dstMarks, $ret);
		if ($this->savedpath!="") {
			$fn = $this->savedpath."/forms/".$tabrs->fields["tablename"].".list.htm";
			if ($this->mode=="front-end") {
				$fn = $this->savedpath."/forms/".$tabrs->fields["tablename"].".htm";
			}
			if ($filemode=="master") $fn = $this->savedpath."/forms/".$tabrs->fields["tablename"].".master.htm";
			$this->saveToFile($ret, $fn);
		}	
		return $ret;
	}//end of generate list
	function generateViewForm($tabid){
		$frm = "forms/page.view.htm";
		$tabrs = $this->conn->Execute("select * from am_tables where tableid=$tabid");
		if (!$tabrs) return "";
		$fldrs = $this->conn->Execute("select * from am_fields where tableid=$tabid and fldview=1 order by fldorder");
		if (!$fldrs) return "";
		if ($fldrs->RecordCount()<1) {
			$fldrs->Close();
			return "";
		}
		if ($tabrs->fields["hasview"]!=1) return;
		$xt = new XTemplate($frm);
		while (!$fldrs->EOF){
			$xt->assign("am_fields", $fldrs->fields);
			$xt->parse("main.am_fldname");
			$fldrs->MoveNext();
		}
		$fldrs->Close();
		$xt->assign("tablename", $tabrs->fields["tablename"]);
		$xt->assign("tabledesc", $tabrs->fields["tabledesc"]);
		$xt->assign("tablepkey", $tabrs->fields["tablepkey"]);
		# some control
		$btn = "<input type='button' class='btn' value='".$this->labels["label_goback"]."' onClick=\"history.go( -1);\">";
		$xt->assign("button_goback", $btn);
		foreach ($this->labels as $k=>$v){
			$xt->assign($k, $v);
		}
		$xt->parse("main");
		$ret = $xt->text("main");
		$ret = str_replace($this->srcMarks, $this->dstMarks, $ret);
		if ($this->savedpath!="") {
			$fn = $this->savedpath."/forms/".$tabrs->fields["tablename"].".view.htm";
			$this->saveToFile($ret, $fn);
		}	
		return $ret;
	}//end of generate view
	function generateControl($row){
		$ctrl = $row["fldcontrol"]; if ($ctrl=="") $ctrl = "textbox";
		$control = "";
		switch($ctrl){
			case "textbox": 
				$control = "<input type=\"text\" name=\"".$row["fldname"]."\" id=\"".$row["fldname"]."\" size=".$this->controlWidth;
				$control.= " value=\"{".$row["tablename"].".".$row["fldname"]."}\">";
				if ($row["fldtype"]=="D"){
					$control.="&nbsp;<a href=\"javascript:NewCal('".$row["fldname"]."','ddmmyyyy')\"><img src=\"images/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Pick a date\"></a>";
				}
				break;
			case "password": 
				$control = "<input type=\"password\" name=\"".$row["fldname"]."\" id=\"".$row["fldname"]."\" size=".$this->controlWidth;
				$control.= " value=\"{".$row["tablename"].".".$row["fldname"]."}\">";
				break;
			case "textarea": 
				$control = "<textarea name=\"".$row["fldname"]."\" id=\"".$row["fldname"]."\" rows=5 cols=".$this->controlWidth.">";
				$control.= "{".$row["tablename"].".".$row["fldname"]."}</textarea>";
				break;
			case "combobox": 
				$control.= "{".$row["fldname"]."}";
				break;
			case "multiselect": 
				$control.= "{".$row["fldname"]."}";
				break;
			case "checkbox": 
				$control.= "{".$row["fldname"]."}";
				break;
			case "file": 
				$control.= "<input type=\"file\" name=\"".$row["fldname"]."\" size=\"".$this->controlWidth."\">";
				$control.= "&nbsp;{file_".$row["fldname"]."}";
				break;
			case "editor": 
				$control = "<textarea name=\"".$row["fldname"]."\" id=\"".$row["fldname"]."\" rows=10 style=\"width:100%\">";
				$control.= "{".$row["tablename"].".".$row["fldname"]."}</textarea>\n";
				break;
		}
		if ($row["fldlabel"]!=""){
			$control.= "&nbsp;{labels.after_".$row["fldname"]."}";
		}
		return $control;
	}
	function generateTags($tArr){
		$retTags = "<table border=0 cellpadding=3 cellspacing=0><tr>\n";
		for($k=0;$k<count($tArr);$k++){
			$class = ($k==0)?"activeTab":"deactiveTab";
			$tag = $tArr[$k]; $n = $k+1;
			$retTags.= "<td id=\"tdTabs$n\" nowrap class=\"".$class."\" onClick=\"javascript:showFormTag(this, $n);\">".$tag."</td>\n";
		}
		$retTags.="<td class=\"lastTab\">&nbsp;</td>\n";
		$retTags.= "</tr></table>\n";
		return $retTags;
	}
	function generateEditForm($tabid){
		$frm = "forms/page.edit.htm";
		$tabrs = $this->conn->Execute("select * from am_tables where tableid=$tabid");
		if (!$tabrs) return "";
		if ($tabrs->fields["hasedit"]!=1) return;
		$tabname = $tabrs->fields["tablename"];
		# loop by tags
		$this->conn->Execute("update am_fields set fldtag='' where tableid=$tabid and fldtag is null");
		$tagrs = $this->conn->Execute("select fldtag from am_fields where tableid=$tabid and fldedit=1 group by fldtag order by fldtag");
		$tagArr = array(); $tagnum = 1;
		$xt = new XTemplate($frm);
		while (!$tagrs->EOF){		
			$tag = $tagrs->fields["fldtag"]; 
			array_push($tagArr, $tag); //echo $tag."<br>"; 
			$sql = "select * from am_fields where tableid=$tabid and fldedit=1";
			$sql.= " and fldtag='$tag'";
			$sql.= " order by fldorder";
			$fldrs = $this->conn->Execute($sql);
			while (!$fldrs->EOF){
				$row = $fldrs->fields;
				$xt->assign("am_fields", $row);
				$control = $this->generateControl($row);
				$xt->assign("control", $control);
				$xt->parse("main.tag.am_fldname");
				$fldrs->MoveNext();
			}
			$fldrs->Close();
			$xt->assign("tagnum", $tagnum);
			$display = ($tagnum==1)? "":"none"; $xt->assign("displaytagnum", $display);
			$xt->parse("main.tag");
			$tagrs->MoveNext(); $tagnum++;
		}//end while
		//print_r($tagArr);
		$tagrs->Close();
		if (count($tagArr)>1) $xt->assign("tags", $this->generateTags($tagArr));	
		# end loop by tags	
		$xt->assign("tablename", $tabrs->fields["tablename"]);
		$xt->assign("tabledesc", $tabrs->fields["tabledesc"]);
		$xt->assign("tablepkey", $tabrs->fields["tablepkey"]);
		# some control
		$btn = "<input type='submit' class='btn' value='".$this->labels["label_save"]."'>";
		$xt->assign("button_save", $btn);
		$btn = "<input type='button' class='btn' value='".$this->labels["label_cancel"]."' onClick=\"window.location.href='$tabname.list.php';\">";
		$xt->assign("button_cancel", $btn);
		
		foreach ($this->labels as $k=>$v){
			$xt->assign($k, $v);
		}
		$xt->parse("main");
		$ret = $xt->text("main");
		$ret = str_replace($this->srcMarks, $this->dstMarks, $ret);
		if ($this->savedpath!="") {
			$fn = $this->savedpath."/forms/".$tabrs->fields["tablename"].".edit.htm";
			$this->saveToFile($ret, $fn);
		}	
		return $ret;
	}//end of generate edit
	function generateListPage($tabid){
		$frm = "newpage.list.php";
		$tabrs = $this->conn->Execute("select * from am_tables where tableid=$tabid");
		if (!$tabrs) return "";
		$fldrs = $this->conn->Execute("select * from am_fields where tableid=$tabid order by fldorder");
		if (!$fldrs) return "";
		if ($fldrs->RecordCount()<1) {
			$fldrs->Close();
			return "";
		}
		if ($tabrs->fields["haslist"]!=1) return;
		$tabname = $tabrs->fields["tablename"];
		$tabpkey = $tabrs->fields["tablepkey"];
		$xt = new XTemplate($frm);
		$searchFlds = ""; $dateFlds = ""; $numFlds = "";
		$fileFlds = ""; $Menus = ""; $multiselections = ""; $staticmenus = "";
		$fldArr = array();
		while (!$fldrs->EOF){
			$type = $fldrs->fields["fldtype"];
			$ctrl = $fldrs->fields["fldcontrol"];
			$otype =strtolower($fldrs->fields["fldotype"]);
			$fname = $fldrs->fields["fldname"];
			# special treatment for SQL SQLServer
			if ($otype=="ntext"){
				$fldArr[] = "CAST($fname as TEXT) as $fname";
			} elseif ($otype=="nvarchar"){
				$fldArr[] = "CAST($fname as VARCHAR(250)) as $fname";
			} else $fldArr[] = $fname;
			# scan file fields
			$opts = $this->generateOptions($fldrs->fields["fldoptions"]);
			if ($ctrl=="file"){
				$fileFlds.= "'".$fldrs->fields["fldname"]."'=>array('fldtype'=>'".$type."','filetype'=>'".@$opts['filetype']."','filelocation'=>'".@$opts['filelocation']."'),\n";
			}
			# scan search fields & date fields
			if ($type=="C") $searchFlds.=$fldrs->fields["fldname"].",";
			if ($type=="D") $dateFlds.=$fldrs->fields["fldname"].",";
			if ($type=="N") $numFlds.=$fldrs->fields["fldname"].",";
			# generate file download script
			if ($type=="B"){
				$this->generateFilePage($tabname, $fldrs->fields["fldname"], $tabpkey);
			}
			# multi-select 
			if ($ctrl=="multiselect" && $fldrs->fields["fldedit"]==1){
				if (trim($opts["lookuptable"])!=""){ # lookup by table
					$multiselections.="'".$fldrs->fields["fldname"]."'=>'select ".$opts["lookupfieldshow"];
					$multiselections.=",".$opts["lookupfieldvalue"]." from ".$opts["lookuptable"]."',";
				} 
			}// end if control = multiselect
			# menu lookup
			if ($ctrl=="combobox" && count(array_keys($opts))>0){
				if (@$opts["lookuptable"]!=""){
					$Menus.="\"".$fldrs->fields["fldname"]."\"=>\"";
					$Menus.="select ".$opts["lookupfieldshow"].", ".$opts["lookupfieldvalue"]." from ".$opts["lookuptable"];
					$Menus.=" where ".$opts["lookupfieldvalue"]."='#value#'\",\n";
				} else { # lookup by values
					$staticmenus.="'".$fldrs->fields["fldname"]."'=>array(";
					foreach($opts as $ak=>$av){
						if ($ak=="") $ak=$av;
						if ($av!="") $staticmenus.="'".trim($ak)."'=>'".trim($av)."',";
					}
					if (substr($staticmenus, strlen($staticmenus)-1,1)==",") $staticmenus = substr($staticmenus, 0, strlen($staticmenus)-1);
					$staticmenus.="),";
				}	
			}//end if 			
			$fldrs->MoveNext();
		}//end while
		if ($searchFlds!="") $searchFlds = substr($searchFlds, 0, strlen($searchFlds)-1);
		if ($dateFlds!="") $dateFlds = substr($dateFlds, 0, strlen($dateFlds)-1);
		if ($numFlds!="") $numFlds = substr($numFlds, 0, strlen($numFlds)-1);
		if ($fileFlds!="") $fileFlds = substr($fileFlds, 0, strlen($fileFlds)-2);
		if ($Menus!="") $Menus = substr($Menus, 0, strlen($Menus)-2);
		if ($multiselections!="") $multiselections = substr($multiselections, 0, strlen($multiselections)-1);
		if ($staticmenus!="") $staticmenus = substr($staticmenus, 0, strlen($staticmenus)-1);
		# now parse
		$xt->assign("table", $tabname);
		$frm = "forms/".$tabname.".list.htm";
		if ($this->mode=="front-end") $frm = "forms/".$tabname.".htm";
		$xt->assign("form", $frm);
		$xt->assign("fields", $searchFlds);
		$xt->assign("pfield", $tabrs->fields["tablepkey"]);
		# - display fields
		if ($this->dbtype=="mssql"){
			$xt->assign("dspflds", implode(",", $fldArr));
		} else $xt->assign("dspflds", "*");
		# --
		$xt->assign("dateflds", $dateFlds);
		$xt->assign("numflds", $numFlds);
		$xt->assign("menu", $Menus);
		$xt->assign("fileflds", $fileFlds);
		$xt->assign("multiselect", $multiselections);
		$xt->assign("staticmenu", $staticmenus);
		$xt->assign("pagesize", $this->pageSize);
		$xt->assign("order", trim($tabrs->fields["fldorder"]));
		if ($tabrs->fields["fldorder"]!="") $xt->assign("ordertype", trim($tabrs->fields["ordertype"]));
		$xt->assign("HEADER", "include(\"header.inc.php\");");
		$xt->assign("FOOTER", "include(\"footer.inc.php\")");
		# master - details
		$mkeys = ""; $mtabs = "";
		if (trim($tabrs->fields["masterpkey"])!="") {
			$mkeys = $tabrs->fields["masterpkey"];
		}	
		if (trim($tabrs->fields["masterpkey2"])!="") {
			$mkeys.= ",".$tabrs->fields["masterpkey2"];
		}	
		if (trim($tabrs->fields["masterpkey3"])!="") {
			$mkeys.= ",".$tabrs->fields["masterpkey3"];
		}	
		$xt->assign("masterkey", $mkeys);
		$xt->assign("masterquote", "'");
		$xt->parse("main");
		$fn = $this->savedpath."/".$tabname.".list.php";
		if ($this->mode=="front-end"){
			$fn = $this->savedpath."/".$tabname.".php";
		}
		$this->saveToFile($xt->text("main"), $fn);
	}//end of function generate list page
	function generateMasterPage($tabid){
		$frm = "master.list.php";
		$tabrs = $this->conn->Execute("select * from am_tables where tableid=$tabid");
		if (!$tabrs) return "";
		$fldrs = $this->conn->Execute("select * from am_fields where tableid=$tabid order by fldorder");
		if (!$fldrs) return "";
		if ($fldrs->RecordCount()<1) {
			$fldrs->Close();
			return "";
		}
		if ($tabrs->fields["haslist"]!=1) return;
		$tabname = $tabrs->fields["tablename"];
		$tabpkey = $tabrs->fields["tablepkey"];
		$xt = new XTemplate($frm);
		$dateFlds = "";	$fileFlds = ""; $Menus = "";
		while (!$fldrs->EOF){
			$type = $fldrs->fields["fldtype"];
			$ctrl = $fldrs->fields["fldcontrol"];
			# scan file fields
			$opts = $this->generateOptions($fldrs->fields["fldoptions"]);
			if ($ctrl=="file"){
				$fileFlds.= "'".$fldrs->fields["fldname"]."'=>array('fldtype'=>'".$type."','filetype'=>'".@$opts['filetype']."','filelocation'=>'".@$opts['filelocation']."'),\n";
			}
			# scan date fields
			if ($type=="D") $dateFlds.=$fldrs->fields["fldname"].",";
			# menu lookup
			if ($ctrl=="combobox" && count(array_keys($opts))>0){
				$Menus.="\"".$fldrs->fields["fldname"]."\"=>\"";
				$Menus.="select ".$opts["lookupfieldshow"].", ".$opts["lookupfieldvalue"]." from ".$opts["lookuptable"];
				$Menus.=" where ".$opts["lookupfieldvalue"]."=#value#\",\n";
			}
			$fldrs->MoveNext();
		}//end while
		if ($dateFlds!="") $dateFlds = substr($dateFlds, 0, strlen($dateFlds)-1);
		if ($fileFlds!="") $fileFlds = substr($fileFlds, 0, strlen($fileFlds)-2);
		if ($Menus!="") $Menus = substr($Menus, 0, strlen($Menus)-2);
		# now parse
		$xt->assign("table", $tabname);
		$xt->assign("pfield", trim($tabrs->fields["tablepkey"]));
		$xt->assign("dateflds", $dateFlds);
		$xt->assign("menu", $Menus);
		$xt->assign("fileflds", $fileFlds);
		$xt->parse("main");
		$fn = $this->savedpath."/".$tabname.".master.php";
		$this->saveToFile($xt->text("main"), $fn);
	}//end of function generate master page
	function generateViewPage($tabid){
		$frm = "newpage.view.php";
		$tabrs = $this->conn->Execute("select * from am_tables where tableid=$tabid");
		if (!$tabrs) return "";
		$fldrs = $this->conn->Execute("select * from am_fields where tableid=$tabid order by fldorder");
		if (!$fldrs) return "";
		if ($fldrs->RecordCount()<1) {
			$fldrs->Close();
			return "";
		}
		if ($tabrs->fields["hasview"]!=1) return;
		if ($tabrs->fields["tablepkey"]=="" || strpos($tabrs->fields["tablepkey"], ",")) return;
		$tabname = $tabrs->fields["tablename"];
		$tabpkey = $tabrs->fields["tablepkey"];
		$xt = new XTemplate($frm);
		$searchFlds = ""; $dateFlds = ""; $numFlds = ""; $quote="";
		$fileFlds = ""; $multiselections = ""; $staticmenus = "";
		$fldArr = array();
		while (!$fldrs->EOF){
			$type = $fldrs->fields["fldtype"];
			$ctrl = $fldrs->fields["fldcontrol"];
			$otype =strtolower($fldrs->fields["fldotype"]);
			$fname = $fldrs->fields["fldname"];
			# special treatment for SQL SQLServer
			if ($otype=="ntext"){
				$fldArr[] = "CAST($fname as TEXT) as $fname";
			} elseif ($otype=="nvarchar"){
				$fldArr[] = "CAST($fname as VARCHAR(250)) as $fname";
			} else $fldArr[] = $fname;
			# scan file fields
			$opts = $this->generateOptions($fldrs->fields["fldoptions"]);
			if ($ctrl=="file"){
				$fileFlds.= "'".$fldrs->fields["fldname"]."'=>array('fldtype'=>'".$type."','filetype'=>'".@$opts['filetype']."','filelocation'=>'".@$opts['filelocation']."'),\n";
			}
			# search, date and number fields
			if ($type=="C") $searchFlds.=$fldrs->fields["fldname"].",";
			if ($type=="D") $dateFlds.=$fldrs->fields["fldname"].",";
			if ($type=="N") $numFlds.=$fldrs->fields["fldname"].",";
			if ($fldrs->fields["fldname"]==$tabpkey && $fldrs->fields["fldtype"]=="C") $quote="'";
			# multi-select 
			if ($ctrl=="multiselect" && $fldrs->fields["fldedit"]==1){
				if (trim($opts["lookuptable"])!=""){ # lookup by table
					$multiselections.="'".$fldrs->fields["fldname"]."'=>'select ".$opts["lookupfieldshow"];
					$multiselections.=",".$opts["lookupfieldvalue"]." from ".$opts["lookuptable"]."',";
				} 
			}// end if control = multiselect
			# menu lookup
			if ($ctrl=="combobox" && count(array_keys($opts))>0){
				if (@$opts["lookuptable"]!=""){
					$Menus.="\"".$fldrs->fields["fldname"]."\"=>\"";
					$Menus.="select ".$opts["lookupfieldshow"].", ".$opts["lookupfieldvalue"]." from ".$opts["lookuptable"];
					$Menus.=" where ".$opts["lookupfieldvalue"]."='#value#'\",\n";
				} else { # lookup by values
					$staticmenus.="'".$fldrs->fields["fldname"]."'=>array(";
					foreach($opts as $ak=>$av){
						if ($ak=="") $ak=$av;
						if ($av!="") $staticmenus.="'".trim($ak)."'=>'".trim($av)."',";
					}
					if (substr($staticmenus, strlen($staticmenus)-1,1)==",") $staticmenus = substr($staticmenus, 0, strlen($staticmenus)-1);
					$staticmenus.="),";
				}//end if 
			}	
			$fldrs->MoveNext();
		}//end while
		if ($searchFlds!="") $searchFlds = substr($searchFlds, 0, strlen($searchFlds)-1);
		if ($dateFlds!="") $dateFlds = substr($dateFlds, 0, strlen($dateFlds)-1);
		if ($numFlds!="") $numFlds = substr($numFlds, 0, strlen($numFlds)-1);
		if ($fileFlds!="") $fileFlds = substr($fileFlds, 0, strlen($fileFlds)-2);
		if ($Menus!="") $Menus = substr($Menus, 0, strlen($Menus)-2);
		if ($multiselections!="") $multiselections = substr($multiselections, 0, strlen($multiselections)-1);
		if ($staticmenus!="") $staticmenus = substr($staticmenus, 0, strlen($staticmenus)-1);
		# now parse
		$xt->assign("table", $tabname);
		$xt->assign("form", "forms/".$tabname.".view.htm");
		$xt->assign("pkey", $tabrs->fields["tablepkey"]);
		# - display fields
		if ($this->dbtype=="mssql"){
			$xt->assign("dspflds", implode(",", $fldArr));
		} else $xt->assign("dspflds", "*");
		# --
		$xt->assign("dateflds", $dateFlds);
		$xt->assign("numflds", $numFlds);
		$xt->assign("menu", $Menus);
		$xt->assign("fileflds", $fileFlds);
		$retPage = $tabname.".list.php"; if ($this->mode=="front-end") $retPage = $tabname.".php";
		$xt->assign("returnpage", $retPage);
		$xt->assign("multiselect", $multiselections);
		$xt->assign("staticmenu", $staticmenus);
		$xt->assign("quote", $quote);
		$xt->assign("HEADER", "include(\"header.inc.php\");");
		$xt->assign("FOOTER", "include(\"footer.inc.php\")");
		$xt->parse("main");
		$this->saveToFile($xt->text("main"), $this->savedpath."/".$tabname.".view.php");
	}//end of function generate view page
	function generateOptions($opts){
		$optArr = explode("\n", $opts);
		for($kk=0;$kk<count($optArr);$kk++){
			$optArr[$kk] = str_replace(chr(13), "", $optArr[$kk]);
			$optArr[$kk] = str_replace(chr(10), "", $optArr[$kk]);
		}
		$retarr = array();
		foreach($optArr as $line) {
			$lineArr = explode("=", $line);
			if (count($lineArr)==1) {
				$v = $lineArr[0];
			} else $v = $lineArr[1];
			if (trim($lineArr[0])!="") $retarr[trim($lineArr[0])] = trim($v);
		}//end for
		if (!array_key_exists("lookup", $retarr)) $retarr["lookup"] = "";
		if (!array_key_exists("lookuptable", $retarr)) $retarr["lookuptable"] = "";
		if (!array_key_exists("lookupfieldvalue", $retarr)) $retarr["lookupfieldvalue"] = "";
		if (!array_key_exists("lookupfieldshow", $retarr)) $retarr["lookupfieldshow"] = "";
		return $retarr;
	}//end of function generate options
	function generateFilePage($tab, $fld, $pfld){
		$code = file_get_contents("table.fld.get.php");
		$code = str_replace(array("{table}", "{fldname}", "{pfld}"), array($tab, $fld, $pfld), $code);
		$this->saveToFile($code, $this->savedpath."/".$tab.".".$fld.".get.php");
		return true;
	}
	function generateEditPage($tabid){
		$frm = "newpage.edit.php";
		$tabrs = $this->conn->Execute("select * from am_tables where tableid=$tabid");
		if (!$tabrs) return "";
		$fldrs = $this->conn->Execute("select * from am_fields where tableid=$tabid order by fldorder");
		if (!$fldrs) return "";
		if ($fldrs->RecordCount()<1) {
			$fldrs->Close();
			return "";
		}
		if ($tabrs->fields["hasedit"]!=1) return;
		if (trim($tabrs->fields["tablepkey"])=="" || strpos($tabrs->fields["tablepkey"], ",")) return;
		$tabname = $tabrs->fields["tablename"];
		$tabpkey = $tabrs->fields["tablepkey"];
		$xt = new XTemplate($frm);
		$Flds = ""; $dateFlds = ""; $numFlds = ""; $quote=""; $checkboxes = ""; $fileFlds = "";
		$menus = ""; $staticmenus = ""; $multiselections = ""; $ruleFlds = "";
		while (!$fldrs->EOF){
			$type = $fldrs->fields["fldtype"]; 
			$ctrl = $fldrs->fields["fldcontrol"];
			# scan for edit fields, date fields and num fields
			if ($fldrs->fields["fldedit"]==1) $Flds.=$fldrs->fields["fldname"].",";
			if ($type=="D" && $fldrs->fields["fldedit"]==1) $dateFlds.=$fldrs->fields["fldname"].",";
			if ($type=="N" && $fldrs->fields["fldedit"]==1) $numFlds.=$fldrs->fields["fldname"].",";
			# Now determine special control
			$optArr = $this->generateOptions($fldrs->fields["fldoptions"]);
			if ($ctrl=="combobox" && $fldrs->fields["fldedit"]==1){
				if ($optArr["lookuptable"]!=""){ # lookup by table
					$menus.="'".$fldrs->fields["fldname"]."'=>'select ".$optArr["lookupfieldshow"];
					$menus.=",".$optArr["lookupfieldvalue"]." from ".$optArr["lookuptable"]."',";
				} else { # lookup by values
					$staticmenus.="'".$fldrs->fields["fldname"]."'=>array(";
					foreach($optArr as $ak=>$av){
						if ($ak=="") $ak=$av;
						if ($av!="") $staticmenus.="'".trim($ak)."'=>'".trim($av)."',";
					}
					if (substr($staticmenus, strlen($staticmenus)-1,1)==",") $staticmenus = substr($staticmenus, 0, strlen($staticmenus)-1);
					$staticmenus.="),";
				}//end if 
			}// end if control = combobox
			# multi-select 
			if ($ctrl=="multiselect" && $fldrs->fields["fldedit"]==1){
				if (trim($optArr["lookuptable"])!=""){ # lookup by table
					$multiselections.="'".$fldrs->fields["fldname"]."'=>'select ".$optArr["lookupfieldshow"];
					$multiselections.=",".$optArr["lookupfieldvalue"]." from ".$optArr["lookuptable"]."',";
				} 
			}// end if control = multiselect
			# scan file fields
			if ($ctrl=="file" && $fldrs->fields["fldedit"]==1){
				$fileFlds.= "'".$fldrs->fields["fldname"]."'=>array('fldtype'=>'".$type."','filetype'=>'".@$optArr['filetype']."','filelocation'=>'".@$optArr['filelocation']."'),\n";
			}
			# checkboexes
			if ($ctrl=="checkbox" && $fldrs->fields["fldedit"]==1) $checkboxes.=$fldrs->fields["fldname"].",";		
			if ($fldrs->fields["fldname"]==$tabpkey && $fldrs->fields["fldtype"]=="C") $quote="'";
			# rule fields
			if ($fldrs->fields["fldedit"]==1 && @$optArr["rule"]!=""){
				$ruleFlds.="'".$fldrs->fields["fldname"]."'=>'".$optArr["rule"]."',\n";
			}
			$fldrs->MoveNext();
		}//end while
		if ($Flds!="") $Flds = substr($Flds, 0, strlen($Flds)-1);
		if ($dateFlds!="") $dateFlds = substr($dateFlds, 0, strlen($dateFlds)-1);
		if ($numFlds!="") $numFlds = substr($numFlds, 0, strlen($numFlds)-1);
		if ($checkboxes!="") $checkboxes = substr($checkboxes, 0, strlen($checkboxes)-1);
		if ($multiselections!="") $multiselections = substr($multiselections, 0, strlen($multiselections)-1);
		if ($fileFlds!="") $fileFlds = substr($fileFlds, 0, strlen($fileFlds)-2);
		if ($ruleFlds!="") $ruleFlds = substr($ruleFlds, 0, strlen($ruleFlds)-2);
		if (substr($menus, strlen($menus)-1,1)==",") $menus = substr($menus, 0, strlen($menus)-1);
		if (substr($staticmenus, strlen($staticmenus)-1,1)==",") $staticmenus = substr($staticmenus, 0, strlen($staticmenus)-1);
		# now parse
		$xt->assign("table", $tabname);
		$xt->assign("form", "forms/".$tabname.".edit.htm");
		$xt->assign("pkey", trim($tabrs->fields["tablepkey"]));
		$xt->assign("fields", $Flds);
		$xt->assign("dateflds", $dateFlds);
		$xt->assign("numflds", $numFlds);
		$xt->assign("fileflds", $fileFlds);
		$xt->assign("returnpage", "../".$this->directory."/".$tabname.".list.php");
		$xt->assign("quote", $quote);
		$xt->assign("checkboxes", $checkboxes);
		$xt->assign("menu", $menus);
		$xt->assign("staticmenu", $staticmenus);
		$xt->assign("multiselect", $multiselections);
		$xt->assign("ruleflds", $ruleFlds);
		$xt->assign("HEADER", "include(\"header.inc.php\");");
		$xt->assign("FOOTER", "include(\"footer.inc.php\")");
		$xt->parse("main");
		$this->saveToFile($xt->text("main"), $this->savedpath."/".$tabname.".edit.php");
	}//end of function generate edit page
	function generateAddPage($tabid){
		$frm = "newpage.add.php";
		$tabrs = $this->conn->Execute("select * from am_tables where tableid=$tabid");
		if (!$tabrs) return "";
		$fldrs = $this->conn->Execute("select * from am_fields where tableid=$tabid order by fldorder");
		if (!$fldrs) return "";
		if ($fldrs->RecordCount()<1) {
			$fldrs->Close();
			return "";
		}
		if ($tabrs->fields["hasedit"]!=1) return;
		if (trim($tabrs->fields["tablepkey"])=="" || strpos($tabrs->fields["tablepkey"], ",")) return;
		$tabname = $tabrs->fields["tablename"];
		$tabpkey = $tabrs->fields["tablepkey"];
		$xt = new XTemplate($frm);
		$Flds = ""; $dateFlds = ""; $numFlds = ""; $quote=""; $checkboxes = ""; $fileFlds = "";
		$menus = ""; $staticmenus = ""; $multiselections = ""; $defaultVals = ""; $ruleFlds = ""; $editors = "";
		while (!$fldrs->EOF){
			$type = $fldrs->fields["fldtype"]; $ctrl = $fldrs->fields["fldcontrol"];
			if ($fldrs->fields["fldedit"]==1) $Flds.=$fldrs->fields["fldname"].",";
			if ($type=="D" && $fldrs->fields["fldedit"]==1) $dateFlds.=$fldrs->fields["fldname"].",";
			if ($type=="N" && $fldrs->fields["fldedit"]==1) $numFlds.=$fldrs->fields["fldname"].",";
			# Now determine special control
			$optArr = $this->generateOptions($fldrs->fields["fldoptions"]);
			if ($ctrl=="combobox" && $fldrs->fields["fldedit"]==1){
				if ($optArr["lookuptable"]!=""){ # lookup by table
					$menus.="'".$fldrs->fields["fldname"]."'=>'select ".$optArr["lookupfieldshow"];
					$menus.=",".$optArr["lookupfieldvalue"]." from ".$optArr["lookuptable"]."',";
				} else { # lookup by values
					$staticmenus.="'".$fldrs->fields["fldname"]."'=>array(";
					foreach($optArr as $ak=>$av){
						if ($ak=="") $ak=$av;
						if ($av!="") $staticmenus.="'".trim($ak)."'=>'".trim($av)."',";
					}
					if (substr($staticmenus, strlen($staticmenus)-1,1)==",") $staticmenus = substr($staticmenus, 0, strlen($staticmenus)-1);
					$staticmenus.="),";
				}//end if 
			}// end if control = combobox
			# multi-select 
			if ($ctrl=="multiselect" && $fldrs->fields["fldedit"]==1){
				if (trim($optArr["lookuptable"])!=""){ # lookup by table
					$multiselections.="'".$fldrs->fields["fldname"]."'=>'select ".$optArr["lookupfieldshow"];
					$multiselections.=",".$optArr["lookupfieldvalue"]." from ".$optArr["lookuptable"]."',";
				} 
			}// end if control = multiselect
			# scan file fields
			if ($ctrl=="file" && $fldrs->fields["fldedit"]==1){
				$fileFlds.= "'".$fldrs->fields["fldname"]."'=>array('fldtype'=>'".$type."','filetype'=>'".@$optArr['filetype']."','filelocation'=>'".@$optArr['filelocation']."'),\n";
			}
			if ($ctrl=="checkbox" && $fldrs->fields["fldedit"]==1) $checkboxes.=$fldrs->fields["fldname"].",";		
			if ($fldrs->fields["fldname"]==$tabpkey && $fldrs->fields["fldtype"]=="C") $quote="'";
			# default values
			if (trim(@$optArr["default"])!=""){
				$defaultVals.= "'".$fldrs->fields["fldname"]."'=>'".$optArr["default"]."',\n";
			}
			# rule fields
			if ($fldrs->fields["fldedit"]==1 && @$optArr["rule"]!=""){
				$ruleFlds.="'".$fldrs->fields["fldname"]."'=>'".$optArr["rule"]."',\n";
			}
			$fldrs->MoveNext();
		}//end while
		if ($Flds!="") $Flds = substr($Flds, 0, strlen($Flds)-1);
		if ($dateFlds!="") $dateFlds = substr($dateFlds, 0, strlen($dateFlds)-1);
		if ($numFlds!="") $numFlds = substr($numFlds, 0, strlen($numFlds)-1);
		if ($fileFlds!="") $fileFlds = substr($fileFlds, 0, strlen($fileFlds)-2);
		if ($checkboxes!="") $checkboxes = substr($checkboxes, 0, strlen($checkboxes)-1);
		if (substr($menus, strlen($menus)-1,1)==",") $menus = substr($menus, 0, strlen($menus)-1);
		if (substr($staticmenus, strlen($staticmenus)-1,1)==",") $staticmenus = substr($staticmenus, 0, strlen($staticmenus)-1);
		if ($defaultVals!="") $defaultVals = substr($defaultVals, 0, strlen($defaultVals)-2);
		if ($multiselections!="") $multiselections = substr($multiselections, 0, strlen($multiselections)-1);
		if ($ruleFlds!="") $ruleFlds = substr($ruleFlds, 0, strlen($ruleFlds)-2);
		# now parse
		$xt->assign("table", $tabname);
		$xt->assign("form", "forms/".$tabname.".edit.htm");
		$xt->assign("pkey", $tabrs->fields["tablepkey"]);
		$xt->assign("fields", $Flds);
		$xt->assign("dateflds", $dateFlds);
		$xt->assign("numflds", $numFlds);
		$xt->assign("fileflds", $fileFlds);
		$xt->assign("returnpage", "../".$this->directory."/".$tabname.".list.php");
		$xt->assign("quote", $quote);
		$xt->assign("checkboxes", $checkboxes);
		$xt->assign("menu", $menus);
		$xt->assign("staticmenu", $staticmenus);
		$xt->assign("defaultvalues", $defaultVals);
		$xt->assign("multiselect", $multiselections);
		$xt->assign("ruleflds", $ruleFlds);
		$xt->assign("HEADER", "include(\"header.inc.php\");");
		$xt->assign("FOOTER", "include(\"footer.inc.php\")");
		$xt->parse("main");
		$this->saveToFile($xt->text("main"), $this->savedpath."/".$tabname.".add.php");
	}//end of function generate add page
	function generateDeletePage($tabid){
		$frm = "newpage.delete.php";
		$tabrs = $this->conn->Execute("select * from am_tables where tableid=$tabid");
		if (!$tabrs) return "";
		if ($tabrs->fields["hasdelete"]!=1) return;
		if ($tabrs->fields["tablepkey"]=="" || strpos($tabrs->fields["tablepkey"], ",")) return;
		$dbname  = $tabrs->fields["dbname"];
		$tabname = $tabrs->fields["tablename"];
		$tabpkey = $tabrs->fields["tablepkey"];
		$fldrs = $this->conn->Execute("select fldtype from am_fields where tableid=$tabid and fldname='$tabpkey'");
		if (!$fldrs) return "";
		if ($fldrs->RecordCount()<1) {
			$fldrs->Close();
			return "";
		}
		$xt = new XTemplate($frm);
		$quote=($fldrs->fields["fldtype"]=="C")?"'":"";
		$sql = "select * from am_tables where dbname='$dbname' and (mastertable='$tabname' or mastertable2='$tabname' or mastertable3='$tabname')";
		$detailRS = $this->conn->Execute($sql);
		# delete detail tables
		$detailTables = "";
		while (!$detailRS->EOF){
			$row = $detailRS->fields; $fkey = "";
			if ($row["mastertable"]==$tabname) $fkey = $row["masterpkey"];
			if ($row["mastertable2"]==$tabname) $fkey = $row["masterpkey2"];
			if ($row["mastertable3"]==$tabname) $fkey = $row["masterpkey3"];
			if ($fkey!="") $detailTables.="'".$row["tablename"]."'=>'".$fkey."',\n";
			$detailRS->MoveNext();
		}
		$detailRS->Close();
		if ($detailTables!=""){
			$detailTables = substr($detailTables, 0, strlen($detailTables)-2);
		}
		# now parse
		$xt->assign("table", $tabname);
		$xt->assign("pkey", $tabrs->fields["tablepkey"]);
		$xt->assign("returnpage", "../".$this->directory."/".$tabname.".list.php");
		$xt->assign("quote", $quote);
		$xt->assign("detailTables", $detailTables);
		$xt->assign("HEADER", "include(\"header.inc.php\");");
		$xt->assign("FOOTER", "include(\"footer.inc.php\")");
		$xt->parse("main");
		$this->saveToFile($xt->text("main"), $this->savedpath."/".$tabname.".delete.php");
	}//end of function generate delete page
	function generateCommonPage($tabArr){
		# connect page
		$dbrs = $this->conn->Execute("select * from am_dbs where dbname='".$this->dbname."'");
		$tp = new XTemplate("connect.inc.php");
		$tp->assign("HOST", $dbrs->fields["dbhost"]);
		$tp->assign("USER", $dbrs->fields["dbuser"]);
		$tp->assign("PASSWORD", $dbrs->fields["dbpassword"]);
		$tp->assign("DATABASE", $dbrs->fields["dbname"]);
		$tp->assign("DRIVER", $dbrs->fields["dbtype"]);
		$tp->assign("session_user", "status_User");
		$tp->assign("session_admin", "status_UserLevel");
		$tp->assign("session_admin_value", "-1");
		$tp->assign("login_page", "login.php");
		$secured = ($this->mode=="front-end")?"0":"1";
		$tp->assign("has_security", $secured);
		$tp->assign("unsecured_pages", "test.php");
		
		$tp->parse("main");
		if (!file_exists($this->savedpath."/connect.php")) {
			$this->saveToFile($tp->text("main"), $this->savedpath."/connect.php");
		}
		# copy some files
		@mkdir($this->savedpath."/forms"); @mkdir($this->savedpath."/images"); @mkdir($this->savedpath."/labels");
		$pageArr = array(
			"header.inc.php", 
			"footer.inc.php"
		);
		$formArr = array("page.htm", "confirm.delete.htm", "search.htm");		
		$imageArr = array("plus.gif", "preview.gif", "search.gif", "cal.gif");		
		$labelArr = array("common.label.inc");		
		foreach($pageArr as $p){
			if (!file_exists($this->savedpath."/".$p)){
				copy($this->apppath."/appm/".$p, $this->savedpath."/".$p);
			}
		}//end copy pages
		foreach($formArr as $p){
			copy($this->apppath."/appm/forms/".$p, $this->savedpath."/forms/".$p);
		}//end copy forms
		foreach($imageArr as $p){
			copy($this->apppath."/appm/images/".$p, $this->savedpath."/images/".$p);
		}//end copy images
		//foreach($labelArr as $p){
		//	copy($this->apppath."/appm/labels/".$p, $this->savedpath."/labels/".$p);
		//}//end copy images
		# copy classes
		//$cFiles = $this->getFiles("appm/classes");
		//@mkdir($this->savedpath."/classes");
		//foreach($cFiles as $p){
		//	copy($this->apppath."/appm/classes/".$p, $this->savedpath."/classes/".$p);
		//}//end copy classes

		# finally, generate index page
		//$this->generateIndexPage(implode(",",$tabArr));
		$this->generateLabels($tabArr);
		return true;
	}//end of function generate common pages
	function generateIndexPage($tabs){
		$form = "forms/index.inc.htm";
		$xt = new XTemplate($form);
		$firstPage="";
		$tables = $this->conn->Execute("select * from am_tables where dbname='".$this->dbname."' and haslist=1 and tableid in ($tabs) order by ordernum");
		while(!$tables->EOF){
			$tdclass = "";
			if ($firstPage=="" && file_exists($this->savedpath."/".$tables->fields["tablename"].".list.php")) {
				$firstPage = $tables->fields["tablename"].".list.php";
				$tdclass = "tdselected";
			}	
			$xt->assign("am_tables", $tables->fields);
			$xt->assign("target", $this->defaultTarget);
			$xt->assign("tdclass", $tdclass);
			$xt->parse("main.am_tables");
			$tables->MoveNext();
		}
		$xt->assign("dbdesc", $this->dbdesc);
		$xt->assign("frameSrc", $firstPage);
		$xt->parse("main");
		$ret = $xt->text("main");
		$ret = str_replace($this->srcMarks, $this->dstMarks, $ret);
		$this->saveToFile($ret, $this->savedpath."/forms/index.htm");
		if (!file_exists($this->savedpath."/index.php")) $this->saveToFile(file_get_contents("index.inc.php"), $this->savedpath."/index.php");
	}
	function generateLabels($tabArr){
		$sql = "select tableid, tablename, tabledesc from am_tables where dbname='".$this->dbname."'";
		$sql.= " and tablename not like 'am_%' and tableid in ('".implode("','", $tabArr)."')";
		$tabrs = $this->conn->Execute($sql);
		while(!$tabrs->EOF){
			$row = $tabrs->fields;
			$labels = $row["tablename"]." = ".$row["tabledesc"]."\n";
			$fldrs = $this->conn->Execute("select * from am_fields where tableid=".$row['tableid']);
			while (!$fldrs->EOF){
				$labels.=$fldrs->fields["fldname"]." = ".$fldrs->fields["flddesc"]."\n";
				if ($fldrs->fields["fldlabel"]!=""){
					$labels.="after_".$fldrs->fields["fldname"]." = ".$fldrs->fields["fldlabel"]."\n";
				}
				$fldrs->MoveNext();
			}
			$fldrs->Close();
			$this->saveToFile($labels, $this->savedpath."/labels/".$row["tablename"].".label.inc");
			$tabrs->MoveNext();
		}//end while
		$tabrs->Close();
		
		# for tables.labels.inc
		$sql = "select tableid, tablename, tabledesc from am_tables where dbname='".$this->dbname."' and tablename not like 'am_%'";
		$tablabels = "";
		$tabrs = $this->conn->Execute($sql);
		while(!$tabrs->EOF){
			$tablabels.= $tabrs->fields["tablename"]." = ".$tabrs->fields["tabledesc"]."\n";
			$tabrs->MoveNext();
		}
		$this->saveToFile($tablabels, $this->savedpath."/labels/tables.label.inc");
		$tabrs->Close();
	}//end of function generateLabels
}//end of class
?>
<?php session_start();?>
<?php include("connect.php")?>
<?php 
function changeType($src){
	$tmp="C"; $src = strtolower($src);
	switch($src){
		case "char": 
			$tmp="C"; break;
		case "varchar": 
			$tmp="C"; break;
		case "nvarchar": 
			$tmp="C"; break;
		case "ntext": 
			$tmp="C"; break;
		case "text": 
			$tmp="C"; break;
		case "longtext": 
			$tmp="C"; break;
		case "date": 
			$tmp="D"; break;
		case "datetime": 
			$tmp="D"; break;
		case "time": 
			$tmp="D"; break;
		case "int": 
			$tmp="N"; break;
		case "smallint": 
			$tmp="N"; break;
		case "bigint": 
			$tmp="N"; break;
		case "bit": 
			$tmp="N"; break;
		case "money": 
			$tmp="N"; break;
		case "numeric": 
			$tmp="N"; break;
		case "decimal": 
			$tmp="N"; break;
		case "binary": 
			$tmp="B"; break;
		case "blob": 
			$tmp="B"; break;
		case "image": 
			$tmp="B"; break;
	}
	if (strpos($src, "blob")>0) $tmp = "B";
	return $tmp;
}//end of function
?>
<?php
$id = @$_REQUEST["tableid"].@$_REQUEST["key"];
if ($id=="") $id = @$_SESSION["am_tableid"];
$tabrs = $oConn->Execute("select * from am_tables where tableid=$id");
if (!$tabrs) die("Invalid parameter !");
if ($tabrs->RecordCount()<1) die("Table not found !");
$dbname = $tabrs->fields["dbname"]; $tablename = $tabrs->fields["tablename"];
$rs = $oConn->Execute("select * from am_dbs where dbname='$dbname'");
if ($rs->RecordCount()<1) die("No database found !");
$host = $rs->fields["dbhost"];
$db = $rs->fields["dbname"];
$user = $rs->fields["dbuser"];
$pass = $rs->fields["dbpassword"];
$driver = $rs->fields["dbtype"];
$_lcConn = &ADONewConnection($driver);
$_lcConn->Connect($host, $user, $pass, $db);
if (!$_lcConn) die("<font color=red>Can not connect to database [$db]</font>.");
$fields = $_lcConn->MetaColumns($tablename);
# $oConn->debug=true;
$flds = "";
foreach($fields as $k=>$v){
	$fList["tableid"] = $id;
	$fList["tablename"] = $tablename;
	$fList["dbname"] = $dbname;
	$fList["fldid"] = $oConn->GenID();
	$fldname = $v->name; $flds.="'".$fldname."',";
	$fList["fldname"] = $fldname;
	$fList["flddesc"] = $fldname;
	$fList["fldtype"] = changeType($v->type);
	$fList["fldotype"] = $v->type;
	$fList["fldlength"] = $v->max_length;
	$num = $oCls->GetFieldValue("select max(fldorder) from am_fields where tableid=$id");
	$fList["fldorder"] = intval($num)+1;
	$fList["fldcontrol"] = "textbox"; 
	$fList["fldtag"] = "";
	$fList["fldedit"] = 0;
	$fList["fldlist"] = 0;
	$fList["fldview"] = 0;
	$fList["fldoptions"] = "";
	if ($fList["fldtype"]=="B"){
		$fList["fldcontrol"] = "file";
		$fList["fldoptions"] = "filelocation=DB\n";
		$fList["fldoptions"].= "filetype=*\n";
		$fList["fldoptions"].= "fldtype=B";
	}
	$e = $oCls->GetFieldValue("select fldid from am_fields where dbname='$dbname' and tablename='$tablename' and fldname='$fldname'");
	if ($e==""){
		$oConn->Replace("am_fields", $fList, "fldid", true);
	} else {
		$tmprs = $oConn->Execute("select * from am_fields where fldid=$e");
		$fList["fldid"] = $e;
		$fList["flddesc"] = $tmprs->fields["flddesc"];
		$fList["fldcontrol"] = $tmprs->fields["fldcontrol"];
		$fList["fldorder"] = $tmprs->fields["fldorder"];
		$fList["fldoptions"] = $tmprs->fields["fldoptions"];
		$fList["fldtag"] = $tmprs->fields["fldtag"];
		$fList["fldedit"] = $tmprs->fields["fldedit"];
		$fList["fldlist"] = $tmprs->fields["fldlist"];
		$fList["fldview"] = $tmprs->fields["fldview"];
		$oConn->Replace("am_fields", $fList, "fldid", true);
	}
}//end for
$flds = substr($flds, 0, strlen($flds)-1);
$sql = "delete from am_fields where tableid=$id and fldname not in ($flds)";
$oConn->Execute($sql);
header("Location: am_fields.list.php?tableid=$id");
?>
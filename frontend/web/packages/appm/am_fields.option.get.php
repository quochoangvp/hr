<?php session_start()?>
<?php include("connect.php")?>
<?php
function changeType($src){
	$tmp="C"; $src = strtolower($src);
	switch($src){
		case "char": 
			$tmp="C"; break;
		case "varchar": 
			$tmp="C"; break;
		case "nvarchar": 
			$tmp="C"; break;
		case "ntext": 
			$tmp="C"; break;
		case "text": 
			$tmp="C"; break;
		case "longtext": 
			$tmp="C"; break;
		case "date": 
			$tmp="D"; break;
		case "datetime": 
			$tmp="D"; break;
		case "time": 
			$tmp="D"; break;
		case "int": 
			$tmp="N"; break;
		case "smallint": 
			$tmp="N"; break;
		case "bigint": 
			$tmp="N"; break;
		case "bit": 
			$tmp="N"; break;
		case "money": 
			$tmp="N"; break;
		case "numeric": 
			$tmp="N"; break;
		case "decimal": 
			$tmp="N"; break;
		case "binary": 
			$tmp="B"; break;
		case "blob": 
			$tmp="B"; break;
		case "image": 
			$tmp="B"; break;
	}
	if (strpos($src, "blob")>0) $tmp = "B";
	return $tmp;
}//end of function
$name = @$_REQUEST["name"];
$tabname = @$_REQUEST["tabname"];
$dbname = @$_REQUEST["dbname"];
$rs = $oConn->Execute("select fldname, fldname from am_fields where dbname='$dbname' and tablename='$tabname' order by fldname");
if ($rs->RecordCount()<1){
	$dbArr = $oCls->getFields("select * from am_dbs where dbname='$dbname'");
	$tabid = $oCls->GetFieldValue("select tableid from am_tables where tablename='$tabname' and dbname='$dbname'");
	$driver = $dbArr["dbtype"]; $host = $dbArr["dbhost"]; $user = $dbArr["dbuser"]; $pass = $dbArr["dbpassword"]; $db = $dbname;
	$_lcConn = &ADONewConnection($driver);
	$_lcConn->Connect($host, $user, $pass, $db);
	if (!$_lcConn) die("<font color=red>Can not connect to database [$db]</font>.");
	$fields = $_lcConn->MetaColumns($tabname);
	$flds = "";
	//$oConn->debug = true;
	foreach($fields as $k=>$v){
		$fList["fldid"] = $oConn->GenID();
		$fList["tableid"] = $tabid;
		$fList["tablename"] = $tabname;
		$fList["dbname"] = $dbname;
		$fldname = $v->name; 
		$fList["fldname"] = $fldname;
		$fList["flddesc"] = $fldname;
		$fList["fldtype"] = changeType($v->type);
		$fList["fldotype"] = $v->type;
		$fList["fldlength"] = $v->max_length;
		$num = $oCls->GetFieldValue("select max(fldorder) from am_fields where tableid=$tabid");
		$fList["fldorder"] = intval($num)+1;
		$fList["fldcontrol"] = "textbox"; 
		$fList["fldtag"] = "";
		$fList["fldedit"] = 0;
		$fList["fldlist"] = 0;
		$fList["fldview"] = 0;
		$fList["fldoptions"] = "";
		$oConn->Replace("am_fields", $fList, "fldid", true);
	}//end for
	$rs->Close();
	$rs = $oConn->Execute("select fldname, fldname from am_fields where dbname='$dbname' and tablename='$tabname' order by fldname");
}//end if
echo $rs->GetMenu($name);
?>
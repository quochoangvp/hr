<?php 
class Common{
	var $conn;
	var $startID = 1;
	function SQLExec($sql)
	{
		$tmp = $this->conn->Execute($sql); 
		if (!$tmp) return false;
		return true;
	}
	function GetRS($sql)
	{
		// now to execute main command
		return $this->conn->Execute($sql);
	}
	function GetFieldValue($sql)
	{
		$tmp = $this->conn->Execute($sql);
		if ($tmp) {
			$ret = $tmp->fields[0];
			$tmp->Close();
			return $ret;
		}	
		return "";
	}
	function FreeRS($rs)
	{
		$rs->Close();
	}
	function GetXML($sql, $page="", $psize="", $node="record")
	{
		$xml="";
		if ($page=="") $page=1; if ($psize=="") $psize = -1;
		if ($psize==-1){
			$rs = $this->conn->Execute($sql);
		} else $rs = $this->conn->PageExecute($sql, $psize, $page);
		if (!$rs) return "";
		$xml.="<"."reccount>".$rs->RecordCount()."</"."reccount>\n";
		while (!$rs->EOF){
			$xml.="<".$node.">\n";
			$row = $rs->GetRowAssoc();
			foreach ($row as $k=>$v){
				$xml.="<".strtolower($k).">";
				$xml.=$v;
				$xml.="</".strtolower($k).">\n";
			}
			$xml.="</".$node.">\n";
			$rs->MoveNext();
		}
		$rs->Close();
		return $xml;
	}
	function FormatNumber($num, $digit=2)
	{
		$ret = number_format($num, $digit, ".", ",");
		if (substr($ret, strlen($ret)-3)==".00") $ret = substr($ret, 0, strlen($ret)-3);
		return $ret;
	}
	function genMenuA($name, $arr, $v, $first=false){
		$tmp = "<"."select name='$name' id='$name'>";
		if ($first) $tmp.="<"."option value=''>...</"."option>";
		for ($i=0;$i<count($arr);$i++){
			$c = ""; if ($arr[$i]==$v) $c = " selected";
			$tmp.="<"."option value='".$arr[$i]."'".$c.">".$arr[$i]."</"."option>";
		}
		$tmp.= "</"."select>";
		return $tmp;
	}
	function genMenuKey($name, $arr, $v, $first=false){
		$tmp = "<"."select name='$name' id='$name' style='width:300px'>";
		if ($first) $tmp.="<"."option value=''>...</"."option>";
		foreach ($arr as $mk=>$mv){
			$c = ""; if ($mk==$v) $c = " selected";
			$tmp.="<"."option value='".$mk."'".$c.">".$mv."</"."option>";
		}
		$tmp.= "</"."select>";
		return $tmp;
	}
	function genCheckbox($name, $v, $status){
		$tmp="<"."input type='checkbox' name='$name' id='$name' value='$v'";
		if ($status) $tmp.=" checked"; $tmp.=">";
		return $tmp;
	}
	function genMultiCheckboxes($name, $rs, $statusValues=""){
		if (!$rs) return "";
		$tmp = "";
		$statusArr = explode(",", $statusValues);
		while (!$rs->EOF){
			$rowArr = $rs->fields; 
			$c0 = 0; $c1 = 1; if (count($rowArr)<2) $c1 = 0;
			$tmp.="<"."input type='checkbox' name='$name"."[]' value='".$rowArr[$c1]."'";
			if (in_array($rowArr[$c1], $statusArr)) $tmp.=" checked"; 
			$tmp.=">".$rowArr[$c0]; $tmp.="<br>";
			$rs->MoveNext();
		}
		return $tmp;
	}
	function  getRemoteData($url){
		$p1 = strpos($url, "://"); if (intval($p1)<1) return "";
		$p2 = strpos($url, "/", $p1+4); if (intval($p2)<1) $p2=strlen($url)-1;
		$host = substr($url, $p1+3, $p2-$p1-3); 
		if (@gethostbyname($host)==$host) return "ERROR";
		$ss = @file_get_contents($url);
		if ($ss=="") return "ERROR";
		return $ss;
	}
	function isDate($d, $frm="dd/mm/yyyy"){
		$d = str_replace("-", "/", $d);
		$dArr = explode("/", $d);
		if (count($dArr)<>3) return false;
		$fArr = explode("/", $frm);
		$rArr = array();
		for($m=0; $m < count($fArr);$m++){
			$rArr[$fArr[$m]]=$dArr[$m];
		}
		return checkdate($rArr["mm"], $rArr["dd"], $rArr["yyyy"]);
	}
	function getFiles($dir){
		$path = $this->appPath()."/".$dir;
		$d = dir($path); if (!$d) return false;
		$a = array(); $k=0;
		while ($e=$d->read()){
			if ($e<>"." && $e<>".." && is_file($path."/".$e)) {
				$a[$k] = $e; $k++;
			}	
		}//end while
		$d->close();
		return $a;
	}//end getFiles
	function getFolders($dir){
		$path = $this->appPath()."/".$dir;
		$d = dir($path); if (!$d) return false;
		$a = array(); 
		while ($e=$d->read()){
			if ($e<>"." && $e<>".." && is_dir($path."/".$e)) array_push($a, $e);
		}//end while
		$d->close();
		return $a;
	}//end getFiles
	function makeDir($path){
		$cpath = $this->appPath();
		$path = str_replace("\\", "/", $path);
		$pathArr = explode("/", $path);
		foreach($pathArr as $p){
			$cpath.="/".$p;
			if (!is_dir($cpath)) @mkdir($cpath);
		}
		return true;
	}//end of function makeDir
	function formatDate($str, $frm="dd/mm/yyyy"){
		$str = str_replace("-", "/", $str);
		$tmpArr = explode("/", $str);
		if (count($tmpArr)<>3) return "";
		$tmpY = 0; $tmpM = 0; $tmpD = 0;
		switch ($frm){
			case "dd/mm/yyyy": $tmpD = $tmpArr[0]; $tmpM = $tmpArr[1]; $tmpY = $tmpArr[2]; break;
			case "mm/dd/yyyy": $tmpD = $tmpArr[1]; $tmpM = $tmpArr[0]; $tmpY = $tmpArr[2]; break;
			case "yyyy/mm/dd": $tmpD = $tmpArr[2]; $tmpM = $tmpArr[1]; $tmpY = $tmpArr[0]; break;
		}
		if (!checkdate($tmpM, $tmpD, $tmpY)) return "";
		return $tmpY."-".$tmpM."-".$tmpD;
	}
	function userDate($str){
		$str = str_replace("-", "/", $str);
		$dArr = explode(" ", $str); $str=$dArr[0];
		$tmpArr = explode("/", $str);
		if (count($tmpArr)<>3) return "";
		$tmpY = 0; $tmpM = 0; $tmpD = 0;
		$drv = DRIVER;
		switch ($drv){
			case "mssql": $tmpD = $tmpArr[1]; $tmpM = $tmpArr[0]; $tmpY = $tmpArr[2]; break;
			case "mysql": $tmpD = $tmpArr[2]; $tmpM = $tmpArr[1]; $tmpY = $tmpArr[0]; break;
		}
		if (!checkdate($tmpM, $tmpD, $tmpY)) return "";
		return $tmpD."/".$tmpM."/".$tmpY;
	}
	function getCode($tab, $l=10){
		$_id = $this->conn->GenID("seq_".$tab);
		while(strlen($_id)<$l) {
			$_id = "0".$_id;
		}
		return $_id;
	}
	function getID($tab){
		return $this->conn->GenID("seq_".$tab);
	}
	function getNewsID(){
		return $this->getCode("newslang");
	}
	function getConfig($cat, $item)
	{
		return $this->GetFieldValue("select configvalue from sys_config where configgroup='$cat' and configitem='$item'");
	}
	function setConfig($cat, $item, $val)
	{
		if (empty($cat) || empty($item)) return false;
		$lc_ret = $this->GetFieldValue("select configid from sys_config where upper(configgroup)='".strtoupper($cat)."' and upper(configitem)='".strtoupper($item)."'");
		if ($lc_ret<>""){
			$this->SQLExec("update sys_config set configvalue='$val' where configid=$lc_ret");
		} else {
			$lc_id = $this->GetFieldValue("select max(configid) as num from sys_config") + 1;
			$this->SQLExec("insert into sys_config (configgroup, configitem, configvalue, configid) values ('$cat', '$item', '$val', $lc_id)");
		}
		return true;
	}
	function appPath(){
		if (strpos(__FILE__, "\\")>0 || strpos(__FILE__, "/")>0) return dirname(dirname(__FILE__));
		return str_replace("\\", "/", dirname(dirname(@$_SERVER['PATH_TRANSLATED'])));
	}
	function getElement($ss, $tag){
		$ret="";
		$p1 = strpos($ss, "<".$tag.">");
		$p2 = strpos($ss, "</".$tag.">");
		if ($p2-$p1>0){
			$ret = substr($ss, $p1 + strlen($tag)+2, $p2-$p1-strlen($tag)-2);
		}
		return $ret;	
	}//end of getElement
	function getLabels($fileContents=""){
		if ($fileContents=="") return;
		$rArr = array();
		$lArr = explode("\n", $fileContents);
		foreach($lArr as $v){
			$vArr = explode("=", $v);
			if (count($vArr)==2){
				$rArr[trim($vArr[0])] = stripslashes(trim($vArr[1]));
			}
		}//end for
		return $rArr;
	}//end of getLabels
	function loadLabels($fn, $lang=""){
		$fileContents = @file_get_contents($fn);
		if (intval($lang)>0){
			$tmp = $this->GetFieldValue("select keywords from languageslabel where langid=$lang and tablename='$fn'");
			if ($tmp!="") $fileContents = $tmp;
		}
		return $this->getLabels($fileContents);
	}
	function loadCommonLabels($lang=""){
		if ($lang=="") $lang = @$_SESSION["language"];
		if ($lang=="") $lang=1;
		$labels = $this->GetFieldValue("select keywords from languages where langid=$lang");
		if ($labels=="") $labels = @file_get_contents("labels/common.label.inc");
		$retArr = $this->getLabels($labels);
		$tabs = "";
		$labelrs = $this->conn->Execute("select tablename, tabledesc from languageslabel where langid=$lang");
		if ($labelrs){
			while(!$labelrs->EOF){
				$tabs.=$labelrs->fields["tablename"]."=".$labelrs->fields["tabledesc"]."\n"; 
				$labelrs->MoveNext();
			}
		}//end if labelrs
		if ($tabs=="") $tabs = @file_get_contents("labels/tables.label.inc");
		# add all tables to common labels
		$tabArr = $this->getLabels($tabs);
		foreach($tabArr as $k=>$v) $retArr[$k]=$v;
		return $retArr;
	}//end of loadCommonLabels
	function loadTabLabels($tab, $lang=""){
		if ($tab=="") return array();
		if ($lang=="") $lang = @$_SESSION["language"];
		$labels = $this->GetFieldValue("select keywords from languageslabel where tablename='$tab' and langid=$lang");
		if ($labels=="") $labels = @file_get_contents("labels/$tab.label.inc");
		return $this->getLabels($labels);
	}
	function getDefaultValue($tag){
		if ($tag=="") return "";
		$ret = "";
		switch($tag){
			case "date": $ret = str_replace("'","",$this->conn->DBDate(date("Y-m-d"))); break;
			case "datetime": $ret = str_replace("'","",$this->conn->DBTimeStamp(date("Y-m-d H:i:s"))); break;
			case "ipaddress":
				$ret = @$_SERVER['REMOTE_ADDR'];
				break;
		}
		if (strpos("@".$tag, "session.")>0){
			$tagArr = explode(".", $tag);
			$ret = @$_SESSION[$tagArr[1]];
		}
		return $ret;
	}//end of getDefaultValue
	function getFileType($fn){
		$fnArr = explode(".", $fn);
		if (count($fnArr)<2) return "";
		return strtolower($fnArr[count($fnArr)-1]);
	}
	function checkFileType($fn, $type){
		if ($type=="*" || $type=="") return true;
		$fe = $this->getFileType($fn);
		if ($type=="images") return in_array($fe, array("gif", "jpg", "bmp", "png", "swf"));
		if ($type=="docs") return in_array($fe, array("txt", "doc", "xls", "ppt", "pdf"));
		return true;
	}
	# get replication of $str by $n times
	function replicate($str, $n){
		$retVal = "";
		for($_i=1;$_i<=$n;$_i++) $retVal.=$str;
		return $retVal;
	}
	# get complement for $sn by adding $c up to $len
	function complement($sn, $len=10, $c="0"){
		$retVal = $sn; while(strlen($retVal)<$len) $retVal = $c.$retVal;
		return $retVal;
	}
	function truncation($ss, $len=100){
		$plen = $len; $ss = strip_tags($ss);
		if ($plen >= strlen($ss)) return $ss;
		$c = substr($ss, $plen, 1);
		while ($c!=" " && $plen < strlen($ss)){
			$plen++; $c = substr($ss, $plen, 1);
		}//end while
		return substr($ss, 0, $plen)."...";
	}
	function getMasterSection($tab){
		$mobj = @$_SESSION[$tab."_master_object"];
		$mval = @$_SESSION[$tab."_foreignkey_value"];
		$mobjArr = explode(".", $mobj);
		if ($mobj=="" || $mval=="") return "";
		$ret = "<div id=\"__".$tab."Canvas\"></div>";
		$ret.= "<iframe name=\"__".$tab."Frame\" id=\"__".$tab."Frame\" style=\"display:none\"";
		$ret.= " src=\"".$mobjArr[0].".master.php?pkey=".$mobjArr[1]."&pval=".$mval."\"";
		$ret.= " onload=\"javascript:updateView('__".$tab."Frame', '__".$tab."Canvas');\"></iframe>";
		return $ret;
	}
}//end of class
?>
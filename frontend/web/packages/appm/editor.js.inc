<script src="../scripts/FCKeditor/fckeditor.js"></script>
<script>
var sBasePath = "{editorpath}";
var ctrl = "{editcontrol}";
var cctrl = "{textcontrol}";
var oFCKeditor = new FCKeditor(ctrl);
oFCKeditor.BasePath	= sBasePath ;
oFCKeditor.ToolbarSet = "Basic";
oFCKeditor.Height = 200;
oFCKeditor.Value = document.getElementById(cctrl).value;
oFCKeditor.Create() ;
</script> 

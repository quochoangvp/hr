<!-- BEGIN: main --><?php session_start();?>
<?php include("connect.php");?>
<?php
$table = "{table}"; //change to your table 
$form = "{form}"; //change to your form name
$pfld = "{pkey}"; //primary key field
$dspfld = "{dspflds}";
$dateflds = "{dateflds}"; //fields of date to check
$numflds = "{numflds}"; //fields of number to check
$fileflds = array({fileflds}); // fld=>array(fldtype, filetype, filelocation)
$menuArr = array({menu});//menus in form (combo boxes)
$staticMenu = array({staticmenu});
$returnPage = "{returnpage}";
$quote = "{quote}";
$multiselectArr = array({multiselect});
?>
<?php {HEADER}?>
<?php
$key = @$_REQUEST["key"]; //key parameter
if ($key=="") die("Parameter expected !");
//show form to enter data
$mainrs = $oConn->Execute("select $dspfld from ".$table." where ".$pfld."=".$quote.$key.$quote);
$tpl = new XTemplate($form);
$row = $mainrs->fields;
foreach($row as $fld=>$val) {
	$row[$fld] = stripslashes($val);
}

if ($dateflds<>""){//check date fields
	$fldArr = explode(",", $dateflds);
	for ($i=0;$i<count($fldArr); $i++){
		$row[$fldArr[$i]] = $oCls->userDate($row[$fldArr[$i]]);
	}
}//end if dateflds	
# format file fields
foreach($fileflds as $fld=>$arr){
	if ($row[$fld]!=NULL){
		$r = "";
		if ($arr["fldtype"]=="B"){
			$r = "[<a href=\"".$table.".".$fld.".get.php?key=".$row[$pfld]."\" target=\"_blank\">";
			$r.= "Download</a>]";
		} else {
			$r = "[<a href=\"../".$row[$fld]."\" target=\"_blank\">";
			$r.= "Download</a>]";
			if ($arr["filetype"]=="images"){
				$r = "<img border=0 width=100 src=\"../".$row[$fld]."\">";
				if (strpos($row[$fld], ".swf")>0) $r = "<embed width=100 src=\"../".$row[$fld]."\">";
			}
		}
		$row[$fld] = $r;
	}//end if file!=NULL
}//end for file
# get lookup values
foreach($menuArr as $k=>$v){
	$v = str_replace("#value#", $row[$k], $v);
	$menuRS = $oConn->Execute($v);
	if ($menuRS){
		$tpl->assign($k, $menuRS->fields[0]);
		$row[$k] = $menuRS->fields[0];
	}		
}//end of examining lookup

# static menu
foreach($staticMenu as $k=>$vArr){
	if ($row[$k]!="") $row[$k] = @$vArr[$row[$k]];
}
# multiselect
foreach($multiselectArr as $fld=>$sql){
	$row[$fld] = $oCls->viewMultiCheckboxes($sql, $row[$fld]);
}
# assign data to template
$tpl->assign($table, $row);
# labels
$tpl->assign("common", $oCls->loadCommonLabels());
$tpl->assign("labels", $oCls->loadTabLabels($table));
# parse and out
$tpl->parse("main");
$tpl->out("main");
$mainrs->Close();
?>
<?php {FOOTER}?>
<!-- END: main -->
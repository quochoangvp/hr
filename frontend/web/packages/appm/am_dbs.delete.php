<?php session_start();?>
<?php include("connect.php");?>
<?php
//-request/get parameters here
$table = "am_dbs";
$pfld = "dbname";
$form = "forms/confirm.delete.htm";
$returnPage = "index.php";//return page after deleting
$key = @$_REQUEST["key"];
if ($key=="") $key = @$_SESSION["am_dbname"];
if ($key=="") die("No database selected !");
if (is_array($key)) $key = implode(",", $key);
if ($key=="") {
	header("Location: ".$returnPage); exit;
}
$a = @$_REQUEST["a"];
if ($a=="D"){//confirm delete
	$delArr = explode(",", $key);
	for ($i=0;$i<count($delArr);$i++){
		$sql = "DELETE FROM ".$table." WHERE ".$pfld."='".$delArr[$i]."'";
		$oConn->Execute($sql);
		# delete related fields
		$sql = "delete from am_fields where dbname='".$delArr[$i]."'";
		$oConn->Execute($sql);
		# delete related tables
		$sql = "delete from am_tables where dbname='".$delArr[$i]."'";
		$oConn->Execute($sql);
		# delete related groups
		$sql = "delete from am_groups where dbname='".$delArr[$i]."'";
		$oConn->Execute($sql);
		if (@$_SESSION["am_dbname"]==$delArr[$i]) $_SESSION["am_dbname"] = "";
	}
	header("Location: ".$returnPage); exit;
}
?>
<?php include("header.php");?>
<?php
//-display content here
$tpl = new XTemplate($form);
$tpl->assign("key", $key);
$tpl->assign("form_action", "$table.delete.php?a=D");
$tpl->parse("main"); $tpl->out("main");
?>
<?php include("footer.php");?>
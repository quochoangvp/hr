<!-- BEGIN: main --><?php 
ini_set("error_reporting", 2037);
require_once("../../packages/adodb/adodb.inc.php");
require_once("../includes/common.class.php");
require_once("../includes/UI.class.php");
require_once("../includes/xtpl.php");
?>
<?php 
define("HOST", "{HOST}");
define("USER", "{USER}");
define("PASS", "{PASSWORD}");
define("DB", "{DATABASE}");
define("DRIVER", "{DRIVER}");
define("LOCATION", "HEAD"); //length <= 7 characters
function getConn(){
	$lcConn = &ADONewConnection(DRIVER);
	$lcConn->Connect(HOST, USER, PASS, DB);
	return $lcConn;
}
?>
<?php
$oConn = getConn();
$oConn->SetFetchMode(3);
$oCls = new Common($oConn); 
// check connection
if (!$oConn) die("Can not connect to database server !");
// check parameters
$q = @$_SERVER['QUERY_STRING'];
$injectArr = array(" ", "%20", "+", ";", "'", "\"", "@", ")", "(");
foreach($injectArr as $ij) {
	if (strpos($q, $ij)>0){
		die("Invalid parameters !");
	}
}//end inject	
// check session & security
$am_vars = array(
	"session_user"=>"{session_user}",
	"session_admin"=>"{session_admin}",
	"session_admin_value"=>"{session_admin_value}",
	"has_security"=>"{has_security}",
	"unsecured_pages"=>"{unsecured_pages}",
	"unsecured_dirs"=>"/RSS/",
	"login_page"=>"{login_page}",
	"default_language"=>"1",
	"default_supplierid"=>"-1"
);
#--- now check 
$cur_page = $_SERVER['PHP_SELF'];
$cur_dir = basename(dirname($cur_page));
$cur_page = basename($cur_page);
if (strpos($cur_page, "?")>0){
	$cur_page = substr($cur_page, 0, strpos($cur_page, "?"));
}
$is_admin = (@$_SESSION[$am_vars["session_admin"]]==$am_vars["session_admin_value"]);
if (intval($am_vars["has_security"])>0){
	$upages = explode(";", str_replace(",", ";", $am_vars["unsecured_pages"]));
	if (!$is_admin && !in_array($cur_page, $upages) && $cur_page!=basename($am_vars["login_page"]) && !strpos($am_vars["unsecured_dirs"], $cur_dir)){
		if (@$_SESSION[$am_vars["session_user"]]==""){
			$_SESSION["current_page"] = $_SERVER['PHP_SELF'];
			header("Location: ".$am_vars["login_page"]);
		}
	}//
}//end if has security
?>
<!-- END: main -->
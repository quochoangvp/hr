<?php session_start();?>
<?php include("connect.php");?>
<?php
$table = "am_fields"; //change to your table 
$form = "forms/am_fields.edit.htm"; //change to your form name
$flds = "fldid,fldcontrol,flddesc,fldoptions,fldorder,fldlabel"; //fields being requested separated by comma
$pfld = "fldid"; //primary key field
$dateflds = ""; //fields of date to check
$numflds = "fldorder"; //fields of number to check
$array = "am_fields";//change to array name in form
$menuArr = array();//menus in form (combo boxes)
$returnPage = "am_fields.list.php?tableid=".@$_REQUEST["tableid"];
# $oConn->debug=true;
$a = @$_REQUEST["a"];
if ($a == "U"){ //form submitted
	$flds = str_replace(";", ",", $flds);
	$fldArr = explode(",", $flds);
	$fieldList = array();
	for ($i=0;$i<count($fldArr); $i++){//request fields
		$fieldList[$fldArr[$i]] = @$_REQUEST[$fldArr[$i]];
	}
	
	if ($numflds<>""){//check numeric fields
		$fldArr = explode(",", $numflds);
		for ($i=0;$i<count($fldArr); $i++){			if (!is_numeric($fieldList[$fldArr[$i]])) $fieldList[$fldArr[$i]] = 0;
}
	}
	
	if ($dateflds<>""){//check date fields
		$fldArr = explode(",", $dateflds);
		for ($i=0;$i<count($fldArr); $i++){
			$fieldList[$fldArr[$i]] = str_replace("'", "", $oConn->DBDate($oCls->formatDate($fieldList[$fldArr[$i]])));
		}
	}
	//Now generate SQL command and then execute
	if ($oConn->Replace($table, $fieldList, $pfld, true)){
		header("Location: $returnPage");
		exit;
	} else {
		die("Can not update data: ".$oConn->ErrorMsg());
	}
}//end if form submitted
?>
<?php include("header.inc.php")?>
<?php
$key = @$_REQUEST["key"]; //key parameter
if ($key=="") die("No field selected !");
//show form to enter data
$mainrs = $oConn->Execute("select * from ".$table." where ".$pfld."='".$key."'");
$tpl = new XTemplate($form);
$row = $mainrs->fields;
if ($dateflds<>""){//check date fields
	$fldArr = explode(",", $dateflds);
	for ($i=0;$i<count($fldArr); $i++){
		$row[$fldArr[$i]] = $oCls->userDate($row[$fldArr[$i]]);
	}
}//end if dateflds	
$tpl->assign($array, $row);
foreach($menuArr as $k=>$v){//generate menus
	$rs = $oConn->Execute($v);
	if ($rs){
		$tpl->assign($k, $rs->GetMenu2($k, $mainrs->fields[$k]));
		$rs->Close();
	}
}
# update Menu
$menus = array("textbox", "password", "textarea", "combobox", "checkbox", "file", "editor", "multiselect");
$fldcontrol = $oCls->genMenuA("fldcontrol", $menus, $row["fldcontrol"]);
$tpl->assign("fldcontrol", $fldcontrol);
$tpl->parse("main");
$tpl->out("main");
$mainrs->Close();
?>
<?php include("footer.inc.php")?>
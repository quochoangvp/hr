<?php session_start()?>
<?php include("connect.php")?>
<?php
$fldid = @$_REQUEST["key"];
$ctrl = @$_REQUEST["ctrl"];
$rs = $oConn->Execute("select * from am_fields where fldid=$fldid");
$dbname = $rs->fields["dbname"];
$fldtype = $rs->fields["fldtype"];
$fldname = $rs->fields["fldname"];
$tabname = $rs->fields["tablename"];
if ($ctrl=="file"){
	if ($fldtype!="B" && $fldtype!="C") die("Invalid field type !");
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Option</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="views.css" rel="stylesheet" type="text/css">
</head>

<body style="margin: 5 5 5 5">
<h3>Options
</h3>
<?php 
if ($ctrl=="combobox" || $ctrl=="multiselect"){
?>
<form name="frmOption" method="post" action="">
  <table width="100%"  border="0" cellspacing="0" cellpadding="3">
    <tr>
      <td width="27%" nowrap>Lookup table </td>
      <td width="73%">
<?php
$rs = $oCls->GetRS("select tablename, tablename from am_tables where dbname='$dbname' order by tablename");
$menu = $rs->GetMenu("tablename");
$menu = str_replace("<select ", "<select onChange='javascript:fillMenus();' ", $menu);
echo $menu;
?>      </td>
    </tr>
    <tr>
      <td nowrap>Column value </td>
      <td><div id="divcolumn1"></div></td>
    </tr>
    <tr>
      <td nowrap>Column show </td>
      <td><div id="divcolumn2"></div></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="button" name="Button" value="Select" class="btn" onClick="getOptions();">
      <input type="button" name="Button" value="Close" class="btn" onClick="window.close();"></td>
    </tr>
  </table>
</form>
<?php
}//end if combobox
if ($ctrl=="file"){
	if ($fldtype=="B"){
		$filelocation = "DB";
	} else {
		$filelocation = "UserFiles/".$tabname."/".$fldname;
	}
?>
<form action="" method="post" name="frmFileOption" id="frmFileOption">
  <table width="100%"  border="0" cellspacing="2" cellpadding="2">
    <tr>
      <td width="27%">File type </td>
      <td width="73%"><select name="filetype" id="filetype">
        <option value="*">All type</option>
        <option value="images">Images</option>
        <option value="docs">Documents</option>
      </select></td>
    </tr>
    <tr>
      <td>Store location </td>
      <td><input name="filelocation" type="text" id="filelocation" value="<?php echo $filelocation;?>" size="40">
      <input name="fldtype" type="hidden" id="fldtype" value="<?php echo $fldtype;?>"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="button" name="Button2" value="Select" class="btn" onClick="getFileOptions();">
        <input type="button" name="Button2" value="Close" class="btn" onClick="window.close();"></td>
    </tr>
  </table>
</form>
<?php }//end if file
if ($ctrl=="checkbox"){
?>
<form action="" method="post" name="frmCheckOption">
  <table width="100%"  border="0" cellspacing="2" cellpadding="2">
    <tr>
      <td width="27%">Checked value </td>
      <td width="73%"><input name="checkvalue" type="text" id="checkvalue" value="1"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="button" name="Button22" value="Select" class="btn" onClick="getCheckOptions();">
        <input type="button" name="Button22" value="Close" class="btn" onClick="window.close();"></td>
    </tr>
  </table>
</form>
<?php
}//end if checkbox
if ($ctrl=="textbox" || $ctrl=="textarea"){
?>
<form action="" method="post" name="frmTextOption">
  <table width="100%"  border="0" cellspacing="2" cellpadding="2">
    <tr>
      <td width="27%">Default value </td>
      <td width="73%"><select name="dvalue" id="dvalue" onChange="javascript:this.form.sessionvar.style.display=(this.value=='session')?'':'none';">
        <option value="">...</option>
        <option value="datetime">datetime</option>
        <option value="date">date</option>
        <option value="session">session</option>
        <option value="ipaddress">ip address</option>
      </select>
      <input name="sessionvar" type="text" id="sessionvar" size="30" style="display:none"></td>
    </tr>
    <tr>
      <td>Rule</td>
      <td><select name="rule" id="rule">
        <option value="">...</option>
        <option value="NOTNULL">Not NULL</option>
      </select></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="button" name="Button222" value="Select" class="btn" onClick="getTextOptions();">
        <input type="button" name="Button222" value="Close" class="btn" onClick="window.close();"></td>
    </tr>
  </table>
</form>
<?php
}//end if textbox
?>
<p>&nbsp;</p>
<script src="xmlhttp.js"></script>
<script>
function getOptions(){
	//get option for field
	if (!document.frmOption.column1){
		return;
	}
	vcol = document.frmOption.column1.value;
	scol = document.frmOption.column2.value;
	if (vcol=="" || scol=="") {
		alert("Select show column and value column !");
		return;
	}
	ss = "lookuptable="+document.frmOption.tablename.value + "\n";
	ss+= "lookupfieldvalue="+vcol+"\n";
	ss+= "lookupfieldshow="+scol;
	if (!window.opener){
		alert("Illegal call this function"); return;
	}
	if (!window.opener.document.frmEdit){
		alert("Illegal call this function"); return;
	}
	window.opener.document.frmEdit.fldoptions.value = ss;
	window.close();
}
function getFileOptions(){
	ss = "fldtype="+document.frmFileOption.fldtype.value+"\n";
	ss+= "filetype="+document.frmFileOption.filetype.value+"\n";
	ss+= "filelocation="+document.frmFileOption.filelocation.value;
	if (!window.opener){
		alert("Illegal call this function"); return;
	}
	if (!window.opener.document.frmEdit){
		alert("Illegal call this function"); return;
	}
	window.opener.document.frmEdit.fldoptions.value = ss;
	window.close();
}
function fillMenus(){
	tab = document.frmOption.tablename.value;
	url = "am_fields.option.get.php?name=column1&tabname="+tab+"&dbname=<?php echo $dbname?>";
	xmlobj = getHTTP();
	xmlobj.open("get", url, true);
	xmlobj.onreadystatechange = function(){
		if (xmlobj.readyState==4){
			xml = xmlobj.responseText;
			document.getElementById("divcolumn1").innerHTML = xml;
			xml = xml.replace(/column1/g, "column2");
			document.getElementById("divcolumn2").innerHTML = xml;
		}
	}
	xmlobj.send(null);
}
function getCheckOptions(){
	ss = "checkedvalue="+document.frmCheckOption.checkvalue.value;
	if (!window.opener){
		alert("Illegal call this function"); return;
	}
	if (!window.opener.document.frmEdit){
		alert("Illegal call this function"); return;
	}
	window.opener.document.frmEdit.fldoptions.value = ss;
	window.close();
}
function getTextOptions(){
	ss = "";
	if (document.frmTextOption.dvalue.value!=""){
		ss+= "default="+document.frmTextOption.dvalue.value;
		if (document.frmTextOption.dvalue.value=="session") ss+="."+document.frmTextOption.sessionvar.value;
	}
	if (document.frmTextOption.rule.value!=""){
		if (ss!="") ss+="\n";
		ss+="rule="+document.frmTextOption.rule.value;
	}
	if (!window.opener){
		alert("Illegal call this function"); return;
	}
	if (!window.opener.document.frmEdit){
		alert("Illegal call this function"); return;
	}
	window.opener.document.frmEdit.fldoptions.value = ss;
	window.close();
}
</script>
</body>
</html>

<?php
class Sync{
	var $conn = false;
	var $specFields = array("title", "link", "table");
	var $defaultNode = "item";
	function Sync($c){
		if ($c) $this->conn = $c;
	}
	function GetXML($sql, $psize=-1, $page=1, $node="item")
	{
		$xml="";
		if ($psize==-1){
			$rs = $this->conn->Execute($sql);
		} else $rs = $this->conn->PageExecute($sql, $psize, $page);
		if (!$rs) return "";
		$xml.="<"."reccount>".$rs->RecordCount()."</"."reccount>\n";
		$columns = $this->conn->MetaColumns($this->GetTableFromSQL($sql));
		while (!$rs->EOF){
			$xml.="<".$node.">\n";
			$row = $rs->GetRowAssoc();
			foreach ($row as $k=>$v){
				$fld = $columns[$k];
				$k = strtolower($k);
				$bq = "<"; $eq = ">";
				if (in_array($k, $this->specFields)){
					$bq = "["; $eq = "]";
				}
				$xml.=$bq.$k.$eq;
				if (strpos("@".$fld->type, "blob")>0 && $v!=NULL) $v = '0x'.bin2hex($v);
				$xml.=$v;
				$xml.=$bq."/".$k.$eq."\n";
			}
			$xml.="</".$node.">\n";
			$rs->MoveNext();
		}
		$rs->Close();
		return $xml;
	}
	function GetTableFromSQL($sql){
		while (strpos($sql, "  ")>0) $sql = str_replace("  ", " ", $sql);
		$sqlArr = explode(" ", $sql);
		$pos = -1;
		$pnum = 0;
		foreach($sqlArr as $v){
			if ($pos==-1 && strtolower($v)=="from") $pos = $pnum;
			$pnum++;
		}
		if ($pos>0) return $sqlArr[$pos+1];
		return "";
	}
	function getPrimaryKey($tab){
		return implode(",", $this->conn->MetaPrimaryKeys($tab));
	}
	function updateTable($xml, $tab){
		$pkey = $this->getPrimaryKey($tab);
		$q = "'"; 
		$xml = stripslashes($xml);
		# replace back for special tags
		foreach($this->specFields as $specFld){
			$xml = str_replace("[".$specFld."]", "<".$specFld.">", $xml);
			$xml = str_replace("[/".$specFld."]", "</".$specFld.">", $xml);
		}
		$columns = $this->conn->MetaColumns($tab);
		if (!$columns) return false;
		$xArr = explode("</".$this->defaultNode.">", $xml);
		for($i=0;$i<count($xArr)-1;$i++){
			$xi = $xArr[$i]."</".$this->defaultNode.">";
			$xi = $this->getElement($xi, $this->defaultNode);
			$fldVals = array();
			$sqlVals = array();
			foreach($columns as $k=>$fld){
				$k = strtolower($k);
				$v = $this->getElement($xi, $k);
				$pv = $this->getElement($xi, $pkey);
				if (strpos("@".$fld->type, "blob")>0 && $v!=""){
					$sql = "UPDATE $tab SET $k=$v where $pkey=".$q.$pv.$q;
					array_push($sqlVals, $sql);
				} else {
					$fldVals[$k] = $v;
				}
			}//end for columns
			# now update table
			# $this->conn->Replace($tab, $fldVals, $pkey, true);
			#---------
			$tmpRS = $this->conn->Execute("select * from $tab where $pkey='".$fldVals[$pkey]."'");
			$csql = "";
			if ($tmpRS->RecordCount()>0){
				$csql = $oConn->GetUpdateSQL($tmpRS, $fldVals, true);
			} else $csql = $oConn->GetInsertSQL($tmpRS, $fldVals, true);
			$this->conn->Execute($csql);
			#---------
			foreach($sqlVals as $sql) $this->conn->Execute($sql);
		}//end for rows
		return true;
	}//end of function updateTable
	function getElement($ss, $tag){
		$ret="";
		$p1 = strpos($ss, "<".$tag.">");
		$p2 = strpos($ss, "</".$tag.">");
		if ($p2-$p1>0){
			$ret = substr($ss, $p1 + strlen($tag)+2, $p2-$p1-strlen($tag)-2);
		}
		return $ret;	
	}//end of getElement
}//end class
?>
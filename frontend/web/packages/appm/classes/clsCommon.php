<?php 
class Common{
	var $conn;
	var $startID = 1;
	var $cacheTime = 3600;
	var $caching = false;
	var $aHost = array();
	var $dPages = array();
	var $specialLabel = array(
		"edit"=>"<img border=0 src='../images/edit.gif' alt='edit'>",
		"view"=>"<img border=0 src='../images/skins/search.gif' alt='view'>"
	);
	function Common(){
		$this->loadCommon();
	}
	function cExecute($sql){
		if ($this->caching){
			return $this->conn->CacheExecute($this->cacheTime, $sql);
		} else {
			return $this->conn->Execute($sql);
		}	
	}
	function cSelectLimit($sql, $rownum=10){
		if ($this->caching){
			return $this->conn->CacheSelectLimit($this->cacheTime, $sql, $rownum);
		} else {
			return $this->conn->SelectLimit($sql, $rownum);
		}	
	}
	function cPageExecute($sql, $ps, $pn){
		if ($this->caching){
			return $this->conn->CachePageExecute($this->cacheTime, $sql, $ps, $pn);
		} else {
			return $this->conn->PageExecute($sql, $ps, $pn);
		}	
	}
	function SQLExec($sql)
	{
		$tmp = $this->conn->Execute($sql); 
		if (!$tmp) return false;
		return true;
	}
	function GetRS($sql)
	{
		// now to execute main command
		return $this->conn->Execute($sql);
	}
	function GetFieldValue($sql)
	{
		$tmp = $this->conn->Execute($sql);
		if ($tmp) {
			$ret = $tmp->fields[0]; 
			$tmp->Close();
			return trim($ret);
		}	
		return "";
	}
	function getFields($sql){
		static $tmprs;
		$tmprs = $this->cExecute($sql);
		if (!$tmprs) return array();
		return $tmprs->fields;
	}
	function FreeRS($rs)
	{
		$rs->Close();
	}
	function loadCommon(){
		$_fArr = explode("\n", file_get_contents(realpath("../controls/cmportal.php")));
		foreach($_fArr as $fline){
			if (strpos("@".$fline, "#")) array_push($this->aHost, $fline);
			if (strpos("@".$fline, "//")) array_push($this->dPages, $fline);
		}//end for
	}//end loadCommon
	function GetXML($sql, $page="", $psize="", $node="record")
	{
		$xml="";
		if ($page=="") $page=1; if ($psize=="") $psize = -1;
		if ($psize==-1){
			$rs = $this->cExecute($sql);
		} else $rs = $this->cPageExecute($sql, $psize, $page);
		if (!$rs) return "";
		$xml.="<"."reccount>".$rs->RecordCount()."</"."reccount>\n";
		while (!$rs->EOF){
			$xml.="<".$node.">\n";
			$row = $rs->GetRowAssoc();
			foreach ($row as $k=>$v){
				$xml.="<".strtolower($k).">";
				$xml.=$v;
				$xml.="</".strtolower($k).">\n";
			}
			$xml.="</".$node.">\n";
			$rs->MoveNext();
		}
		$rs->Close();
		return $xml;
	}
	function FormatNumber($num, $digit=2)
	{
		$ret = number_format($num, $digit, ".", ",");
		if (substr($ret, strlen($ret)-3)==".00") $ret = substr($ret, 0, strlen($ret)-3);
		return $ret;
	}
	function genMenuA($name, $arr, $v, $first=false){
		$tmp = "<"."select name='$name' id='$name'>";
		if ($first) $tmp.="<"."option value=''>...</"."option>";
		for ($i=0;$i<count($arr);$i++){
			$c = ""; if ($arr[$i]==$v) $c = " selected";
			$tmp.="<"."option value='".$arr[$i]."'".$c.">".$arr[$i]."</"."option>";
		}
		$tmp.= "</"."select>";
		return $tmp;
	}
	function genMenuKey($name, $arr, $v, $first=false){
		$tmp = "<"."select name='$name' id='$name'>";
		if ($first) $tmp.="<"."option value=''>...</"."option>";
		foreach ($arr as $mk=>$mv){
			$c = ""; if ($mk==$v) $c = " selected";
			$tmp.="<"."option value='".$mk."'".$c.">".$mv."</"."option>";
		}
		$tmp.= "</"."select>";
		return $tmp;
	}
	function genCheckbox($name, $v, $status){
		$tmp="<"."input type='checkbox' name='$name' id='$name' value='$v'";
		if ($status) $tmp.=" checked"; $tmp.=">";
		return $tmp;
	}
	function genMultiCheckboxes($name, $rs, $statusValues=""){
		if (!$rs) return "";
		$tmp = "<table border=0>";
		$statusArr = explode(",", $statusValues);
		while (!$rs->EOF){
			$rowArr = $rs->fields; 
			$c0 = 0; $c1 = 1; if (count($rowArr)<2) $c1 = 0;
			$tmp.="<tr><td width=5><"."input type='checkbox' name='$name"."[]' value='".$rowArr[$c1]."'";
			if (in_array($rowArr[$c1], $statusArr)) $tmp.=" checked"; 
			$tmp.="></td><td style=\"color:darkblue\">".$rowArr[$c0]; $tmp.="</td></tr>";
			$rs->MoveNext();
		}
		$tmp.= "</table>";
		return $tmp;
	}
	function viewMultiCheckboxes($sql, $statusValues=""){
		static $rs;
		$rs = $this->conn->Execute($sql);
		if (!$rs) return "";
		$tmp = "<table border=0>";
		$statusArr = explode(",", $statusValues);
		while (!$rs->EOF){
			$rowArr = $rs->fields; 
			$c0 = 0; $c1 = 1; if (count($rowArr)<2) $c1 = 0;
			if (in_array($rowArr[$c1], $statusArr)) {
				$tmp.="<tr><td width=5><b>&radic</b";
				$tmp.="</td><td style=\"color:darkblue\">".$rowArr[$c0]; $tmp.="</td></tr>";
			}
			$rs->MoveNext();
		}
		$tmp.= "</table>";
		return $tmp;
	}
	function  getRemoteData($url){
		$ss = @file_get_contents($url);
		if ($ss=="") return "ERROR";
		return $ss;
	}
	function isDate($d, $frm="dd/mm/yyyy"){
		$d = str_replace("-", "/", $d);
		$dArr = explode("/", $d);
		if (count($dArr)<>3) return false;
		$fArr = explode("/", $frm);
		$rArr = array();
		for($m=0; $m < count($fArr);$m++){
			$rArr[$fArr[$m]]=$dArr[$m];
		}
		return checkdate($rArr["mm"], $rArr["dd"], $rArr["yyyy"]);
	}
	function getFiles($dir, $_ext="*"){
		$path = $this->appPath()."/".$dir;
		$d = dir($path); if (!$d) return false;
		$a = array(); $k=0;
		while ($e=$d->read()){
			if ($e<>"." && $e<>".." && is_file($path."/".$e)) {
				if ($this->checkFileType($e, strtolower($_ext))) {
					$a[$k] = $e; $k++;
				}	
			}	
		}//end while
		$d->close();
		asort($a);
		return $a;
	}//end getFiles
	function getFolders($dir){
		$path = $this->appPath()."/".$dir;
		$d = dir($path); if (!$d) return false;
		$a = array(); 
		while ($e=$d->read()){
			if ($e<>"." && $e<>".." && is_dir($path."/".$e)) array_push($a, $e);
		}//end while
		$d->close();
		sort($a);
		return $a;
	}//end getFiles
	function makeDir($path){
		$cpath = $this->appPath();
		$path = str_replace("\\", "/", $path);
		$pathArr = explode("/", $path);
		foreach($pathArr as $p){
			$cpath.="/".$p;
			if (!is_dir($cpath)) mkdir($cpath);
		}
		return true;
	}//end of function makeDir
	function createDir($path){
		$cpath = "";
		$path = str_replace("\\", "/", $path);
		$pathArr = explode("/", $path);
		foreach($pathArr as $p){
			$cpath.="/".$p;
			if (!is_dir($cpath)) mkdir($cpath);
		}
		return true;
	}//end of function makeDir
	function formatDate($str, $frm="dd/mm/yyyy"){
		$str = str_replace("-", "/", $str);
		$tmpArr = explode("/", $str);
		if (count($tmpArr)<>3) return "";
		$tmpY = 0; $tmpM = 0; $tmpD = 0;
		switch ($frm){
			case "dd/mm/yyyy": $tmpD = $tmpArr[0]; $tmpM = $tmpArr[1]; $tmpY = $tmpArr[2]; break;
			case "mm/dd/yyyy": $tmpD = $tmpArr[1]; $tmpM = $tmpArr[0]; $tmpY = $tmpArr[2]; break;
			case "yyyy/mm/dd": $tmpD = $tmpArr[2]; $tmpM = $tmpArr[1]; $tmpY = $tmpArr[0]; break;
		}
		if (!checkdate($tmpM, $tmpD, $tmpY)) return "";
		return $tmpY."-".$tmpM."-".$tmpD;
	}
	function userDate($str){
		$str = str_replace("-", "/", $str);
		$dArr = explode(" ", $str); $str=$dArr[0];
		$tmpArr = explode("/", $str);
		if (count($tmpArr)<>3) return "";
		$tmpY = 0; $tmpM = 0; $tmpD = 0;
		$drv = DRIVER;
		switch ($drv){
			case "mssql": $tmpD = $tmpArr[2]; $tmpM = $tmpArr[1]; $tmpY = $tmpArr[0]; break;
			case "mysql": $tmpD = $tmpArr[2]; $tmpM = $tmpArr[1]; $tmpY = $tmpArr[0]; break;
		}
		if (!checkdate($tmpM, $tmpD, $tmpY)) return "";
		return $tmpD."/".$tmpM."/".$tmpY;
	}
	function getCode($tab, $l=10){
		$_id = $this->getID($tab);
		while(strlen($_id)<$l) {
			$_id = "0".$_id;
		}
		return $_id;
	}
	function getID($tab){
		$_num = $this->GetFieldValue("select ordernum from seqdata where tablename='$tab'");
		$_num = intval($_num)+1;
		$_fld = array("tablename"=>$tab, "ordernum"=>$_num);
		$this->conn->Replace("seqdata", $_fld, "tablename", true);
		return $_num;
	}
	function getNewsID(){
		return $this->getCode("newslang");
	}
	function getConfig($cat, $item)
	{
		return $this->GetFieldValue("select configvalue from sys_config where configgroup='$cat' and configitem='$item'");
	}
	function setConfig($cat, $item, $val)
	{
		if (empty($cat) || empty($item)) return false;
		$lc_ret = $this->GetFieldValue("select configid from sys_config where upper(configgroup)='".strtoupper($cat)."' and upper(configitem)='".strtoupper($item)."'");
		if ($lc_ret<>""){
			$this->SQLExec("update sys_config set configvalue='$val' where configid=$lc_ret");
		} else {
			$lc_id = $this->GetFieldValue("select max(configid) as num from sys_config") + 1;
			$this->SQLExec("insert into sys_config (configgroup, configitem, configvalue, configid) values ('$cat', '$item', '$val', $lc_id)");
		}
		return true;
	}
	function getConfigx($cat, $item)
	{
		$lang = @$_SESSION["language"]; $sup = @$_SESSION["supplierid"];
		if ($lang=="") $lang=1; if ($sup=="") $sup=-1;
		return $this->GetFieldValue("select configvalue from sys_configx where configgroup='$cat' and configitem='$item' and language=$lang and supplierid=$sup");
	}
	function setConfigx($cat, $item, $val)
	{
		$lang = @$_SESSION["language"]; $sup = @$_SESSION["supplierid"];
		if ($lang=="") $lang=1; if ($sup=="") $sup=-1;
		if (empty($cat) || empty($item)) return false;
		$lc_ret = $this->GetFieldValue("select configid from sys_configx where upper(configgroup)='".strtoupper($cat)."' and upper(configitem)='".strtoupper($item)."' and language=$lang and supplierid=$sup");
		if ($lc_ret<>""){
			$this->SQLExec("update sys_configx set configvalue='$val' where configid=$lc_ret");
		} else {
			$lc_id = $this->GetFieldValue("select max(configid) as num from sys_configx") + 1;
			$this->SQLExec("insert into sys_configx (configgroup, configitem, configvalue, configid, language, supplierid) values ('$cat', '$item', '$val', $lc_id, $lang, $sup)");
		}
		return true;
	}
	function appPath(){
		if (strpos(__FILE__, "\\")>=0 || strpos(__FILE__, "/")>=0) return dirname(dirname(dirname(__FILE__)));
		return str_replace("\\", "/", dirname(dirname(@$_SERVER['PATH_TRANSLATED'])));
	}
	function getElement($ss, $tag){
		$ret="";
		$p1 = strpos($ss, "<".$tag.">");
		$p2 = strpos($ss, "</".$tag.">");
		if ($p2-$p1>0){
			$ret = substr($ss, $p1 + strlen($tag)+2, $p2-$p1-strlen($tag)-2);
		}
		return $ret;	
	}//end of getElement
	function getXmlElement($ss, $tag){
		static $p0, $p1, $p2;
		$ret="";
		$p1 = strpos($ss, "<".$tag);
		$p2 = strpos($ss, "</".$tag.">");
		if ($p2-$p1>0){
			$p0 = strpos($ss, ">", $p1+1);
			$ret = substr($ss, $p0+1, $p2-$p0-1);
		}
		return $ret;	
	}//end of getElement
	function getLabels($fileContents=""){
		static $rArr, $lArr, $vArr, $v;
		if ($fileContents=="") return;
		$rArr = array();
		$lArr = explode("\n", $fileContents);
		foreach($lArr as $v){
			$vArr = explode("=", $v);
			if (count($vArr)==2){
				$rArr[trim($vArr[0])] = stripslashes(trim($vArr[1]));
			}
		}//end for
		return $rArr;
	}//end of getLabels
	function loadLabels($fn, $lang=""){
		$fileContents = @file_get_contents($fn);
		if (intval($lang)>0){
			$tmp = $this->GetFieldValue("select keywords from languageslabel where langid=$lang and tablename='$fn'");
			if ($tmp!="") $fileContents = $tmp;
		}
		return $this->getLabels($fileContents);
	}
	function loadCommonLabels($lang=""){
		if ($lang=="") $lang = @$_SESSION["language"];
		if ($lang=="") $lang=1;
		$labels = $this->GetFieldValue("select keywords from languages where langid=$lang");
		if ($labels=="") $labels = @file_get_contents($this->appPath()."/controls/labels/common.label.inc");
		$retArr = $this->getLabels($labels);
		$tabs = "";
		$labelrs = $this->conn->Execute("select tablename, tabledesc from languageslabel where langid=$lang");
		if ($labelrs){
			while(!$labelrs->EOF){
				$tabs.=$labelrs->fields["tablename"]."=".$labelrs->fields["tabledesc"]."\n"; 
				$labelrs->MoveNext();
			}
		}//end if labelrs
		if ($tabs=="") $tabs = @file_get_contents($this->appPath()."/controls/labels/tables.label.inc");
		# add all tables to common labels
		$tabArr = $this->getLabels($tabs);
		foreach($tabArr as $k=>$v) $retArr[$k]=$v;
		foreach($this->specialLabel as $k=>$v) $retArr[$k]=$v;
		$retArr["add"] = "<img border=0 src='../images/newstar.gif'>&nbsp;".$retArr["add"];
		return $retArr;
	}//end of loadCommonLabels
	function loadTabLabels($tab, $lang=""){
		if ($tab=="") return array();
		if ($lang=="") $lang = @$_SESSION["language"];
		$labels = $this->GetFieldValue("select keywords from languageslabel where tablename='$tab' and langid=$lang");
		if ($labels=="") $labels = @file_get_contents($this->appPath()."/controls/labels/$tab.label.inc");
		return $this->getLabels($labels);
	}
	function getDefaultValue($tag){
		if ($tag=="") return "";
		$ret = "";
		switch($tag){
			case "date": $ret = str_replace("'","",$this->conn->DBDate(strtotime(date("Y-m-d")))); break;
			case "datetime": $ret = str_replace("'","",$this->conn->DBTimeStamp(strtotime(date("Y-m-d H:i:s")))); break;
			case "ipaddress":
				$ret = @$_SERVER['REMOTE_ADDR'];
				break;
		}
		if (strpos("@".$tag, "session.")>0){
			$tagArr = explode(".", $tag);
			$ret = @$_SESSION[$tagArr[1]];
		}
		return $ret;
	}//end of getDefaultValue
	function getFileType($fn){
		$fnArr = explode(".", $fn);
		if (count($fnArr)<2) return "";
		return strtolower($fnArr[count($fnArr)-1]);
	}
	function checkFileType($fn, $type){
		if ($type=="*" || $type=="") return true; 
		$fe = $this->getFileType($fn);
		if ($type=="images") return in_array($fe, array("gif", "jpg", "bmp", "png", "swf"));
		if ($type=="media") return in_array($fe, array("mp3", "mp4", "avi", "dat", "wmv", "ram", "flv"));
		if ($type=="docs") return in_array($fe, array("txt", "doc", "xls", "ppt", "pdf"));
		if ($type=="htm") return in_array($fe, array("htm", "html", "php", "asp", "aspx"));
		return false;
	}
	# get replication of $str by $n times
	function replicate($str, $n){
		$retVal = "";
		for($_i=1;$_i<=$n;$_i++) $retVal.=$str;
		return $retVal;
	}
	# get complement for $sn by adding $c up to $len
	function complement($sn, $len=10, $c="0"){
		$retVal = $sn; while(strlen($retVal)<$len) $retVal = $c.$retVal;
		return $retVal;
	}
	function truncation($ss, $len=100){
		$plen = $len; $ss = strip_tags($ss);
		if ($plen >= strlen($ss)) return $ss;
		$c = substr($ss, $plen, 1);
		while ($c!=" " && $plen < strlen($ss)){
			$plen++; $c = substr($ss, $plen, 1);
		}//end while
		return substr($ss, 0, $plen)."...";
	}
	function getMasterFrame($tab){
		$mobj = @$_SESSION[$tab."_master_object"];
		$mval = @$_SESSION[$tab."_foreignkey_value"];
		$mobjArr = explode(".", $mobj);
		if ($mobj=="" || $mval=="") return "";
		$folder = basename(dirname(@$_SERVER['PHP_SELF']));
		$ret = "<div id=\"__".$tab."Canvas\"></div>";
		$ret.= "<iframe name=\"__".$tab."Frame\" id=\"__".$tab."Frame\" style=\"display:none\"";
		$ret.= " src=\"../$folder/".$mobjArr[0].".master.php?pkey=".$mobjArr[1]."&pval=".$mval."\"";
		$ret.= " onload=\"javascript:updateView('__".$tab."Frame', '__".$tab."Canvas');\"></iframe>";
		return $ret;
	}
	function getMasterSection($tab){
		static $tpl, $sql, $rs, $quote;
		$mobj = @$_SESSION[$tab."_master_object"];
		$mval = @$_SESSION[$tab."_foreignkey_value"];
		$mobjArr = explode(".", $mobj);
		if ($mobj=="" || $mval=="") return "";
		$folder = basename(dirname(@$_SERVER['PHP_SELF']));
		//$ret = "<div id=\"__".$tab."Canvas\"></div>";
		//$ret.= "<iframe name=\"__".$tab."Frame\" id=\"__".$tab."Frame\" style=\"display:none\"";
		//$ret.= " src=\"../$folder/".$mobjArr[0].".master.php?pkey=".$mobjArr[1]."&pval=".$mval."\"";
		//$ret.= " onload=\"javascript:updateView('__".$tab."Frame', '__".$tab."Canvas');\"></iframe>";
		$table = $mobjArr[0]; $mkey = $mobjArr[1];
		$coltype = "@".strtolower($this->getColumnType($table, $mkey));
		$quote = ""; if ($coltype=="@n" || $coltype=="@c" || strpos($coltype, "char")>0 || strpos($coltype, "text")>0) $quote = "'";
		$tpl = new XTemplate("forms/".$table.".master.htm");
		$sql = "select * from $table where $mkey=".$quote.$mval.$quote;
		$rs = $this->conn->Execute($sql); 
		if (!$rs) return "";
		$tpl->assign($table, $rs->fields);
		$tpl->assign("common", $this->loadCommonLabels());
		$tpl->assign("labels", $this->loadTabLabels($table));
		$tpl->parse("main.".$table); $tpl->parse("main");
		return $tpl->text("main");
	}
	function getColumnType($table, $colname){
		static $colArr, $colObj, $colType;
		$colArr = $this->conn->MetaColumns($table);
		if (!$colArr) return "";
		$colObj = $colArr[strtoupper($colname)];
		return $colObj->type;
	}
	function getProtectCode($tab="users"){
		$t = @$_SESSION["protect_".$tab];
		if ($t=="") {
			$t = rand(10000,20000);
			$_SESSION["protect_".$tab] = $t;
		}
		return $t;
	}//end of getProtectCode
	function releaseProtectCode($tab="users"){
		$_SESSION["protect_".$tab] = "";
	}//end of function
	function getConfigArray($ss){
		$ss =str_replace(chr(13).chr(10), "\n", $ss);
		$ssArr = explode("\n", $ss);
		$retArr = array();
		foreach($ssArr as $v){
			if ($v!=""){
				$vArr = explode("=", $v);
				$retArr[$vArr[0]] = @$vArr[1];
			}
		}
		return $retArr;
	}
	function getChannel($pid){
		$_tsql = "select newstopiclang.* from newstopiclang inner join newslang on newstopiclang.topicid=newslang.topicid where newslang.pageid='$pid'";
		if (substr($pid, 0, 4)=="root") {
			$pid = substr($pid, 4);
			$_tsql = "select * from newstopiclang where topicid=$pid";
		}	
		$_trs = $this->conn->Execute($_tsql);
		if (!$_trs) return false;
		$_trow = $_trs->fields; $_trs->Close();
		return $_trow;
	}
	function getRSS($pageid, $cond="", $order="", $ps=10, $pn=1, $link){
		if (@intval($ps)<10) $ps=10; if (@intval($pn)<1) $pn=1; 
		$order = ($order=="1")?"":$order; $order = ($order=="-1")?"desc":$order;
		$trow = $this->getChannel($pageid);
		if (!$trow) return "";
		if ($order=="" && $trow["topictype"]=="NEWS"){
			if (!strpos("@".$pageid, "root")){
				$order = "desc";
			} 
		}//end if order==""
		$ret = "<?"."xml version=\"1.0\" encoding=\"utf-8\" ?>\n";
		$ret.= "<"."rss version=\"2.0\">\n";
		$ret.= "<"."channel>\n";
		if (strpos("@".$pageid, "root")){
			$topicid = substr($pageid, 4);
			$title = $this->GetFieldValue("select topicname from newstopiclang where topicid=$topicid");
		} else {
			$title = $this->GetFieldValue("select title from newslang where pageid='$pageid'");
		}	
		$ret.= "<"."title>".$title;
		$ret.= "<"."/title>\n";
		$link_str = "concat('../portal/index.php?pageid=', pageid, '&topicid=', topicid, '&link=$link')";
		if (DRIVER=="mssql") $link_str = "'../portal/index.php?pageid=' + pageid + '&topicid=' + cast(topicid as varchar(10)) + '&link=$link'";
		$sql = "select title, $link_str as link, note as description from newslang where parent='$pageid' and status=1";
		if ($cond<>"") $sql.= " and $cond";
		$sql.= " order by recdate";
		if ($order<>"") $sql.= " $order";
		$ret.= $this->GetXML($sql, $pn, $ps-1, "item");
		#-add [next page] to XML
		$rc = $this->getElement($ret, "reccount");
		if ($rc-$ps+1==0){
			$ret.="<"."item>\n";
			$next = "xem tiếp..."; if (@$_SESSION["language"]<>1) $next="more...";
			$next="<"."font color='blue'>".$next."<"."/font>";
			$ret.="<"."title>$next</"."title>\n";
			$np = $pn+1;
			$ret.="<"."link>index.php?pageid=$pageid&pagenum=$np&pagesize=$ps</"."link>\n";
			$ret.="<"."/item>\n";
		}//END IF
		$ret.= "</"."channel>\n";
		$ret.= "</"."rss>\n";
		return $ret;
	}
	function checkLocal($h){
		if (!strpos("@".$h, ".")) return true;
		$hArr = explode(".", $h);
		$_ret = true;
		foreach($hArr as $h){
			if (!is_numeric($h)) $_ret = false;
		}
		return $_ret;
	}
	function getEnc($ss=""){
		if ($ss=="") return $ss;
		return md5(base64_encode($ss));
	}
	function updateEnc($ss){
		$this->setConfig("system", "key", $ss);
	}
	function getParentRSS($pageid){
		$parent = $this->GetFieldValue("select parent from newslang where pageid='$pageid'");
		if ($parent=="") return "";
		$cond = "pageid < $pageid";
		$order = "desc";
		if (strpos("@".$parent, "root")){
			$cond = "";
			$order = "";
		}
		return $this->getRSS($parent, $cond, $order);
	}
	function saveToFile($ss, $fn){
		$fh = fopen($fn, "w");
		if (!$fh) return false;
		fwrite($fh, $ss); fclose($fh); return true;
	}
	function quoteTable($table, $orow){
		if (!$orow) return false;
		if ($table=="") return $orow;
		$row = $orow;
		$colArr = $this->conn->MetaColumns($table);
		foreach($colArr as $col=>$colObj){
			$colname = $colObj->name;
			$coltype = "@".$colObj->type;
			if (in_array($colname, array_keys($orow))){
				if (strpos($coltype, "char")>0 || strpos($coltype, "text")>0){
					if (!strpos("@".$row[$colname], "'")) $row[$colname] = "'".$row[$colname]."'";
				}
			}
		}//end for
		return $row;
	}//end of function
	function color($ss, $color){
		if ($ss=="") return "";
		if ($color=="") return $ss;
		return "<font color=\"$color\">".$ss."</font>";
	}
	function hyperlink($ss, $url, $target=""){
		static $ret;
		if ($ss=="") return "";
		if ($url=="") return $ss;
		$ret = "<a href=\"$url\"";
		if ($target!="") $ret.= " target=\"$target\"";
		$ret.= ">".$ss."</a>";
		return $ret;
	}
}//end of class
?>
<?php 
class openLDAP{
	# variables to be changed
	var $controller="10.0.0.7"; // to be changed
	var $dn="ou=Users,dc=bitcojsc,dc=net"; // to be changed
	var $port=389; // to be changed
	var $load = false; // to be changed
	# other variables
	var $conn=false; // connection to LDAP
	var $db=false; // connection to database
	var $bind=false; // bind handler
	var $err="";
	var $user=""; // user
	var $password; // password
	var $uid = "cn";
	var $fields = "cn,displayname,mailaddress,dn";
	var $displayname = "sn";#"displayname";
	var $mailaddress = "mailaddress";
	function openLDAP($dbconn){
		if ($this->load){
			if (!extension_loaded('php_ldap')) {
				if (!@dl('php_ldap.dll')) $this->err = "Can not connect to LDAP server !";
			}
		}//end of load
		$this->conn = @ldap_connect($this->controller, $this->port);
		if ($this->conn){
			ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
		} 
		if ($dbconn) $this->db = $dbconn;
		# It may get config value on controller, dn, port, user, password.... here 
	}//end of constructor
	function __destruct(){
		if ($this->conn) ldap_close($this->conn);
	}
	function authenticate($username,$password){
		$this->user = $username;
		$this->bind = @ldap_bind($this->conn, $this->uid."=".$username.",".$this->dn, $password);
		if ($this->bind) {
			return true;
		} else return false;
	} // end of authenticate
	function rebind(){
		$this->bind = @ldap_bind($this->conn, $this->uid."=".$this->user.",".$this->dn, $this->password);
		if ($this->bind){
			return true;
		} else return false;
	}
	function all_users($search = "*"){
		$filter="(&(objectClass=person)(cn=$search))";
		if ($this->user<>"") $this->rebind();
		$arr = explode(",",$this->fields);
		//$sr = @ldap_search($this->conn, $this->dn, $filter, $arr);	
		$sr = @ldap_search($this->conn, $this->dn, $filter);	
		$ret = array();
		if (!$sr) return $ret;
		$entries = ldap_get_entries($this->conn, $sr);
		for ($i=0;$i<$entries["count"];$i++){
			$e = $entries[$i];
			$k = $e["dn"]; 
			$a = $this->getDNArr($k); $k = $a["cn"];
			$v = $e[$this->displayname][0];
			$ret[$k] = $v;
		}
		asort($ret);
		return $ret;
	}// end of all_users
	function getDNArr($dn){
		$ret = array();
		$tmp = explode(",", $dn);
		foreach($tmp as $v){
			$vArr = explode("=", $v);
			$ret[$vArr[0]] = $vArr[1];
		}
		return $ret;
	}//end of getDN
}//end of class
?>
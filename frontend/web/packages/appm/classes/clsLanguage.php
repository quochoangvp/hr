<?php
class Language extends Common{
	function Language($c){
		if ($c) $this->conn = $c;
	}
	function updateDefaultCommon($lang){
		$fileContent = file_get_contents("labels/common.label.inc");
		if ($fileContent=="") return;
		$k = $this->GetFieldValue("select keywords from languages where langid=$lang");
		if ($k==""){
			$fld["langid"] = $lang; 
			$fld["keywords"] = $fileContent;
			$this->conn->Replace("languages", $fld, "langid", true);
		}
		return true;
	}//end of updateDefaultCommon
	function updateDefaultTables($lang){
		$fileContent = file_get_contents("labels/tables.label.inc");
		if ($fileContent=="") return false;
		$tArr = $this->getLabels($fileContent);
		foreach($lArr as $tabname=>$tabdesc){
			$lrs = $this->conn->Execute("select * from languageslabel where langid=$lang and tablename='$tabname'");
			$fld["langid"] = $lang;
			if ($lrs->RecordCount()>0){
				if ($lrs->fields["keywords"]==""){
					$fld["keywords"] = file_get_contents("labels/$tabname.label.inc");
				}
				if ($lrs->fields["tabledesc"]==""){
					$fld["tabledesc"] = $tabdesc;
				}
			} else {
				$fld["tablename"] = $tabname;
				$fld["tabledesc"] = $tabdesc;
				$fld["id"] = $this->getID("languageslabel");
				$fld["keywords"] = file_get_contents("labels/$tabname.label.inc");
			}
			$lrs->Close();
			$this->conn->Replace("languageslabel", $fld, "id", true);
		}//end of loop for tables
		return true;
	}//end of function updateDefaultTables
	function updateAll(){
		$langRS = $this->conn->Execute("select * from languages");
		while(!$langRS->EOF){
			$lg = $langRS->fields["langid"];
			$this->updateDefaultCommon($lg);
			$this->updateDefaultTables($lg);
			$langRS->MoveNext();
		}
		$langRS->Close();
		return true;
	}//end of function updateAll
}//end of class
?>
<?php
class Category{
	var $conn;
	var $tmp="";
	var $catcols = 4;
	var $path = "";
	var $catImgWidth = 100;
	var $skinLoc = "templates";
	function Category(){
		$this->path = str_replace("\\", "/", dirname(dirname(__FILE__)));
	}
	function getPath($ma="",$link=""){
		if ($ma=="" || $ma=="-1") return "";
		if ($link=="") $link = @$_SERVER['PHP_SELF'];
		$this->tmp = "";
		$tmp_ma = $ma;
		$this->rs = $this->conn->Execute("select categoryid, categoryname, highercategoryid from categories where categoryid=".$tmp_ma."");
		while ($this->rs->RecordCount()>0 && intval($tmp_ma)>0){
			$this->tmp = "<a href='".$link."?catid=".$this->rs->fields["categoryid"]."'>".$this->rs->fields["categoryname"]."</a> / ".$this->tmp;
			$tmp_ma = $this->rs->fields["highercategoryid"];
			$this->rs->Close();
			$this->rs = $this->conn->Execute("select categoryid, categoryname, highercategoryid from categories where categoryid=".$tmp_ma."");
		}
		return $this->tmp;
	}//end getPath()
	function showCategories($cid=""){
		$t = 0;
		if (intval($cid)==0){
			$t=1;
			$sql = "select * from categories where (highercategoryid is null) or (highercategoryid=0)";
		} else{
			$t = 2;
			$sql = "select * from categories where highercategoryid=$cid";
		}
		$sql.=" order by categoryid";
		$rs = $this->conn->Execute($sql);
		if ($t==2){//show sub-categories
			$w = intval(100/$this->catcols);
			$z_tpl = $this->showPath($cid);
			$z_tpl.= "<table border='0' width='100%'>";
			$k=1;	
			while (!$rs->EOF){
				$cat = $this->showNode($rs->fields);
				if (($k%$this->catcols)==1) $z_tpl.="<tr>";
				$z_tpl.="<td width='$w%' valign='top' onmouseover='catmHover(this)' onmouseout='catmOut(this)'>".$cat."</td>";
				if (($k%$this->catcols)==0) $z_tpl.="</tr>";
				$rs->MoveNext();
				$k++;
			}//end while rs
			if (($k%($this->catcols))!=0) $z_tpl.="</tr>";
			$z_tpl.="</table>";
			$rs->Close();
			return $z_tpl;
		}//end if t==2
		if ($t==1){//show top-level categories
			$z_tpl = new XTemplate($this->path."/".$this->skinLoc."/topcat.list.htm");
			while (!$rs->EOF){
				$z_tpl->assign("cat", $this->showCategory($rs->fields));
				$z_tpl->parse("main.row");
				$rs->MoveNext();
			}//end while
			$rs->Close();
			$z_tpl->parse("main");
			return $z_tpl->text("main");
		}//end if t==1
	}//end of showCat
	function showCategory($row){
		if (!$row) return "";
		$x_tpl = new XTemplate($this->path."/".$this->skinLoc."/topcat.htm");
		$x_tpl->assign("maincat", $row);
		$id = $row["categoryid"];
		$subrs = $this->conn->Execute("select * from categories where highercategoryid=$id order by categoryid");
		while (!$subrs->EOF){
			$x_tpl->assign("category", $subrs->fields);
			$x_tpl->parse("main.subcat");
			$subrs->MoveNext();
		}
		$subrs->Close();
		$x_tpl->parse("main");
		return $x_tpl->text("main");
	}//end of showCategory
	function showNode($row){
		$y_tpl = new XTemplate($this->path."/".$this->skinLoc."/cat.htm");
		$y_tpl->assign("cat", $row);
		if ($row["image"]<>""){
			$img = "<img border=0 width=".$this->catImgWidth." src=\"../".$row["image"]."\">";
			$y_tpl->assign("catimage", $img);
		}
		$y_tpl->parse("main");
		return $y_tpl->text("main");
	}//end of function
	function showPath($ma=""){
		if ($ma=="" || $ma=="-1") return "";
		$this->tmp = "";
		$tmp_ma = $ma;
		$this->rs = $this->conn->Execute("select categoryid, categoryname, highercategoryid from categories where categoryid=".$tmp_ma."");
		while ($this->rs->RecordCount()>0 && intval($tmp_ma)>0){
			$this->tmp = "<a href=\"javascript:showCat('".$this->rs->fields["categoryid"]."')\">".$this->rs->fields["categoryname"]."</a> / ".$this->tmp;
			$tmp_ma = $this->rs->fields["highercategoryid"];
			$this->rs->Close();
			$this->rs = $this->conn->Execute("select categoryid, categoryname, highercategoryid from categories where categoryid='".$tmp_ma."'");
		}
		$this->tmp = "<table><tr><td height=22>".$this->tmp."</td></tr></table>";
		return $this->tmp;
	}//end showPath()
}//end of class
?>
<?php session_start();?>
<?php include("connect.php");?>
<?php
$table = "am_dbs"; //change to your table 
$form = "forms/am_dbs.edit.htm"; //change to your form name
$flds = "dbhost,dbname,dbuser,dbpassword,dbdesc,dbtype,directory,dbcharset,author"; //fields being requested separated by comma
$pfld = "dbname"; //primary key field
$returnPage = "index.php";//return page after updating
$dateflds = ""; //fields of date to check
$numflds = ""; //fields of number to check
$menuArr = array();//menus in form (combo boxes)

$a = @$_REQUEST["a"];
if ($a == "U"){ //form submitted
	$flds = str_replace(";", ",", $flds);
	$fldArr = explode(",", $flds);
	$fieldList = array();
	for ($i=0;$i<count($fldArr); $i++){//request fields
		$fieldList[$fldArr[$i]] = @$_REQUEST[$fldArr[$i]];
	}
	$fieldList[$pfld] = strtoupper($fieldList[$pfld]);
	if ($numflds<>""){//check numeric fields
		$fldArr = explode(",", $numflds);
		for ($i=0;$i<count($fldArr); $i++){			
			if (!is_numeric($fieldList[$fldArr[$i]])) $fieldList[$fldArr[$i]] = 0;
		}
	}
	# author
	if ($fieldList["author"]=="") $fieldList["author"] = @$_SESSION["bcshop_status_User"];
	# date fields
	if ($dateflds<>""){//check date fields
		$fldArr = explode(",", $dateflds);
		for ($i=0;$i<count($fldArr); $i++){
			$fieldList[$fldArr[$i]] = str_replace("'","",$oConn->DBDate(formatDate($fieldList[$fldArr[$i]])));
		}
	}
	//Now generate SQL command and then execute
	if ($oConn->Replace($table, $fieldList, $pfld, true)){
		header("Location: $returnPage");
		exit;
	} else {
		die("Can not insert data !");
	}
}//end if form submitted
?>
<?php include("header.php");?>
<?php
//show form to enter data
$tpl = new XTemplate($form);
foreach($menuArr as $k=>$v){//generate menus
	$rs = $oConn->Execute($v);
	if ($rs) {
		$tpl->assign($k, $rs->GetMenu($k, ""));
		$rs->Close();
	}	
}
$row = array("author"=>@$_SESSION["User"]);
$tpl->assign($table, $row);
$tpl->parse("main");
$tpl->out("main");
?>
<?php include("footer.php");?>
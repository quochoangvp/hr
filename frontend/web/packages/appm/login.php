<?php session_start();?>
<?php include("connect.php");?>
<?php include("classes/clsLDAP.php");?>
<?php
$a = @$_REQUEST["a"];
$valid = true;
$msg = "";
ob_start();
if ($a=="L"){
	$u = strtoupper(@$_REQUEST["user"]);
	$p = @$_REQUEST["password"];
	if ($u=="ADMIN" || $u=="ROOT"){
		# check in database
		if (md5($p)!="c7c2f0ead19bf73b402f4709212bfd27") $valid = false;
	} else {
		# check user & password in LDAP
		$ldap = new openLDAP($oConn);
		if ($ldap->authenticate($u, $p)){
			$valid = true;
		} else $valid = false;
		# end check
	}
	if ($valid){
		$_SESSION[$am_vars["session_user"]] = $u;
		if ($u=="ADMIN" || $u=="ROOT"){
			$_SESSION[$am_vars["session_admin"]] = $am_vars["session_admin_value"];
		}
		ob_end_clean();
		if (@$_SESSION["current_page"]!=""){
			header("Location: ".$_SESSION["current_page"]); break;
		} else {
			header("Location: index.php"); break;
		}
	} else {
		$msg = "Invalid user / password !";
	}
}//end if $a=="L"
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
&nbsp;
<p align="center" style="color:red">
<?php if ($msg!="") echo $msg;?>
&nbsp;</p>
<h3>Login</h3>
<form action="login.php?a=L" method="post" name="frmLogin" id="frmLogin">
  <table width="100%"  border="0" cellspacing="2" cellpadding="2">
    <tr>
      <td width="29%" align="right">User:</td>
      <td width="71%"><input name="user" type="text" id="user" value="<?php echo @$_SESSION[@$am_vars["session_user"]];?>" size="40"></td>
    </tr>
    <tr>
      <td align="right">Password:</td>
      <td><input name="password" type="password" id="password" size="40"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="Submit" value="Login"></td>
    </tr>
  </table>
</form>
<p>&nbsp;</p>
</body>
</html>

<?php session_start();?>
<?php include("connect.php");?>
<?php
$key = @$_REQUEST["key"]; //key parameter
if ($key=="") $key = @$_SESSION["am_dbname"];
if ($key=="") header("Location: index.php");
?>
<?php
$table = "am_dbs"; //change to your table 
$form = "forms/am_dbs.edit.htm"; //change to your form name
$flds = "dbhost,dbname,dbuser,dbpassword,dbdesc,dbtype,directory,dbcharset,author"; //fields being requested separated by comma
$pfld = "dbname"; //primary key field
$dateflds = ""; //fields of date to check
$numflds = ""; //fields of number to check
$array = "am_dbs";//change to array name in form
$menuArr = array();//menus in form (combo boxes)
$returnPage = "index.php";

$a = @$_REQUEST["a"];
if ($a == "U"){ //form submitted
	$flds = str_replace(";", ",", $flds);
	$fldArr = explode(",", $flds);
	$fieldList = array();
	for ($i=0;$i<count($fldArr); $i++){//request fields
		$fieldList[$fldArr[$i]] = @$_REQUEST[$fldArr[$i]];
	}
	
	if ($numflds<>""){//check numeric fields
		$fldArr = explode(",", $numflds);
		for ($i=0;$i<count($fldArr); $i++){			if (!is_numeric($fieldList[$fldArr[$i]])) $fieldList[$fldArr[$i]] = 0;
}
	}
	
	if ($dateflds<>""){//check date fields
		$fldArr = explode(",", $dateflds);
		for ($i=0;$i<count($fldArr); $i++){
			$fieldList[$fldArr[$i]] = str_replace("'", "", $oConn->DBDate($oCls->formatDate($fieldList[$fldArr[$i]])));
		}
	}
	//Now generate SQL command and then execute
	if ($oConn->Replace($table, $fieldList, $pfld, true)){
		header("Location: $returnPage?dbname=".$fieldList[$pfld]);
		exit;
	} else {
		die("Can not update data !");
	}
}//end if form submitted
$ga = @$_REQUEST["ga"];
if ($ga=="new"){
	$gn = @$_REQUEST["groupname"];
	if ($gn!=""){
		$gflds["dbname"] = $key;
		$gflds["groupid"] = $oConn->GenID();
		$gflds["groupname"] = $gn;
		$oConn->Replace("am_groups", $gflds, "groupid", true);
	}
}//end if ga=new
if ($ga=="delete"){
	$gArr = @$_REQUEST["group"];
	if (is_array($gArr)){
		foreach($gArr as $gi){
			$oConn->Execute("delete from am_groups where groupid='$gi'");
		}
	}//end if
}//end if ga=delete
?> 
<?php include("header.php");?>
<?php
//show form to enter data
$mainrs = $oConn->Execute("select * from ".$table." where ".$pfld."='".$key."'");
if ($mainrs->RecordCount()<1) die("<center>No database selected !. <a href='javascript:history.go(-1);'>Go back</a></center>");
$tpl = new XTemplate($form);
$row = $mainrs->fields;
if ($dateflds<>""){//check date fields
	$fldArr = explode(",", $dateflds);
	for ($i=0;$i<count($fldArr); $i++){
		$row[$fldArr[$i]] = $oCls->userDate($row[$fldArr[$i]]);
	}
}//end if dateflds	
$tpl->assign($array, $row);
foreach($menuArr as $k=>$v){//generate menus
	$rs = $oConn->Execute($v);
	if ($rs){
		$tpl->assign($k, $rs->GetMenu2($k, $mainrs->fields[$k]));
		$rs->Close();
	}
}
# groups / subpage
$gt = new XTemplate("forms/am_groups.list.htm");
$grs = $oConn->Execute("select * from am_groups where dbname='$key'");
while(!$grs->EOF){
	$gt->assign("am_groups", $grs->fields);
	$gt->parse("main.group");
	$grs->MoveNext();
}
$grs->Close();
$gt->parse("main");
# parse main page
$tpl->assign("groups", $gt->text("main"));
$tpl->parse("main");
$tpl->out("main");
$mainrs->Close();
?>
<?php include("footer.php");?>
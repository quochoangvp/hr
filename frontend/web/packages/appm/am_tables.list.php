<?php session_start();?>
<?php include("connect.php")?>
<?php
$db = @$_SESSION["am_dbname"];
$tnum = $oCls->GetFieldValue("select count(*) from am_tables where dbname='$db'");
if (intval($tnum)<1) header("Location: am_dbs.read.php?bdname=".$db);
$st = @$_REQUEST["status"];
if ($st!=""){
	$_SESSION["am_tables_status"] = $st;
} 
# group
$gid = @$_REQUEST["groupid"];
if ($gid!="") $_SESSION["am_tables_groupid"] = $gid;
# action
$a = @$_REQUEST["a"];
if ($a=="M"){
	$o = @$_REQUEST["o"];
	$f = @$_REQUEST["tableid"];
	$tmprs = $oConn->Execute("select ordernum, tableid from am_tables where tableid=$f");
	if (!$tmprs) die("Table not found !");
	if ($tmprs->RecordCount()<1) die("Table not found !");
	$order = $tmprs->fields["ordernum"]; 
	$count = $oCls->GetFieldValue("select count(*) from am_tables where dbname='".$db."'");
	if ($o=="up"){
		if ($order>1){
			$order--;
			$pf = $oCls->GetFieldValue("select tableid from am_tables where ordernum=$order and dbname='$db'");
			$sql1 = "update am_tables set ordernum=ordernum-1 where tableid=$f";
			$sql2 = "update am_tables set ordernum=ordernum+1 where tableid=$pf";
			$oConn->Execute($sql1);
			$oConn->Execute($sql2);
		}
	}// end move up
	if ($o=="dn"){
		if ($order<$count){
			$order++;
			$pf = $oCls->GetFieldValue("select tableid from am_tables where ordernum=$order and dbname='$db'");
			$sql1 = "update am_tables set ordernum=ordernum+1 where tableid=$f";
			$sql2 = "update am_tables set ordernum=ordernum-1 where tableid=$pf";
			$oConn->Execute($sql1);
			$oConn->Execute($sql2);
		}
	}// end move down
}//end move table
# reorder tables
if ($a=="reorder"){
	$tmpRS = $oConn->Execute("select tableid from am_tables where dbname='$db' order by tablename");
	$num=1;
	while(!$tmpRS->EOF){
		$tid = $tmpRS->fields["tableid"];
		$oConn->Execute("update am_tables set ordernum=$num where tableid=$tid");
		$tmpRS->MoveNext();
		$num++;
	}
	$tmpRS->Close();
}//end of reorder
# move to group
if ($a=="movetogroup"){
	$tArr = @$_REQUEST["tables"];
	$gid = @$_REQUEST["groupid"];
	if (is_array($tArr) && $gid!=""){
		$oConn->Execute("update am_tables set groupid='$gid' where tableid in (".implode(",", $tArr).")");
	}
}//end of putting
if ($a=="delete"){
	$tArr = @$_REQUEST["tables"];
	if (is_array($tArr)){
		$oConn->Execute("delete from am_fields where tableid in (".implode(",", $tArr).")");
		$oConn->Execute("delete from am_tables where tableid in (".implode(",", $tArr).")");
		header("Location: am_tables.list.php");
	}
}//end delete
?>
<?php
function getTABS($_dbn, $frm){
	global $oConn; 
	$t = new XTemplate($frm);
	$sql = "select * from am_tables where upper(dbname)='".strtoupper($_dbn)."' and tablename not like 'seq_%'";
	$lockicon = "<img src='images/open_dna.gif'>";
	switch(@$_SESSION["am_tables_status"]){
		case 1: $sql.= " and haslocked=0"; 
				$t->assign("all", "");
				$t->assign("locked", "");
				$t->assign("unlocked", $lockicon);
				break;
		case -1: $sql.= " and haslocked=1"; 
				$t->assign("all", "");
				$t->assign("locked", $lockicon);
				$t->assign("unlocked", "");
				break;
		default:
				$t->assign("all", $lockicon);
				$t->assign("locked", "");
				$t->assign("unlocked", "");
	}
	if (@$_SESSION["am_tables_groupid"]!="" && @$_SESSION["am_tables_groupid"]!="-1"){
		$sql.= " and groupid='".@$_SESSION["am_tables_groupid"]."'";
	}
	$sql.= " order by ordernum";
	$rs = $oConn->Execute($sql);
	$kk=1;
	$uparr = "<img border=0 src='images/uparr.jpg' alt='Move Up'>";
	$dnarr = "<img border=0 src='images/downarr.jpg' alt='Move Down'>";
	$max = $rs->RecordCount(); 
	while (!$rs->EOF){
		$row = $rs->fields;
		if ($row["ordernum"]<1){
			$sql = "update am_tables set ordernum=$kk where tableid=".$row["tableid"];
			$oConn->Execute($sql);
			$row["ordernum"] = $kk;
		}
		$kk++;
		$order = $row["ordernum"];
		$fldval = $row["tableid"];
		# Now show icon to move field up and down
		$move = "";
		if ($order>1){
			$move.="<a href=\"am_tables.list.php?a=M&o=up&tableid=$fldval\">".$uparr."</a>";
		} else $move.=$uparr;
		if ($order<$max){
			$move.="<a href=\"am_tables.list.php?a=M&o=dn&tableid=$fldval\">".$dnarr."</a>";
		} else $move.=$dnarr;
		if ($row["haslocked"]==1) {
			$t->assign("tabstatus", " disabled");
		} else $t->assign("tabstatus", "");	
		$t->assign("move", $move);
		$t->assign("am_tables", $rs->fields);
		$img = "<img border=0 src=\"images/plus.gif\">";		
		$t->assign("list", ($row["haslist"]==1)?$img:"|");
		$t->assign("view", ($row["hasview"]==1)?$img:"|");
		$t->assign("edit", ($row["hasedit"]==1)?$img:"|");
		$t->assign("del", ($row["hasdelete"]==1)?$img:"|");
		$tdclass = "tdlight"; if ($kk%2==0) $tdclass = "tdgrey";
		$t->assign("tdclass", $tdclass);
		$t->parse("main.tables");
		$rs->MoveNext();
	}
	$t->assign("m_dbname", $_dbn);
	# group combobox
	$grs = $oConn->Execute("select groupname, groupid from am_groups where dbname='$_dbn' order by groupid");
	$gtext = $grs->GetMenu2("groupid", @$_SESSION["am_tables_groupid"]);
	$gtext = str_replace("<option></option>", "<option value='-1' style='color: lightgrey'>Put in group...</option>", $gtext);
	$t->assign("groups", $gtext);
	# parse main
	$t->parse("main");
	$rs->Close();
	return $t->text("main");
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<?php 
$dbn = @$_REQUEST["dbname"];
if ($dbn=="") {
	$dbn = @$_SESSION["am_dbname"];
} else {
	$_SESSION["am_dbname"] = $dbn;
}
if ($dbn=="") die("No database selected !");
$frm = "forms/tab.list.htm";
echo getTABS($dbn, $frm);
?>
</body>
</html>

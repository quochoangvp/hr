<!-- BEGIN: main --><?php 
session_start();
include("connect.php");
# -- definition 
$table = "{table}"; //change to your table 
$form = "{form}"; //change to your form name
$flds = "{fields}"; //fields being requested separated by comma
$pfld = "{pkey}"; //primary key field
$dateflds = "{dateflds}"; //fields of date to check
$numflds = "{numflds}"; //fields of number to check
$editorflds = "{editorflds}";
$ruleflds = array({ruleflds}); // fld=>NOTNULL
$fileflds = array({fileflds}); // fld=>array(fldtype, filetype, filelocation)
$menuArr = array({menu});//menus in form (combo boxes)
$staticMenu = array({staticmenu});
$checkboxes = "{checkboxes}";
$returnPage = "{returnpage}";
$quote = "{quote}";
$multiselectArr = array({multiselect});
$msg = "";

$a = @$_REQUEST["a"];
if ($a == "U"){ //form submitted
	$flds = str_replace(";", ",", $flds);
	$fldArr = explode(",", $flds);
	$fieldList = array();
	# request form fields
	foreach ($fldArr as $fld){//request fields
		if (!in_array($fld, array_keys($fileflds))){
			$req = @$_REQUEST[$fld]; if (is_array($req)) $req = implode(",", $req);
			$fieldList[$fld] = $req;
		}	
	}//end for
	$fieldList[$pfld] = @$_REQUEST["key"];
	# file fields
	foreach($fileflds as $fld=>$arr){
		$file = @$_FILES[$fld];
		$remfile = @$_REQUEST["remove_".$fld];
		if ($remfile!="1" && $file["tmp_name"]!="" && $file["tmp_name"]!="none" && $oCls->checkFileType(@$file["name"], $arr["filetype"])){
			$loc = $arr["filelocation"]."/".$fieldList[$pfld]."_".$file["name"];
			$oCls->makeDir($arr["filelocation"]);
			move_uploaded_file($file["tmp_name"], $oCls->appPath()."/".$loc);
			$fieldList[$fld] = $loc;
			@unlink($file["tmp_name"]);
		} else { 
			if ($remfile=="1") $fieldList[$fld] = NULL;
			if ($file["tmp_name"]!="" && $file["tmp_name"]!="none" && !$oCls->checkFileType(@$file["name"], $arr["filetype"])){
				die("Invalid file type !");				
			}
		}//end if has file uploaded
	}//end of foreach fileflds
	if ($numflds<>""){//check numeric fields
		$fldArr = explode(",", $numflds);
		foreach($fldArr as $fld) if (!is_numeric($fieldList[$fld])) $fieldList[$fld] = 0;
	}//
	
	if ($dateflds<>""){//check date fields
		$fldArr = explode(",", $dateflds);
		foreach($fldArr as $fld) $fieldList[$fld] = $oCls->formatDate($fieldList[$fld]);
	}//
	# Now update table
	# $fldList = $oCls->quoteTable($table, $fieldList);
	# $oConn->debug=true;
	if ($oCls->Update($table, $fieldList, $pfld, true)){
		header("Location: $returnPage");
		exit;
	} else {
		$msg = "Can not update data: ".$oCls->ErrorMsg();
	}
}//end if form submitted
?>
<?php
$key = @$_REQUEST["key"]; //key parameter
# show form to enter data
$mainrs = $oConn->Execute("select * from ".$table." where ".$pfld."=".$quote.$key.$quote);
$tpl = new XTemplate($form);
$row = $mainrs->fields;
foreach($row as $fld=>$val) {
	$row[$fld] = stripslashes($val);
}
if ($dateflds<>""){//check date fields
	$fldArr = explode(",", $dateflds);
	for ($i=0;$i<count($fldArr); $i++){
		$row[$fldArr[$i]] = $oCls->userDate($row[$fldArr[$i]]);
	}
}//end if dateflds	
$tpl->assign($table, $row);
# for menus
foreach($menuArr as $k=>$v){//generate menus
	$rs = $oConn->Execute($v);
	if ($rs){
		$tpl->assign($k, $oCls->genMenuRs($k, $rs, $mainrs->fields[$k])); $rs->Close();
	}
}
# for static menus
foreach($staticMenu as $k=>$vArr) $tpl->assign($k, $oCls->genMenuKey($k, $vArr, $mainrs->fields[$k], true));

# for checkboxes
$checkArr = explode(",", $checkboxes);
foreach($checkArr as $v) $tpl->assign($v, $oCls->genCheckbox($v, "1", $mainrs->fields[$v]));

# multi selections
foreach($multiselectArr as $k=>$sql){
	$rs = $oConn->Execute($sql);
	if ($rs) $tpl->assign($k, $oCls->genMultiCheckboxes($k, $rs, $mainrs->fields[$k]));
} //end for multi selections

# for files
foreach($fileflds as $k=>$arr){
	$faction = "";
	if ($mainrs->fields[$k]!="" && $mainrs->fields[$k]!=NULL){
		$faction = $oCls->genCheckbox("remove_$k", "1").@$commonLabels["deletefile"]." | ";
		$faction.= "<a href=\"";
		$faction.="../../".$mainrs->fields[$k]."\">";
		$faction.= @$commonLabels["view"]."</a>";
		$tpl->assign("file_".$k, $faction);
	}
}//end for files

# action
$tpl->assign("form_action", @$_SERVER['PHP_SELF']."?a=U");
# dateflds and numflds for form checking
$tpl->assign("dateflds", $dateflds);
$tpl->assign("numflds", $numflds);
# not null fields
$nnflds = "";
foreach($ruleflds as $fld=>$v){
	if ($v=="NOTNULL") $nnflds.=$fld.",";
}
if ($nnflds!="") $nnflds = substr($nnflds, 0, strlen($nnflds)-1);
$tpl->assign("nnflds", $nnflds);
# labels
$tpl->assign("common", $commonLabels);
$tpl->assign("labels", $oCls->loadTabLabels($table));

{HEADER}

if ($msg!="") {
	echo $oCls->genAlert($msg);
}
# parse and out
$tpl->parse("main");
$tpl->out("main");
$mainrs->Close();
if ($editorflds!=""){
	require_once($oCls->appPath()."/packages/ckeditor/ckeditor.php");
	$CKEditor = new CKEditor();
	$CKEditor->basePath = $oCls->basePath()."/packages/ckeditor/";
	$editorArr = explode(",", $editorflds);
	foreach($editorArr as $editorid){
		$CKEditor->replace($editorid);
	}//
}//

{FOOTER}
?>
<!-- END: main -->
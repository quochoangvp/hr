var mystatus = "Loading complete !";
var message = "Right click disabled!";
function getHTTP(){
var xmlhttp=false;
if (window.XMLHttpRequest)
{
  try{
  xmlhttp = new XMLHttpRequest();
  } catch(e) {
	xmlhttp = false;  
  }
} else if (window.ActiveXObject)
  {
	  try{
 		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	  } catch(e){
		  xmlhttp = false;
	  }
  }
  return xmlhttp;
}//end of function getHTTP
function getData(url, objid, xmlobj){
	if (!xmlobj) {
		xmlhttp = getHTTP();
	} else xmlhttp = xmlobj;
	xmlhttp.open("GET", url, true);
	ret = "";
	xmlhttp.onreadystatechange= function() {
		if (xmlhttp.readyState==4) {
			if (xmlhttp.status==200){
				obj = document.getElementById(objid);
				if (obj){
					obj.innerHTML = xmlhttp.responseText;
				}
			} else obj.innerHTML = "Can not render !";//end if status
		}//end if
	}//end of function
	xmlhttp.send(null);
}//end of getData
function showData(frame, canvas){
	fobj = document.getElementById(frame);
	if (!fobj) return;
	if (fobj.contentWindow.document.body.innerHTML=="") return;
	cobj = document.getElementById(canvas);
	if (!cobj) return;
	cobj.innerHTML = fobj.contentWindow.document.body.innerHTML;
	window.scrollTo(0,0);
}
function updateCanvas(frameid, canvasid){
	if (!frameid) return;
	if (!canvasid) return;
	cobj = document.getElementById(canvasid);
	if (!cobj) return;
	fobj = document.getElementById(frameid);
	if (!fobj) return;
	cobj.innerHTML = fobj.contentWindow.document.body.innerHTML;
	// set target for all links and forms
	aobjs = cobj.getElementsByTagName("a");
	if (!aobjs) aobjs = cobj.getElementsByTagName("A");
	for(i=0;i<aobjs.length;i++){
		e = aobjs[i];
		e.target = frameid;
	}
	// set target for all forms
	aobjs = cobj.getElementsByTagName("form");
	if (!aobjs) aobjs = cobj.getElementsByTagName("FORM");
	for(i=0;i<aobjs.length;i++){
		e = aobjs[i];
		e.target = frameid;
	}
}//end of updateCanvas
function updateView(frameid, canvasid){
	if (!frameid) return;
	if (!canvasid) return;
	cobj = document.getElementById(canvasid);
	if (!cobj) return;
	fobj = document.getElementById(frameid);
	if (!fobj) return;
	cobj.innerHTML = fobj.contentWindow.document.body.innerHTML;
}//end of updateView
function updateMenu(menus){
	if (menus=="") return;
	mArr = menus.split(",");
	for(i=0;i<mArr.length;i++){
		objid = mArr[i];
		obj1 = document.getElementById("_" + objid);
		obj2 = document.getElementById(objid);
		if (obj1 && obj2){
			obj2.value = obj1.value;
		}
	}//end for
}//end function updateMenu
function updateCheck(checks){
	if (checks=="") return;
	mArr = checks.split(",");
	for(i=0;i<mArr.length;i++){
		objid = mArr[i];
		obj1 = document.getElementById("_" + objid);
		obj2 = document.getElementById(objid);
		if (obj1 && obj2){
			obj2.checked = obj1.value;
		}
	}//end for
}
function showFormTag(obj, i){
	tabs = document.getElementsByTagName("td");
	for(k=0;k<tabs.length;k++){
		tab = tabs[k];
		if (tab.className=="activeTab") tab.className = "deactiveTab";
	}
	obj.className = "activeTab";
	divs = document.getElementsByTagName("div");
	for(k=0;k<divs.length;k++){
		div = divs[k];
		if (div.id.indexOf("_divTag")>0) div.style.display = "none";
	}
	div = document.getElementById("__divTag"+i);
	if (div) div.style.display = "";
}
function catmHover(obj){
	obj.style.border = "1px solid #CCE6FF";
	obj.style.background = "#FFFFF0";
}
function catmOut(obj){
	obj.style.border = "";
	obj.style.background = "";
}
function movePage(frm, n){
	pobj = document.getElementById(frm).page;
	p = pobj.value;
	if (n==-1 && p>1){
		pobj.value--;
		document.getElementById(frm).submit();
	}
	if (n==1 && p<pobj.options.length){
		pobj.value++;
		document.getElementById(frm).submit();
	}
}
function openSearch(objname){
	objs = document.getElementsByName(objname);
	if (!objs) return false;
	obj = objs[0];
	txt = prompt("Nhập từ khóa","", "Tìm kiếm");
	if (!txt) return;
	if (txt=="") return;	
	ret = -1; retval = "";
	for (i=0;i<obj.length;i++){
		v = obj.options[i].value;
		t = obj.options[i].text;
		if ((t.toUpperCase().indexOf(txt.toUpperCase())>=0) || (v.toUpperCase().indexOf(txt.toUpperCase())>=0)) {
			ret = i; retval = v;
			i = obj.length;
		}
	}
	if (ret!=-1) obj.value = retval;
	// obj.focus();
}
function checkList(cname){
	objs= document.getElementsByName(cname);
	if (!objs) return;
	for(i=0;i<objs.length;i++){
		objs[i].checked = !objs[i].checked;
	}
}

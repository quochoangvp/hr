<!-- BEGIN: main --><?php session_start();?>
<?php include("connect.php");?>
<?php
$table = "{table}"; //change to your table 
$form = "forms/{table}.master.htm"; //change to your form name
$pfld = "{pfield}"; //primary key
$dspflds = "*"; //fields to list
$dateflds = "{dateflds}";
$fileflds = array({fileflds}); // fld=>array(fldtype, filetype, filelocation)
$k = @$_REQUEST["pkey"]; $v = @$_REQUEST["pval"];
$filter = "$k='$v'";
$menuArr = array({menu});//look up value from dictionary
# now construct SQL command
$sql = "SELECT $dspflds FROM $table";
$where = "";
if ($filter<>""){
	$where.=" WHERE ";
	$where.=$filter;
}//end if where
$sql.=$where;
?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="views.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php
# main form
$commonLabels = $oCls->loadCommonLabels();
$tpl = new XTemplate($form);
$rs = $oConn->Execute($sql);
if (!$rs) die($oConn->ErrorMsg());
if ($dateflds<>""){//check date fields
	$dateArr = explode(",", $dateflds);
} else $dateArr=array();//end if dateflds	
while (!$rs->EOF){
	$row = $rs->fields;
	# format date fields
	for ($i=0;$i<count($dateArr); $i++){
		$row[$dateArr[$i]] = $oCls->userDate($row[$dateArr[$i]]);
	}
	# format file fields
	foreach($fileflds as $fld=>$arr){
		if ($row[$fld]!=NULL){
			$r = "";
			if ($arr["fldtype"]=="B"){
				$r = "[<a href=\"".$table.".".$fld.".get.php?key=".$row[$pfld]."\" target=\"_blank\">";
				$r.= "Download</a>]";
			} else {
				$r = "[<a href=\"../".$row[$fld]."\" target=\"_blank\">";
				$r.= "Download</a>]";
				if ($arr["filetype"]=="images"){
					$r = "<img border=0 width=100 src=\"../".$row[$fld]."\">";
				}
			}
			$row[$fld] = $r;
		}//end if file!=NULL
	}//end for file
	# get lookup values
	foreach($menuArr as $k=>$v){
		$v = str_replace("#value#", $row[$k], $v);
		$menuRS = $oConn->Execute($v);
		if ($menuRS){
			$tpl->assign($k, $menuRS->fields[0]);
			$row[$k] = $menuRS->fields[0];
		}		
	}//end of examining lookup
	# assign row
	$tpl->assign($table, $row);
	# assign common labels and parse
	$tpl->assign("common", $commonLabels);
	$tpl->parse("main.".$table);
	$rs->MoveNext();
}
# assign tab labels & common labels
$tpl->assign("labels", $oCls->loadTabLabels($table));
$tpl->assign("common", $commonLabels);
$tpl->parse("main");
$tpl->out("main");
$rs->Close();
?>
</body>
</html>
<!-- END: main -->
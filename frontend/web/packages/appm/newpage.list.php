<!-- BEGIN: main --><?php session_start();
include("connect.php");
$table = "{table}"; //change to your table 
$form = "{form}"; //change to your form name
$flds = "{fields}"; //fields being searched separated by comma
$pfld = "{pfield}"; //primary key
$dspflds = "{dspflds}"; //fields to list
$dateflds = "{dateflds}";
$numflds = "{numflds}";
$fileflds = array({fileflds}); // fld=>array(fldtype, filetype, filelocation)
$pagesize = {pagesize};
$filter = "{filter}";
$order = "{order}"; 
$ordertype = "{ordertype}";
$menuArr = array({menu});//look up value from dictionary
$staticMenu = array({staticmenu});
$multiselectArr = array({multiselect});
$msg = "";
# - command to reset
$cmd = @$_REQUEST["cmd"];
if ($cmd=="reset"){
	$search="";
	$_SESSION[$table."_search"]="";
	$_SESSION[$table."_txtsearch"] = "";
	$_SESSION[$table."_page"] = 1;	
}
if ($cmd=="resetall"){
	if (@$_SESSION[$table."_master_object"]!=""){
		$skey = str_replace(".", "_", $_SESSION[$table."_master_object"]);
		$_SESSION[$skey] = "";
	}
	$_SESSION[$table."_foreignkey"] = "";
	$_SESSION[$table."_foreignkey_value"] = "";
	$_SESSION[$table."_master_object"] = "";
	$_SESSION[$table."_page"] = 1;	
}

# for master-details relation
$masterkey = "{masterkey}";
$masterquote = "{masterquote}";
$mkeyArr = explode(",", $masterkey);
foreach($mkeyArr as $mkeyVar){
	if ($mkeyVar!=""){
		$mkey = @$_REQUEST[$mkeyVar];
		if ($mkey!=""){
			# keep current session
			$_SESSION[$table."_foreignkey"] = $mkeyVar;
			$_SESSION[$table."_foreignkey_value"] = $mkey;
			$_SESSION[$table."_master_object"] = @$_REQUEST["master"];
			if (@$_SESSION[$table."_master_object"]!=""){
				$skey = str_replace(".", "_", $_SESSION[$table."_master_object"]);
				$_SESSION[$skey] = $mkey;
			}
		} else {
			if ($_SESSION[$table."_foreignkey"]==$mkeyVar) {
				$mkey=@$_SESSION[$table."_foreignkey_value"];
			} else $mkey="";	
		}
		if ($mkey!=""){
			$filter = ($filter!="") ? $filter." AND " : "";
			$filter.= $mkeyVar."=".$masterquote.$mkey.$masterquote;
		}
	}//end if masterkey
}//end of foreach masterkey	
# for action: search
$a = @$_REQUEST["a"];
$search = @$_SESSION[$table."_search"];
if ($a=="S" && $flds<>""){ //searching for txtsearch
	$txtsearch = @$_REQUEST["txtsearch"];
	if ($txtsearch<>""){
		$fldArr = explode(",", $flds);
		$search="";
		for ($i=0;$i<count($fldArr);$i++){
			$search.="(".$fldArr[$i]." LIKE '%".$txtsearch."%') OR ";
		}
		if (strlen($search)>3) $search = substr($search, 0, strlen($search)-3);
		$_SESSION[$table."_search"] = $search;
		$_SESSION[$table."_txtsearch"] = $txtsearch;
		$_SESSION[$table."_page"] = 1;	
	}
}//end if search
# now construct SQL command
$sql = "SELECT $dspflds FROM $table";
$where = "";
if ($search<>"" || $filter<>""){
	$where.=" WHERE ";
	if ($search<>"") {
		$where.="(".$search.")";
		if ($filter<>"") $where.=" AND (".$filter.")";
	} else {
		if ($filter<>"") $where.=$filter;
	}	
}//end if where
$sql.=$where;
if ($order<>"") $sql.=" order by ".$order; if ($ordertype<>"") $sql.=" ".$ordertype;
# page navigation
$page = @$_REQUEST["page"]; 
if ($page=="") {
	$page = @$_SESSION[$table."_page"];
} else {
	$_SESSION[$table."_page"] = intval($page);
}	
if ($page=="") $page=1;
?>
<?php
# build search form
$tpl = new XTemplate("forms/search.htm");
$tpl->assign("form_action", @$_SERVER['PHP_SELF']);
$tpl->assign("txtsearch", @$_SESSION[$table.'_txtsearch']);
$tpl->parse("main");
$frmSearch = $tpl->text("main");
# build page menu
$tpl = new XTemplate("forms/page.htm");
$reccount = $oCls->GetFieldValue("select count(*) from $table".$where);  
$pagecount = intval($reccount/$pagesize); if (($reccount%$pagesize)>0) $pagecount++;
if ($page > $pagecount) $page = $pagecount;
for ($i=1;$i<=$pagecount;$i++){
	$sel = ""; if ($i==$page) $sel = " selected";
	$tpl->assign("page", $i); $tpl->assign("selected", $sel);
	$tpl->parse("main.line");
}
$tpl->parse("main");
$frmPage = $tpl->text("main");
# main form

$tpl = new XTemplate($form);
$rs = $oConn->PageExecute($sql, $pagesize, $page);
if (!$rs) die($oConn->ErrorMsg());
$dateArr = explode(",", $dateflds);
$numArr = explode(",", $numflds);
$rownum = 0;
while (!$rs->EOF){
	$row = $rs->fields;
	foreach($row as $fld=>$val) {
		$row[$fld] = stripslashes($val);
	}
	# format date fields
	foreach($dateArr as $fld){
		$row[$fld] = $oCls->userDate($row[$fld]);
	}
	# format numeric fields
	foreach($numArr as $fld){
		if ($fld!=$pfld && !in_array($fld, array_keys($menuArr))) {
			$row[$fld] = number_format($row[$fld], 0, ".", ",");
		}	
	}
	# format file fields
	foreach($fileflds as $fld=>$arr){
		if ($row[$fld]!=NULL){
			$r = "";
			$r = "[<a href=\"../../".$row[$fld]."\" target=\"_blank\">";
			$r.= "Download</a>]";
			if ($arr["filetype"]=="images"){
				$r = "<img border=0 width=100 src=\"../".$row[$fld]."\">";
			}
			$row[$fld] = $r;
		}//end if file!=NULL
	}//end for file
	# get lookup values
	foreach($menuArr as $k=>$v){
		$v = str_replace("#value#", $row[$k], $v);
		$menuRS = $oConn->Execute($v);
		if ($menuRS){
			$tpl->assign($k, $menuRS->fields[0]);
			$row[$k] = $menuRS->fields[0];
		}		
	}//end of examining lookup
	# static menu
	foreach($staticMenu as $k=>$vArr){
		if ($row[$k]!="") $row[$k] = @$vArr[$row[$k]];
	}
	# multiselect
	foreach($multiselectArr as $fld=>$sql){
		$row[$fld] = $oCls->viewMultiCheckboxes($sql, $row[$fld]);
	}
	
	# assign row
	$tpl->assign($table, $row);
	# assign common labels and parse
	$tpl->assign("common", $commonLabels);
	# tdclass
	$tdclass = "tdlist"; if ($rownum%2 == 1) $tdclass = "tdgrey";
	$tpl->assign("tdclass", $tdclass);
	# now parse row
	$tpl->parse("main.".$table);
	$rs->MoveNext(); $rownum++;
}
$tpl->assign("SEARCH", $frmSearch); $tpl->assign("PAGE", $frmPage);
# show master table
if (@$_SESSION[$table."_master_object"]!=""){
	$mobjArr = explode(".", $_SESSION[$table."_master_object"]);
	$masterdesc = $oCls->getMasterSection($table);
	$mpage = basename(dirname(@$_SERVER['PHP_SELF']))."/".$mobjArr[0].".php";
	if (strpos(@$_SERVER['PHP_SELF'], ".list.php")>0){
		$mpage = basename(dirname(@$_SERVER['PHP_SELF']))."/".$mobjArr[0].".list.php";
	}
	$masterdesc.= "<br>[<a href='../".$mpage."'>".$commonLabels["goback"]."</a>]";
	$masterdesc.= "<p>";
	$tpl->assign("mastertable", $masterdesc);
}
# statistic
$tpl->assign("TOTAL", number_format($reccount, 0, ".", ",")); 
$tpl->assign("PAGETOTAL", number_format($pagecount, 0, ".", ","));
# assign tab labels & common labels
$tpl->assign("labels", $oCls->loadTabLabels($table));
$tpl->assign("common", $commonLabels);
if ($rs->RecordCount()>0) $tpl->parse("main.button");
$rs->Close();

# parse the page
{HEADER}

if ($msg!="") echo $oCls->genAlert($msg);

$tpl->parse("main");
$tpl->out("main");

{FOOTER}
?>
<!-- END: main -->
<?php session_start();?>
<?php include("connect.php")?>
<?php
$dbname = @$_REQUEST["dbname"];
if ($dbname=="") $dbname = @$_SESSION["am_dbname"];
$rs = $oConn->Execute("select * from am_dbs where dbname='$dbname'");
if ($rs->RecordCount()<1) die("No database found !");
$host = $rs->fields["dbhost"];
$db = $rs->fields["dbname"];
$user = $rs->fields["dbuser"];
$pass = $rs->fields["dbpassword"];
$driver = $rs->fields["dbtype"];
$_lcConn = &ADONewConnection($driver);
$_lcConn->Connect($host, $user, $pass, $db); 
if ($_lcConn->ErrorMsg()!="") die("<font color=red>Can not connect to database [$db]</font>.");
$tables = $_lcConn->MetaTables("TABLES");
if (sizeof($tables)<=1) {
	if (trim($tables[0])=="") die("<font color=red>Can not connect to database [$db]</font>.");
}
$pkeys = array();
foreach($tables as $v){
	$pkeys[$v] = $_lcConn->MetaPrimaryKeys($v);
}
# $oConn->debug=true;
for($i=0; $i<count($tables); $i++){
	$v = $tables[$i];
	$fList["tableid"] = $oConn->GenID();
	$fList["tablename"] = $v;
	$fList["tabledesc"] = $v;
	$fList["dbname"] = $dbname;
	$n = $oCls->GetFieldValue("select max(ordernum) from am_tables where dbname='$dbname'");
	$fList["ordernum"] = intval($n)+1;
	# primary key
	$fList["tablepkey"] = implode(",", $pkeys[$v]);
	$e = $oCls->GetFieldValue("select tablename from am_tables where dbname='$dbname' and tablename='$v'");
	if ($e=="" && substr($v,0,4)!="seq_"){
		$oConn->Replace("am_tables", $fList, "tableid", true);
	}
	# update primary key
	if ($pkeys[$v]!=""){
		$oConn->Execute("update am_tables set tablepkey='".implode(",", $pkeys[$v])."' where dbname='$dbname' and tablename='$v'");
	}
}//end for
$tabstr = "'".implode("','", $tables)."'";
$sql = "delete from am_tables where dbname='".$dbname."' and tablename not in ($tabstr)";
$oConn->Execute($sql);
header("Location: am_tables.list.php?dbname=$dbname");
?>
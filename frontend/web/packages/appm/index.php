<?php session_start()?>
<?php include("connect.php")?>
<?php
# parameters here
function getDBS($frm){
	global $oConn; 
	$t = new XTemplate($frm);
	$rs = $oConn->Execute("select * from am_dbs where author like '%".$_SESSION["bcshop_status_User"]."%' order by dbname");
	while (!$rs->EOF){
		if (@$_SESSION["am_dbname"]=="") $_SESSION["am_dbname"] = $rs->fields["dbname"];
		$t->assign("am_dbs", $rs->fields);
		$tdclass=""; if ($rs->fields["dbname"]==@$_SESSION["am_dbname"]) $tdclass="tdselected";
		$t->assign("target", "_____frameContent");
		$t->assign("tdclass", $tdclass);
		$t->assign("groups", getGroups($oConn, $rs->fields["dbname"]));
		$t->parse("main.dbs");
		$rs->MoveNext();
	}
	$t->parse("main");
	$rs->Close();
	return $t->text("main");
}
function getGroups($conn, $dbn){
	$gt = new XTemplate("forms/am_groups.htm");
	$grs = $conn->Execute("select * from am_groups where dbname='$dbn' order by groupid");
	while (!$grs->EOF){
		$gt->assign("am_groups", $grs->fields);
		$gt->assign("target", "_____frameContent");
		$gt->parse("main.group");
		$grs->MoveNext();
	}
	$gt->parse("main");
	$grs->Close();
	return $gt->text("main");
}
?>
<?php include("header.php")?>
<?php
# display here
$target = "frameContent";
$canvas = "divTables";
$form = "forms/index.htm";
$formdb = "forms/db.list.htm";
$formtab = "forms/tab.list.htm";
$tpl = new XTemplate($form);
$tpl->assign("dbs", getDBS($formdb));
if (@$_SESSION["am_dbname"]!="") $tpl->assign("frameSrc", "am_tables.list.php?dbname=".@$_SESSION["am_dbname"]);
$tpl->parse("main");
$tpl->out("main");
?>
<?php include("footer.php")?>
String.prototype.trim = function()
{
    return this.replace(/(^\s*)|(\s*$)/g, "");
}
/**--------------------------
//* Validate Date Field script- By JavaScriptKit.com
//* For this script and 100s more, visit http://www.javascriptkit.com
//* This notice must stay intact for usage
---------------------------**/
function checkDate(input){
	var validformat=/^\d{2}\/\d{2}\/\d{4}$/; //Basic check for format validity
	var validformat1=/^\d{1}\/\d{2}\/\d{4}$/; //Basic check for format validity
	var validformat2=/^\d{2}\/\d{1}\/\d{4}$/; //Basic check for format validity
	var validformat3=/^\d{1}\/\d{1}\/\d{4}$/; //Basic check for format validity
	var returnval=false;
	if (!validformat.test(input.value) && !validformat1.test(input.value) && !validformat2.test(input.value) && !validformat3.test(input.value)){
		alert("Không đúng khuôn dạng: dd/mm/yyyy.");
	}else{ //Detailed check for valid date ranges
		var monthfield=input.value.split("/")[1];
		var dayfield=input.value.split("/")[0];
		var yearfield=input.value.split("/")[2];
		var dayobj = new Date(yearfield, monthfield-1, dayfield);
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
		{
			alert("Ngày không hợp lệ.");
		} else returnval=true;
	}
	if (returnval==false) input.select();
	//if (returnval) alert("OK");
	return returnval;
}//end of function checkdate
function checkDateValue(inputvalue){
	var validformat=/^\d{2}\/\d{2}\/\d{4}$/; //Basic check for format validity
	var validformat1=/^\d{1}\/\d{2}\/\d{4}$/; //Basic check for format validity
	var validformat2=/^\d{2}\/\d{1}\/\d{4}$/; //Basic check for format validity
	var validformat3=/^\d{1}\/\d{1}\/\d{4}$/; //Basic check for format validity
	var returnval=false;
	if (!validformat.test(inputvalue) && !validformat1.test(inputvalue) && !validformat2.test(inputvalue) && !validformat3.test(inputvalue)){
		return false;
	}else{ //Detailed check for valid date ranges
		var monthfield=input.value.split("/")[1];
		var dayfield=input.value.split("/")[0];
		var yearfield=input.value.split("/")[2];
		var dayobj = new Date(yearfield, monthfield-1, dayfield);
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
		{
			returnval = false;
		} else returnval=true;
	}
	return returnval;
}//end of function checkdate
function compareDate(d1, d2, cat){
	if (!checkDateValue(d1.value) || !checkDateValue(d2.value))	return 0;
	c = Date.parse(d2.value) - Date.parse(d1.value);
	if (cat){
		if (cat=="y") return Math.ceil(c/(3600000*24*365));
		if (cat=="m") return Math.ceil(c/(3600000*24*30));
		if (cat=="d") return Math.ceil(c/(3600000*24));
	}
	return c;
}//end of function comparedate
/* Now check numeric data*/
function checkNumber(object_value) {
	if (object_value.length == 0)
		return true;
	
	var start_format = " .+-0123456789";
	var number_format = " .0123456789";
	var check_char;
	var decimal = false;
	var trailing_blank = false;
	var digits = false;
	
	check_char = start_format.indexOf(object_value.charAt(0));
	if (check_char == 1)
		decimal = true;
	else if (check_char < 1)
		return false;
	 
	for (var i = 1; i < object_value.length; i++)	{
		check_char = number_format.indexOf(object_value.charAt(i))
		if (check_char < 0) {
			return false;
		} else if (check_char == 1)	{
			if (decimal)
				return false;
			else
				decimal = true;
		} else if (check_char == 0) {
			if (decimal || digits)	
			trailing_blank = true;
		}	else if (trailing_blank) { 
			return false;
		} else {
			digits = true;
		}
	}	
	return true;
}//end of checknumber
function checkNumberRange(num, lbound, ubound){
	if (!checkNumber(num) || !checkNumber(lbound) || !checkNumber(ubound)) return false;
	if (num=="") num=0;
	if ((num-lbound >= 0) && (ubound-num >= 0)) return true;
	return false;
}
function checkForm(dateflds, numflds, nnflds){
	if (dateflds!=""){
		dateArr = dateflds.split(",");
		for(i=0;i<dateArr.length;i++){
			obj = document.getElementById(dateArr[i]);
			if (obj){
				if (obj.value!="" && !checkDateValue(obj.value)) {
					alert("Invalid date !"); obj.focus();
					return false;
				}
			}//end if object exists
		}//end for
	}//end if dateflds<>""
	if (numflds!=""){
		numArr = numflds.split(",");
		for(i=0;i<numArr.length;i++){
			obj = document.getElementById(numArr[i]);
			if (obj){
				if (obj.value!="" && !checkNumber(obj.value)){
					alert("Invalid number !"); obj.focus();
					return false;
				}
			}//end if object exists
		}//end for
	}//end if numflds<>""
	if (nnflds && nnflds!=""){ // not null fields
		nnArr = nnflds.split(",");
		for(i=0;i<nnArr.length;i++){
			obj = document.getElementById(nnArr[i]);
			if (obj){
				if (obj.value.trim()==""){
					alert("Empty value !"); obj.focus();
					return false;
				}
			}//end if object exists
		}//end for
	}//end if nnflds<>""
	return true;
}
function removeMark(str){
	str= str.toLowerCase();  
	str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");  
	str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");  
	str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");  
	str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");  
	str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");  
	str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");  
	str= str.replace(/đ/g,"d");  
	str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-"); 
	str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1- 
	str= str.replace(/^\-+|\-+$/g,"");  
	return str;  
}//
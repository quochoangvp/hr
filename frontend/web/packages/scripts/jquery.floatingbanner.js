/*
 * Floating Banner jQuery Plugin v1.0
 * http://code.google.com/p/floatingbanner/
 *
 * Copyright 2010, Funke Web Solutions
 * Released under the GPL Version 3 license.
 *
 * $Id: jquery.floatingbanner.js 4172 2010-04-05 05:57:21Z rob $
 */
(function($) {
    jQuery.fn.floatingbanner = function(options) {

        var settings = jQuery.extend({
            ie6: true
        }, options);

        var banners = new Array();
        this.each(function() {
            var placeholder = $('<div></div>').height($(this).outerHeight()).width($(this).outerWidth()).hide().insertBefore(this);
            var posTop = $(this).position().top;
            banners.push({ 'banner': this, 'placeholder': placeholder, 'posTop': posTop });
            var scrollTop = $(window).scrollTop();
            if (pastView(posTop, scrollTop))
                floatBanner(this, placeholder);
        });

        function floatBanner(element, placeholder) {
            $(element).addClass('floating-banner');
            $(element).css({ 'top': '', 'position': '' });

            $(placeholder).show();
            if (!settings.ie6 && $.browser.msie && parseInt(jQuery.browser.version) == 6) {
                $(element).css({ 'top': $(window).scrollTop(), 'position': 'absolute' });
            }
        }

        function unFloatBanner(element, placeholder) {
            $(element).removeClass('floating-banner');
            $(element).css({ 'position': 'absolute', 'width': '240px' });

            $(placeholder).hide();
            if (!settings.ie6 && $.browser.msie && parseInt(jQuery.browser.version) == 6) {
                $(element).css({ 'position': 'static' });
            }
        }

        function pastView(posTop, scrollTop) {
            if (scrollTop < posTop)
                return false;
            return true;
        };

        function fixBanner(element, placeholder, top) {
            $(element).removeClass('floating-banner');
            $(element).css({ 'top': (top-10), 'position': 'absolute', 'width': '240px' });//VietNM fix top = top - 10

            $(placeholder).hide();
            if (!settings.ie6 && $.browser.msie && parseInt(jQuery.browser.version) == 6) {
                $(element).css({ 'position': 'static' });
            }
        }

        $(window).scroll(function() {
            var scrollTop = $(window).scrollTop();
            var h = $(document).height();
            var wh = $(window).height();
            
            var m = h - 255;

            $(banners).each(function() {
                var dh = $(this.banner).attr('offsetHeight');
                
                if (pastView(this.posTop, scrollTop)) {
                    if (scrollTop < (m - dh)) {
                        floatBanner($(this.banner), $(this.placeholder));
                    } else {
                        fixBanner($(this.banner), $(this.placeholder), (m - dh));
                    }
                }
                else
                    unFloatBanner($(this.banner), $(this.placeholder));
            });
        });
    };
})(jQuery);
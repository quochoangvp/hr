function updateCanvas(frameid, canvasid){
	if (!frameid) return;
	if (!canvasid) return;
	cobj = document.getElementById(canvasid);
	if (!cobj) return;
	fobj = document.getElementById(frameid);
	if (!fobj) return;
	cobj.innerHTML = fobj.contentWindow.document.body.innerHTML;

	// set target for all links and forms
	aobjs = cobj.getElementsByTagName("a");
	if (!aobjs) aobjs = cobj.getElementsByTagName("A");
	k = 0;
	for(i=0;i<aobjs.length;i++){
		e = aobjs[i];
		if (e.href.indexOf(".php")>0) e.target = frameid;
	}
	// set target for all forms
	aobjs = cobj.getElementsByTagName("form");
	if (!aobjs) aobjs = cobj.getElementsByTagName("FORM");
	for(i=0;i<aobjs.length;i++){
		e = aobjs[i];
		e.target = frameid;
	}
}//end of updateCanvas
function updateView(frameid, canvasid){
	if (!frameid) return;
	if (!canvasid) return;
	cobj = document.getElementById(canvasid);
	if (!cobj) return;
	fobj = document.getElementById(frameid);
	if (!fobj) return;
	cobj.innerHTML = fobj.contentWindow.document.body.innerHTML;
}//end of updateView
function movePage(frm, n){
	pobj = document.getElementById(frm).page;
	p = pobj.value;
	if (n==-1 && p>1){
		pobj.value--;
		document.getElementById(frm).submit();
	}
	if (n==1 && p<pobj.options.length){
		pobj.value++;
		document.getElementById(frm).submit();
	}
}
function movePage2(frm, n){
	pcobj = "pagecount";
	pobj = document.getElementById(frm).page;
	p = pobj.value;
	if (n==-1 && p>1){
		pobj.value--;
		document.getElementById(frm).submit();
	}
	if (n==1 && p-document.getElementById(pcobj).value<0){
		pobj.value++;
		document.getElementById(frm).submit();
	}
}
function openSearch(objname){
	objs = document.getElementsByName(objname);
	if (!objs) return false;
	obj = objs[0];
	txt = prompt("Nhập từ khóa","", "Tìm kiếm");
	if (!txt) return;
	if (txt=="") return;	
	ret = -1; retval = "";
	for (i=0;i<obj.length;i++){
		v = obj.options[i].value;
		t = obj.options[i].text;
		if ((t.toUpperCase().indexOf(txt.toUpperCase())>=0) || (v.toUpperCase().indexOf(txt.toUpperCase())>=0)) {
			ret = i; retval = v;
			i = obj.length;
		}
	}
	if (ret!=-1) obj.value = retval;
	// obj.focus();
}
function removePortlet(id){
	if (!window.confirm("Chắc chắn xóa ?")){
		return;
	}	
	frm = document.getElementById("__frameContext");
	if (!frm) return;
}//end of removePortlet
function showFormTag(obj, i){
	tabs = document.getElementsByTagName("td");
	for(k=0;k<tabs.length;k++){
		tab = tabs[k];
		if (tab.className=="activeTab") tab.className = "deactiveTab";
	}
	obj.className = "activeTab";
	divs = document.getElementsByTagName("div");
	for(k=0;k<divs.length;k++){
		div = divs[k];
		if (div.id.indexOf("_divTag")>0) div.style.display = "none";
	}
	div = document.getElementById("__divTag"+i);
	if (div) div.style.display = "";
}

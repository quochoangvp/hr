var offsetfromcursorX = 12;
var offsetfromcursorY = 10;
var offsetdivfrompointerX = 10;
var offsetdivfrompointerY = 14;

document.write('<div id="divtooltip"></div>');
document.write('<img id="imgpointer" src="../images/tooltiparrow.gif">');

var ie = document.all;
var ns6 = document.getElementById && ! document.all;
var enabletip = false;
var tipobj = document.getElementById("divtooltip");
var pointerobj = document.getElementById("imgpointer");

function getDocumentBody() {
	return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
}

function showTooltip(thetext, thewidth, thecolor) {
	var config = document.getElementById("portal_showtip"); 
	if (!config) return; if (config.value!="1") return;
	if (thetext=="") return;
	if (ns6 || ie) {
		if (typeof thewidth != "undefined")
			tipobj.style.width = thewidth + "px";
		if (typeof thecolor != "undefined" && thecolor != "")
			tipobj.style.backgroundColor = thecolor;
		tipobj.innerHTML = thetext;
		tipobj.onmouseout = hideTooltip;
		enabletip = true;
		return false;
	}
}

function PlaceTooltip(e) {
	if (enabletip) {
		var nondefaultpos = false;
		var curX = (ns6) ? e.pageX : event.clientX + getDocumentBody().scrollLeft;
		var curY = (ns6) ? e.pageY : event.clientY + getDocumentBody().scrollTop;
		
		var winwidth = ie && ! window.opera ? getDocumentBody().clientWidth : window.innerWidth - 20;
		var winheight = ie && ! window.opera ? getDocumentBody().clientHeight : window.innerHeight - 20;

		var rightedge = ie && ! window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
		var bottomedge = ie && ! window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;

		var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (- 1) : - 1000;

		if (rightedge < tipobj.offsetWidth) {
			tipobj.style.left = curX - tipobj.offsetWidth + "px";
			nondefaultpos = true;
		}
		else if (curX < leftedge)
			tipobj.style.left = "5px";
		else {
			tipobj.style.left = curX + offsetfromcursorX - offsetdivfrompointerX + "px";
			pointerobj.style.left = curX + offsetfromcursorX + "px";
		}

		if (bottomedge < tipobj.offsetHeight) {
			tipobj.style.top = curY - tipobj.offsetHeight - offsetfromcursorY + "px";
			nondefaultpos = true;
		}
		else {
			tipobj.style.top = curY + offsetfromcursorY + offsetdivfrompointerY + "px";
			pointerobj.style.top = curY + offsetfromcursorY + "px";
		}

		tipobj.style.visibility = "visible";

		if (! nondefaultpos)
			pointerobj.style.visibility = "visible";
		else
			pointerobj.style.visibility = "hidden";
	}
}

function hideTooltip() {
	if (ns6 || ie) {
		enabletip = false;
		tipobj.style.visibility = "hidden";
		pointerobj.style.visibility = "hidden";
		tipobj.style.left = "-1000px";
		tipobj.style.backgroundColor = '';
		tipobj.style.width = '';
	}
}

if ( typeof window.addEventListener != "undefined" ){
	document.addEventListener( "mousemove", PlaceTooltip, false );
} else if ( typeof window.attachEvent != "undefined" ){
	document.attachEvent( "onmousemove", PlaceTooltip );
} else {
	if ( document.onmousemove != null ) {
		var oldOnmousemove = document.onmousemove;
		document.onmousemove = function ( e ) {
			oldOnmousemove( e );
			PlaceTooltip(e);
		};
	} else document.onmousemove = PlaceTooltip;
}
(function($) {
    $.fn.tipsy = function(options) {

        options = $.extend({}, $.fn.tipsy.defaults, options);
        
        return this.each(function() {
            
            var opts = $.fn.tipsy.elementOptions(this, options);
            
            $(this).hover(function() {

                $.data(this, 'cancel.tipsy', true);

                var tip = $.data(this, 'active.tipsy');
                if (!tip) {
                    tip = $('<div class="tipsy"><div class="tipsy-inner"/></div>');
                    tip.css({position: 'absolute', zIndex: 100000});
                    $.data(this, 'active.tipsy', tip);
                }

                if ($(this).attr('title') || typeof($(this).attr('original-title')) != 'string') {
                    $(this).attr('original-title', $(this).attr('title') || '').removeAttr('title');
                }

                var title;
                if (typeof opts.title == 'string') {
                    title = $(this).attr(opts.title == 'title' ? 'original-title' : opts.title);
                } else if (typeof opts.title == 'function') {
                    title = opts.title.call(this);
                }

                tip.find('.tipsy-inner')[opts.html ? 'html' : 'text'](title || opts.fallback);

                var pos = $.extend({}, $(this).offset(), {width: this.offsetWidth, height: this.offsetHeight});
                tip.get(0).className = 'tipsy'; // reset classname in case of dynamic gravity
                tip.remove().css({top: 0, left: 0, visibility: 'hidden', display: 'block'}).appendTo(document.body);
                var actualWidth = tip[0].offsetWidth, actualHeight = tip[0].offsetHeight;
                var gravity = (typeof opts.gravity == 'function') ? opts.gravity.call(this) : opts.gravity;

                switch (gravity.charAt(0)) {
                    case 'n':
                        tip.css({top: pos.top + pos.height, left: pos.left + pos.width / 2 - actualWidth / 2}).addClass('tipsy-north');
                        break;
                    case 's':
                        tip.css({top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2}).addClass('tipsy-south');
                        break;
                    case 'e':
                        tip.css({top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth}).addClass('tipsy-east');
                        break;
                    case 'w':
                        tip.css({top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width}).addClass('tipsy-west');
                        break;
                }

                if (opts.fade) {
                    tip.css({opacity: 0, display: 'block', visibility: 'visible'}).animate({opacity: 0.8});
                } else {
                    tip.css({visibility: 'visible'});
                }

            }, function() {
                $.data(this, 'cancel.tipsy', false);
                var self = this;
                setTimeout(function() {
                    if ($.data(this, 'cancel.tipsy')) return;
                    var tip = $.data(self, 'active.tipsy');
                    if (opts.fade) {
                        tip.stop().fadeOut(function() { $(this).remove(); });
                    } else {
                        tip.remove();
                    }
                }, 100);

            });
            
        });
        
    };
    
    // Overwrite this method to provide options on a per-element basis.
    // For example, you could store the gravity in a 'tipsy-gravity' attribute:
    // return $.extend({}, options, {gravity: $(ele).attr('tipsy-gravity') || 'n' });
    // (remember - do not modify 'options' in place!)
    $.fn.tipsy.elementOptions = function(ele, options) {
        return $.metadata ? $.extend({}, options, $(ele).metadata()) : options;
    };
    
    $.fn.tipsy.defaults = {
        fade: false,
        fallback: '',
        gravity: 'n',
        html: false,
        title: 'title'
    };
    
    $.fn.tipsy.autoNS = function() {
        return $(this).offset().top > ($(document).scrollTop() + $(window).height() / 2) ? 's' : 'n';
    };
    
    $.fn.tipsy.autoWE = function() {
        return $(this).offset().left > ($(document).scrollLeft() + $(window).width() / 2) ? 'e' : 'w';
    };
    
})(jQuery);


//get text Editor ( TinyMCE )  doanns  
    function getTinyMce(_id){
        tinyMCE.init({
            elements : _id,
    		mode : "exact",
    		theme : "advanced",
            skin : "default",
    		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
    		theme_advanced_buttons1 : "code,|,bold,italic,underline,strikethrough,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,link,unlink,image",
    		theme_advanced_buttons2 :"",
    		theme_advanced_buttons3 :"",
    		theme_advanced_toolbar_location : "top",
    		theme_advanced_toolbar_align : "left",
    		theme_advanced_statusbar_location : "bottom",
    		theme_advanced_resizing : true,
            // Example content CSS (should be your site CSS)
    		content_css : "css/content.css",
    
    		// Drop lists for link/image/media/template dialogs
    		template_external_list_url : "lists/template_list.js",
    		external_link_list_url : "lists/link_list.js",
    		external_image_list_url : "lists/image_list.js",
    		media_external_list_url : "lists/media_list.js",
    
    		// Style formats
    		style_formats : [
    			{title : 'Bold text', inline : 'b'},
    			{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
    			{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
    			{title : 'Example 1', inline : 'span', classes : 'example1'},
    			{title : 'Example 2', inline : 'span', classes : 'example2'},
    			{title : 'Table styles'},
    			{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
    		],
    
    		// Replace values for the template plugin
    		template_replace_values : {
    			username : "Some User",
    			staffid : "991234"
    		}
    	});
    }


$(document).ready(function(){
    //$('.showjobselected a').floatingbanner();
    
    $(".ldo-job-blue").hide();
    $(".cat-list-home").show();
    $("#full").click(function() {
        $(".ldo-job-blue").show();
        $(".cat-list-home").hide();
    });
    $("#simple").click(function() {
        $(".ldo-job-blue").hide();
        $(".cat-list-home").show();
    });
    

    var name = "#float-banner-left";
    var menuYloc = null;
    menuYloc = parseInt($(name).css("top").substring(0,$(name).css("top").indexOf("px")));
	$(window).scroll(function () { 
		offset = menuYloc+$(document).scrollTop()+"px";
		$(name).animate({top:offset},{duration:500,queue:false});
	});        
    
    $("#buttonshowjobselected").click(function(){
    	setTimeout("$.ajax({type: 'POST',data: 'getCountViewed=1',success: function(res){$('#viewedjob').html(res);}});", 3000);
    });
});
function onViewJob(_name){
    var check=0;
    var listjob='';
    var job=document.getElementsByName(_name);
    for(var i=0; i<job.length; i++){
        if(job[i].checked==true){
            listjob+=job[i].value+',';
            check++;
        }
    }
    if(check>0){
        var strjob =listjob.substr(0,listjob.length-1);
        var url='/Tin-tuyen-dung/xem-tin-chon/'+strjob;
        
        $('.showjobselected a').attr('href',url);
        $('.showjobselected a').html('Xem <span style="color:red;font-weight:bold;">'+check+'</span> tin đã chọn')
        $('.showjobselected').fadeIn('fast');
        $('.showjobselected a').addClass('floating-banner');
    }else{
        $('.showjobselected').fadeOut('fast');
        $('.showjobselected a').removeClass('floating-banner');
    }
}


function onSendJob(jobid,jobtitle){
        window.open('/Email-job/'+jobid+'/'+jobtitle,"mywindow","location=1,status=1,scrollbars=1, width=650,height=500");
    }
    
function onPrintJob(url){
	window.open(url,"mywindow","location=1,status=1,scrollbars=1, width=750,height=500");
}

























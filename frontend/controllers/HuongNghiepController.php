<?php

namespace frontend\controllers;

use common\models\Vg;

class HuongNghiepController extends \yii\web\Controller
{
	public function actionIndex($id = null)
	{
		if ($id) {
			$vg = Vg::find()->where(['id' => $id])->one();
			if ($vg) {
				return $this->render('chi-tiet', [
					'vg' => $vg
				]);
			} else {
				\Yii::$app->getResponse()->redirect('@web/huong-nghiep');
			}
		} else {
			$vgs = Vg::find()->orderBy('created_at DESC')->all();
			return $this->render('index', [
				'vgs' => $vgs
			]);
		}
	}

}

<?php
namespace frontend\controllers;

use Yii;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use common\models\Job;
use common\models\JobCate;
use common\models\Cv;
use common\models\Employer;
use common\models\Employee;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $jobs = Job::find()->select('job.*, jobcate.name as cate_name, employer.coname as employer_name')
            ->leftJoin('jobcate', '`jobcate`.`id` = `job`.`jobcat_id`')
            ->with('employer')
            ->leftJoin('employer', '`employer`.`id` = `job`.`employer_id`')
            ->with('jobCate')
            ->orderBy([
                'created_at' => SORT_DESC
            ])->asArray()
            ->all();
        $jobCates = JobCate::find()->all();
        return $this->render('index', [
            'jobs' => $jobs,
            'jobCates' => $jobCates
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionDangNhap()
    {
        $session = Yii::$app->session;
        if ($session['employee']) {
            return $this->goHome();
        }

        return $this->render('login');
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        @session_destroy();
        unset($_SESSION);

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionLienHe()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->subject = 'Liên hệ của ' . $model->name;
            if ($model->validate()) {
                if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                    Yii::$app->session->setFlash('error', null);
                    Yii::$app->session->setFlash('success', 'Yêu cầu của bạn đã được gửi đi. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất');
                } else {
                    Yii::$app->session->setFlash('success', null);
                    Yii::$app->session->setFlash('error', 'Hệ thống xảy ra sự cố, không thể gửi email');
                }
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('success', null);
                Yii::$app->session->setFlash('error', 'Hãy chắc chắn bạn đã nhập thông tin chính xác');
                return $this->refresh();
            }
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionGioiThieu()
    {
        return $this->render('about');
    }

    /**
     * Ứng viên đăng ký
     *
     * @return mixed
     */
    public function actionDangKy()
    {
        $model = new Employee();

        if ($model->load(Yii::$app->request->post())) {
            $model->password = md5($model->password);
            $model->birthday = date_format(date_create($model->birthday), 'Y-m-d');
            if ($model->save()) {
                return $this->render('login', [
                    'message' => 'Đăng ký thành công, bây giờ bạn có thể đăng nhập'
                ]);
            } else {
                return $this->render('signup', [
                    'model' => $model,
                    'message' => 'Không thể đăng ký, hãy nhập đẩy đủ thông tin yêu cầu'
                ]);
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $message = '';
        $status = true;
        if (count(Yii::$app->request->post()) > 0) {
            $email = Yii::$app->request->post('email');
            $employee = Employee::find()->where(['email' => $email])->one();
            if ($employee) {
                $newPass = $this->generateRandomString(6);
                $employee->password = md5($newPass);
                if ($employee->save()) {
                    if (Yii::$app->mailer->compose()
                    ->setTo($email)
                    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['adminEmail']])
                    ->setSubject('Khôi phục mật khẩu')
                    ->setHtmlBody('<h1>Mật khẩu mới cho tài khoản của bạn</h1><div><big>' . $newPass . '</big></div>')
                    ->send()) {
                        $message = 'Mật khẩu đã được gửi tới email của bạn.';
                    } else {
                        $status = false;
                        $message = 'Không thể gửi email, hãy thử lại';
                    }
                } else {
                    $status = false;
                    $message = 'Không thể tạo mật khẩu mới, hãy thử lại';
                }
            } else {
                $status = false;
                $message = 'Không tìm thấy tài khoản nào có email này!';
            }
        }
        return $this->render('requestPasswordResetToken', [
            'message' => $message,
            'status' => $status
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionSearch($keyword = '', $category = 0)
    {
        if ($category != 0) {
            $cateWhere = ['job.jobcat_id' => $category];
        } else {
            $cateWhere = 1;
        }
        $jobs = Job::find()->select('job.*, jobcate.name as cate_name, employer.coname as employer_name')
            ->leftJoin('jobcate', '`jobcate`.`id` = `job`.`jobcat_id`')
            ->with('employer')
            ->leftJoin('employer', '`employer`.`id` = `job`.`employer_id`')
            ->with('jobCate')
            ->where(['like', 'job.title', $keyword])
            ->where($cateWhere)
            ->orderBy([
                'created_at' => SORT_DESC
            ])->asArray()
            ->all();
        return $this->render('search', [
            'jobs' => $jobs,
        ]);
    }

    public function actionTimHoSo($keyword = '')
    {
        if (strlen($keyword) !== 0) {
            $where = ['like', 'cv.title', $keyword];
        } else {
            $where = 1;
        }
        $cvs = Cv::find()->select('cv.*, employee.fullname as employee_name, jobcate.name as cate_name')
            ->leftJoin('employee', '`employee`.`id` = `cv`.`employee_id`')
            ->with('employee')
            ->leftJoin('jobcate', '`jobcate`.`id` = `cv`.`jobcat_id`')
            ->with('jobCate')
            ->where($where)
            ->orderBy([
                'cv.created_at' => SORT_DESC
            ])->asArray()
            ->all();
        return $this->render('tim-ho-so', [
            'cvs' => $cvs,
        ]);
    }

    public function actionNhaTuyenDung() {
        $session = Yii::$app->session;
        $jobs = [];
//        unset($session['employer']);
        if ($session['employer']) {

            $jobs = Job::find()->select('job.*, jobcate.name as cate_name, employer.coname as employer_name')
                ->leftJoin('jobcate', '`jobcate`.`id` = `job`.`jobcat_id`')
                ->with('employer')
                ->leftJoin('employer', '`employer`.`id` = `job`.`employer_id`')
                ->with('jobCate')
                ->where(['employer.id' => $session['employer']['id']])
                ->orderBy([
                    'created_at' => SORT_DESC
                ])->asArray()
                ->all();

            $view = 'nhaTuyenDUng';
        } else {
            $view = 'nhaTuyenDungDangNhap';
        }

        return $this->render($view, [
            'jobs' => $jobs,
        ]);
    }

    public function actionDoEmployerLogin() {
        $email = Yii::$app->request->post('email');
        $password = md5(Yii::$app->request->post('password'));
        $employer = Employer::find()->where(['email' => $email, 'password' => $password])->asArray()->one();
        $session = Yii::$app->session;
        $session['employer'] = $employer;
        if ($employer) {
            echo json_encode(['status' => true, 'data' => $employer]);
        } else {
            echo json_encode(['status' => false]);
        }
    }

    public function actionDoEmployerRegister()
    {
        $model = new Employer();

        if ($model->load(Yii::$app->request->post())) {
            $model->password = md5($model->password);
            $model->cologo = UploadedFile::getInstance($model, 'cologo');
            if ($model->save()) {
                if ($model->cologo) {
                    $model->upload();
                }
                echo json_encode(['status' => true, 'message' => 'Đăng ký thành công', 'url' => Url::to('@web/site/nha-tuyen-dung')]);
            } else {
                echo json_encode(['status' => false, 'message' => 'Không thể đăng ký, hãy thử lại']);
            }
        } else {
            echo json_encode(['status' => false, 'message' => 'Hãy điền đầy đủ thông tin']);
        }
    }

    public function actionDoEmployeeLogin() {
        $email = Yii::$app->request->post('email');
        $password = md5(Yii::$app->request->post('password'));
        $employee = Employee::find()->where(['email' => $email, 'password' => $password])->asArray()->one();
        $session = Yii::$app->session;
        $session['employee'] = $employee;
        if(Yii::$app->request->post('job_id')) {
            $jobId = intval(Yii::$app->request->post('job_id'));
            $job = Job::find()->where(['id' => $jobId])->one();
            $job->apply_num++;
            if ($job->save(false)) {
                $this->sendEmailApplyJob($job['employer_id'], $session['employee']['id']);
                echo json_encode(['status' => true, 'url' => Url::to('@web/viec-lam/ung-tuyen-thanh-cong')]);
            } else {
                echo json_encode(['status' => false]);
            }
        } else {
            if ($employee) {
                echo json_encode(['status' => true, 'url' => Url::to('@web/')]);
            } else {
                echo json_encode(['status' => false]);
            }
        }
    }
    private function sendEmailApplyJob($employer_id, $employee_id) {
        $employer = Employer::find()->where(['id' => $employer_id])->one();
        $employee = Employee::find()->where(['id' => $employee_id])->one();
        $cv = Cv::find()->where(['employee_id' => $employee->id])->one();
        return Yii::$app->mailer->compose()
            ->setTo($employer->email)
            ->setFrom([$employee->email => $employee->email])
            ->setSubject('Ứng viên mới ứng tuyển')
            ->setHtmlBody('<h1>Ứng viên ' . $employee->fullname . '</h1><div>' . $cv->body . '</div>')
            ->send();
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

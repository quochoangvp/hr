<?php

namespace frontend\controllers;

use Yii;
use common\models\Job;
use common\models\Jobcate;
use common\models\Employer;
use yii\helpers\Url;
use yii\web\UploadedFile;

class NhaTuyenDungController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionDangViec() {
        $jobCats = Jobcate::find()->asArray()->all();
        return $this->render('dang-viec', [
            'jobCats' => $jobCats
        ]);
    }

    public function actionGuiViecMoi() {
        $session = Yii::$app->session;
        $jobObj = new Job();
        $jobObj->title = Yii::$app->request->post('title');
        $jobObj->body = htmlentities(Yii::$app->request->post('body'));
        $jobObj->deadline = date_format(date_create(Yii::$app->request->post('deadline')), 'Y-m-d');
        $jobObj->jobcat_id = Yii::$app->request->post('jobcat_id');
        $jobObj->employer_id = $session['employer']['id'];
        $jobObj->created_at = date('Y-m-d H:j:s');
        $jobObj->updated_at = date('Y-m-d H:j:s');
        $jobObj->apply_num = 0;
        if($jobObj->save(false)) {
            echo json_encode(['status' => true, 'url' => Url::to('@web/viec-lam/chi-tiet/?id=' . $jobObj->id)]);
        } else {
            echo json_encode(['status' => false]);
        }
    }

    public function actionDangKy() {
        return $this->render('dang-ky');
    }

    public function actionDoEmployeeRegister() {
        $employer = new Employer();
        $employer->email = Yii::$app->request->post('email');
        $employer->password = md5(Yii::$app->request->post('password'));
        $employer->coname = Yii::$app->request->post('coname');
        $employer->codetails = Yii::$app->request->post('codetails');
        $employer->coaddress = Yii::$app->request->post('coaddress');
        $employer->cowebsite = Yii::$app->request->post('cowebsite');
        $employer->cocontact = Yii::$app->request->post('cocontact');
        $thumbnail = UploadedFile::getInstance($model, 'thumbnail');
    }

    public function actionLayLaiMatKhau()
    {
        $message = '';
        $status = true;
        if (count(Yii::$app->request->post()) > 0) {
            $email = Yii::$app->request->post('email');
            $employer = Employer::find()->where(['email' => $email])->one();
            if ($employer) {
                $newPass = $this->generateRandomString(6);
                $employer->password = md5($newPass);
                if ($employer->save()) {
                    if (Yii::$app->mailer->compose()
                    ->setTo($email)
                    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['adminEmail']])
                    ->setSubject('Khôi phục mật khẩu')
                    ->setHtmlBody('<h1>Mật khẩu mới cho tài khoản của bạn</h1><div><big>' . $newPass . '</big></div>')
                    ->send()) {
                        $message = 'Mật khẩu đã được gửi tới email của bạn.';
                    } else {
                        $status = false;
                        $message = 'Không thể gửi email, hãy thử lại';
                    }
                } else {
                    $status = false;
                    $message = 'Không thể tạo mật khẩu mới, hãy thử lại';
                }
            } else {
                $status = false;
                $message = 'Không tìm thấy tài khoản nào có email này!';
            }
        }
        return $this->render('lay-lai-mat-khau', [
            'message' => $message,
            'status' => $status
        ]);
    }

    public function actionHoSo()
    {
        $session = Yii::$app->session;
        $employer = Employer::find()->where(['id' => $session['employer']['id']])->one();
        // var_dump(Yii::$app->request->post());die;
        if ($employer->load(Yii::$app->request->post())) {
            $employer->password = md5($employer->password);
            $employer->cologo = UploadedFile::getInstance($employer, 'cologo');
            $success = null;
            $error = null;
            if ($employer->save()) {
                if ($employer->cologo) {
                    $employer->upload();
                }
                $success = 'Cập nhật thành công';
            } else {
                $error = 'Cập nhật thất bại';
            }
            return $this->render('ho-so', [
                'employer' => $employer,
                'error' => $error,
                'success' => $success
            ]);
        } else {
            return $this->render('ho-so', [
                'employer' => $employer
            ]);
        }
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}

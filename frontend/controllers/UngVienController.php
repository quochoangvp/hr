<?php

namespace frontend\controllers;

use Yii;
use common\models\Cv;

class UngVienController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionHoSo() {
        $session = Yii::$app->session;
        $cv = Cv::find()->where(['employee_id' => $session['employee']['id']])->asArray()->one();
        return $this->render('ho-so',[
            'cv' => $cv,
            'employee' => $session['employee']
        ]);
    }

}

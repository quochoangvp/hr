<?php

namespace frontend\controllers;

use common\models\Cv;
use yii\web\NotFoundHttpException;

class HoSoController extends \yii\web\Controller
{
	public function actionIndex()
	{
		$cvs = Cv::find()->select('cv.*, employee.fullname as employee_name, jobcate.name as cate_name')
		->leftJoin('employee', '`employee`.`id` = `cv`.`employee_id`')
		->with('employee')
		->leftJoin('jobcate', '`jobcate`.`id` = `cv`.`jobcat_id`')
		->with('jobCate')
		->orderBy([
			'cv.created_at' => SORT_DESC
			])->asArray()
		->all();
		return $this->render('index', [
		'cvs' => $cvs,
		]);
	}

	public function actionChiTiet($id)
	{

		$model = $this->findModel($id);
		$model->countview++;
		$model->save();
		$cv = Cv::find()->select('cv.*, employee.fullname as employee_name, jobcate.name as cate_name')
		->leftJoin('employee', '`employee`.`id` = `cv`.`employee_id`')
		->with('employee')
		->leftJoin('jobcate', '`jobcate`.`id` = `cv`.`jobcat_id`')
		->with('jobCate')
		->where(['cv.id' => $id])
		->asArray()
		->one();
		return $this->render('chi-tiet', [
		'cv' => $cv,
		]);
	}

	protected function findModel($id)
    {
        if (($model = Cv::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

<?php

namespace frontend\controllers;

use Yii;
use common\models\Employer;
use common\models\Employee;
use common\models\Job;
use common\models\JobCate;
use common\models\Cv;
use yii\helpers\Url;

class ViecLamController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $jobs = Job::find()->select('job.*, jobcate.name as cate_name, employer.coname as employer_name')
            ->leftJoin('jobcate', '`jobcate`.`id` = `job`.`jobcat_id`')
            ->with('employer')
            ->leftJoin('employer', '`employer`.`id` = `job`.`employer_id`')
            ->with('jobCate')
            ->orderBy([
                'created_at' => SORT_DESC
            ])->asArray()
            ->all();
        return $this->render('index', [
            'jobs' => $jobs
        ]);
    }

    public function actionChiTiet($id)
    {
        $job = Job::find()->select('job.*, jobcate.name as cate_name, employer.coname as coname, employer.coaddress as coaddress, employer.cocontact as cocontact, employer.codetails as codetails')
            ->leftJoin('jobcate', '`jobcate`.`id` = `job`.`jobcat_id`')
            ->with('employer')
            ->leftJoin('employer', '`employer`.`id` = `job`.`employer_id`')
            ->with('jobCate')
            ->where(['job.id' => $id])
            ->orderBy([
                'created_at' => SORT_DESC
            ])->asArray()
            ->one();
        return $this->render('chi-tiet', [
            'job' => $job
        ]);
    }

    public function actionTheoNganhNghe($id)
    {
        $id = intval($id);
        if ($id) {
            $jobCate = JobCate::find()->where(['id' => $id])->one();
            $jobs = Job::find()->select('job.*, jobcate.name as cate_name, employer.coname as employer_name')
                ->leftJoin('jobcate', '`jobcate`.`id` = `job`.`jobcat_id`')
                ->with('employer')
                ->leftJoin('employer', '`employer`.`id` = `job`.`employer_id`')
                ->with('jobCate')
                ->where(['jobcat_id' => $id])
                ->orderBy([
                    'created_at' => SORT_DESC
                ])->asArray()
                ->all();
            return $this->render('theo-nganh-nghe', [
                'jobCate' => $jobCate,
                'jobs' => $jobs
            ]);
        } else {
            \Yii::$app->getResponse()->redirect('@web');
        }
    }

    public function actionNhaTuyenDung($id = null)
    {
        if ($id) {
            $id = intval($id);
            $employer = Employer::find()->where(['id' => $id])->one();
            if ($employer) {
                $jobs = Job::find()->select('job.*, jobcate.name as cate_name, employer.coname as employer_name')
                    ->leftJoin('jobcate', '`jobcate`.`id` = `job`.`jobcat_id`')
                    ->with('employer')
                    ->leftJoin('employer', '`employer`.`id` = `job`.`employer_id`')
                    ->with('jobCate')
                    ->where(['employer_id' => $id])
                    ->orderBy([
                        'created_at' => SORT_DESC
                    ])->asArray()
                    ->all();
                return $this->render('chi-tiet-nha-tuyen-dung', [
                    'employer' => $employer,
                    'jobs' => $jobs
                ]);
            } else {
                \Yii::$app->getResponse()->redirect('@web/viec-lam/nha-tuyen-dung');
            }
        } else {
            // Get all
            $employers = Employer::find()->all();
            return $this->render('nha-tuyen-dung', [
                'employers' => $employers
            ]);
        }
    }

    public function actionUngVien($id = null)
    {
        if ($id) {
            $employee = Employee::find()->where(['id' => $id])->one();
            return $this->render('chi-tiet-ung-vien', [
                'employee' => $employee
            ]);
        } else {
            // Get all
            $employees = Employee::find()->all();
            return $this->render('ung-vien', [
                'employees' => $employees
            ]);
        }
    }

    public function actionUngTuyen($id)
    {
        $job = Job::find()->where(['id' => intval($id)])->one();
        $session = Yii::$app->session;
        if ($session['employee']) {
            $job->apply_num++;
            if ($job->save(false)) {
                $this->sendEmailApplyJob($job['employer_id'], $session['employee']['id'], $job);
                return $this->render('ung-tuyen-thanh-cong');
            } else {
                return $this->render('ung-tuyen-that-bai', [
                    'job' => $job
                ]);
            }
        } else {
            return $this->render('ung-tuyen', [
                'job' => $job
            ]);
        }
    }

    public function actionUngTuyenThanhCong() {
        return $this->render('ung-tuyen-thanh-cong');
    }

    private function sendEmailApplyJob($employer_id, $employee_id, $job) {
        $employer = Employer::find()->where(['id' => $employer_id])->one();
        $employee = Employee::find()->where(['id' => $employee_id])->one();
        $cv = Cv::find()->where(['employee_id' => $employee->id])->one();
        $body = '';
        if ($cv) {
            $body = $cv->body;
        }
        return Yii::$app->mailer->compose()
            ->setTo($employer->email)
            ->setFrom([$employee->email => $employee->email])
            ->setSubject('Ứng viên mới ứng tuyển')
            ->setHtmlBody('<h1>Ứng viên <a href="' . Url::to('http://localhost:8080/hr/viec-lam/ung-vien/?id=' . $employee->id) . '">' . $employee->fullname . '</a></h1><div>' . $body . '</div><div>Ứng tuyển vào vị trí: <a href="' .
                Url::to('http://localhost:8080/hr/viec-lam/chi-tiet/?id=' . $job->id) . '">' . $job->title . '</a></div>')
            ->send();
    }

}

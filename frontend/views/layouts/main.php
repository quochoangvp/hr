<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Jobcate;
use common\models\Vg;

AppAsset::register($this);
$session = Yii::$app->session;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- style hrv in here -->
    <?= Html::cssFile('@web/css/style.css'); ?>
    <?= Html::cssFile('@web/css/log-hover.css'); ?>
    <?= Html::cssFile('@web/css/reset.css'); ?>
    <?= Html::cssFile('@web/packages/jquery/css/smoothness/jquery-ui-1.8.12.custom.css'); ?>
    <?= Html::cssFile('@web/packages/jquery_ui/development-bundle/themes/ui-lightness/jquery.ui.all.css'); ?>
    <?= Html::jsFile('@web/packages/jquery/js/jquery-latest.js'); ?>
    <?= Html::jsFile('@web/packages/jquery_ui/js/jquery-ui-1.8.14.custom.min.js'); ?>
    <!-- tinyMce- doanns -->
    <?= Html::jsFile('@web/packages/tinymce/tinymce.min.js'); ?>
    <script>
        tinymce.init({
            selector: ".tinymce",
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            },
            height: 500,
            plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [{
                title: 'Bold text',
                inline: 'b'
            }, {
                title: 'Red text',
                inline: 'span',
                styles: {
                    color: '#ff0000'
                }
            }, {
                title: 'Red header',
                block: 'h1',
                styles: {
                    color: '#ff0000'
                }
            }, {
                title: 'Example 1',
                inline: 'span',
                classes: 'example1'
            }, {
                title: 'Example 2',
                inline: 'span',
                classes: 'example2'
            }, {
                title: 'Table styles'
            }, {
                title: 'Table row 1',
                selector: 'tr',
                classes: 'tablerow1'
            }],
        });

    </script>
    <?= Html::jsFile('@web/js/common.js'); ?>
</head>
<body id="body">
<div id="body-bg">
    <div id="wrapper">
        <div id="header">
            <div id="hd-left">
                <div class="logo">
                    <?= Html::img(Url::to('@web/images/Logo.png'), ['width' => '160']) ?>
                </div>
                <div class="log-info">
                    <ul class="log-mega">
                        <li class="mega signup">
                            <a class="mega last signup" href="<?= Url::to('@web/site/tim-ho-so') ?>">
                                <span>Tìm hồ sơ</span>
                            </a>
                        </li>
                        <li class="mega signup">
                            <div>
                                <a class="mega signup" href="<?= Url::to('@web/site/nha-tuyen-dung') ?>">
                                    <span>Dành cho nhà tuyển dụng</span>
                                </a>
                            </div>

                        </li>
                        <div class="clear"></div>
                    </ul>
                    <div>
                        <div class="login-box">
                            <div id="sn-not-logged-in">
                                <?php if ($session['employer'] || $session['employee']): ?>
                                    <a href="<?= Url::to('@web/site/logout') ?>">Đăng xuất</a>
                                <?php else: ?>
                                    <a href="<?= Url::to('@web/site/dang-nhap') ?>">Đăng nhập</a>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="clear"></div>
                <div id="search-bn">
                    <div id="portlet_content_112831">
                        <form class="search" name="searchJob" method="get" action="<?= Url::to('@web/site/search') ?>">
                            <select name="category" class="type-Indus" style="width: 150px;">
                                <option value="">Tất cả</option>
                                <?php
                                $jobCates = JobCate::find()->select('id, name')->all();
                                foreach ($jobCates as $cate) {
                                    echo '<option value="' . $cate->id . '"';
                                    if (\Yii::$app->request->get('category') == $cate->id) {
                                        echo ' selected="selected"';
                                    }
                                    echo '>' . $cate->name . '</option>';
                                }
                                ?>
                            </select>
                            <input type="text" value="<?= \Yii::$app->request->get('keyword') ?>" class="inputbox"
                                   maxlength="64" name="keyword" placeholder="Nhập từ khóa tìm kiếm việc làm"/>
                            <ul class="log-search">
                                <li class="search signup">
                                    <a class="search last signup"
                                       onclick="doSearch('<?= Url::to('@web/site/search') ?>')"
                                       style="cursor: pointer;">
                                        <span>Tìm kiếm</span>
                                    </a>
                                </li>
                                <div class="clear"></div>
                            </ul>
                            <div class="clear"></div>
                        </form>
                    </div>

                </div>

                <div id="menunav">
                    <ul class="menu-tag">
                        <li><a href="<?= Url::to('@web/') ?>"><span>Trang chủ</span></a></li>
                        <li><a href="<?= Url::to('@web/site/gioi-thieu') ?>"><span>Giới thiệu</span></a></li>
                        <li><a href="<?= Url::to('@web/viec-lam') ?>"><span>Việc làm</span></a></li>
                        <li><a href="<?= Url::to('@web/viec-lam/nha-tuyen-dung') ?>"><span>Nhà tuyển dụng</span></a>
                        </li>
                        <li><a href="<?= Url::to('@web/viec-lam/ung-vien') ?>"><span>Ứng viên</span></a></li>
                        <li><a href="<?= Url::to('@web/huong-nghiep') ?>"><span>Hướng nghiệp</span></a></li>
                        <li><a class="" href="<?= Url::to('@web/site/lien-he') ?>"><span>Liên hệ</span></a></li>
                        <?php if ($session['employee']): ?>
                        <li><a class="active" href="<?= Url::to('@web/ung-vien/ho-so') ?>"><span>Hồ sơ (<?= $session['employee']['fullname'] ?>)</span></a></li>
                        <?php endif ?>
                        <?php if ($session['employer']): ?>
                        <li><a href="<?= Url::to('@web/nha-tuyen-dung/dang-viec') ?>"><span>Đăng việc</span></a></li>
                        <li><a href="<?= Url::to('@web/nha-tuyen-dung/ho-so') ?>"><span>Hồ sơ</span></a></li>
                        <?php endif ?>
                        <div class="clear"></div>
                    </ul>
                </div>
            </div>
        </div>
        <div id="main">
            <div id="main-page">
                <?= $content ?>
                <div class='clear'></div>
            </div>
            <div id="main-right">
                <div class="ldo-mod-blue">
                    <div class="title">
                        <h4><span>Hướng nghiệp</span></h4>
                    </div>
                    <div class="content">
                        <div id="portlet_content_113177">
                            <ul class="list-news bor-doted">
                                <?php
                                $vgs = Vg::find()->select('id, title')->all();
                                foreach ($vgs as $vg):
                                    ?>
                                    <li>
                                        <?= Html::a($vg->title, Url::to('@web/huong-nghiep/?id=' . $vg->id)) ?>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="footer">
            <div id="bottom">
                <div class="info-l-field f-left">
                    <p class="teamWork"
                       style=""><?= Html::img(Url::to('@web/images/teamwwork.png'), ['width' => '400']) ?></p>
                </div>
                <div class="info-r-field f-right">
                    <div class="mega-bot">
                        <ul>
                            <li><a href="<?= Url::to('@web') ?>">Trang chủ</a></li>
                            <li><a href="<?= Url::to('@web/site/gioi-thieu') ?>">Giới thiệu</a></li>
                            <li><a href="<?= Url::to('@web/site/viec-lam') ?>">Việc làm</a></li>
                            <li><a href="<?= Url::to('@web/viec-lam/nha-tuyen-dung') ?>">Nhà tuyển dụng</a></li>
                            <li><a href="<?= Url::to('@web/viec-lam/ung-vien') ?>">Ứng viên</a></li>
                            <li><a href="<?= Url::to('@web/huong-nghiep') ?>">Hướng nghiệp</a></li>
                            <li><a href="<?= Url::to('@web/site/lien-he') ?>">Liên hệ</a></li>
                            <div class="clear"></div>
                        </ul>
                    </div>
                    <div class="clear"></div>
                    <div class="info-comp">
                        <?= Html::img(Url::to('@web/images/Logo.png'), ['width' => '160']) ?>
                        <p><strong>CÔNG TY CỔ PHẦN GIẢI PHÁP NGUỒN NHÂN LỰC ABC XYZ</strong></p>

                        <p>Địa chỉ: 123 Tam Quan, Phố Huế, Hà Nội, Việt Nam</p>

                        <p>Điện thoại: +84 - 4 3 2392 923</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="rounded-r">
                <div class="rounded-l"></div>
            </div>
            <div style="line-height:30px; margin:0 5px; padding-left:10px; color:#777; text-align:left !important;">
                <p><strong style="text-align:left !important;">Copyright © <?= date('Y') ?> ComTube</strong></p>
            </div>
</body>
</html>

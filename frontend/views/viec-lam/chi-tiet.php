<?php

use yii\helpers\Url;

$this->title = $job['title'];
?>
<div class="box-grid">
  <p class="title-sin">
    <a href="javascript:;">Thông tin doanh nghiệp</a>
  </p>
  <div class="grid-com-hd">
    <div class="infoComSigup">
     <div class="infoCom">
      <p>
        <strong class="f-left a-right">Công ty:</strong>
        <span class="f-right txtname">
          <a href="javascript:;"><?= $job['coname'] ?></a>
        </span>
      </p>
      <div class="clear">
      </div>
      <p>
      </p>
      <p>
        <strong class="f-left a-right">Địa chỉ:</strong>
        <span class="f-right"><?= $job['coaddress'] ?></span>
      </p>
      <div class="clear">
      </div>
      <p>
      </p>
      <p>
        <strong class="f-left a-right">Liên hệ:</strong>
        <span class="f-right"><?= $job['cocontact'] ?></span>
      </p>
      <div class="clear">
      </div>
      <p>
      </p>
      <p>
        <strong class="f-left a-right">Giới thiệu:</strong>
        <div class="f-right">
          <?= nl2br($job['codetails']) ?>
        </div>
      </p>
      <div class="clear">
      </div>
      <p>
      </p>
    </div>
    <div class="clear">
    </div>
  </div>
</div>
<p class="title-sin bor-org">
  <a class="none-href">Chi tiết công việc</a>
</p>
<div class="grid-info">
  <ul>
    <li>
      <p><strong>Ngành nghề:</strong> <?= $job['cate_name'] ?></p>
    </li>
    <li>
      <?= html_entity_decode($job['body']) ?>
    </li>
    <li>
      <p><strong>Hạn nộp hồ sơ:</strong> <?= $job['deadline'] ?></p>
    </li>
  </ul>
</div>
<div class="events-log">
  <ul class="event-mega">
    <li>
      <a class="btn-event" href="<?= Url::to('@web/viec-lam/ung-tuyen/?id=' . $job['id']) ?>">Ứng tuyển</a>
    </li>
  </ul>
</div>
</div>

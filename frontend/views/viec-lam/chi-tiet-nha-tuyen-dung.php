<?php

use yii\helpers\BaseStringHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Thông tin nhà tuyển dụng: ' . $employer->coname;
?>

<div class="box-grid">
	<div class="tabs-content">
        <ul class="mega-tabs">
            <li class="addicon"><a class="active"><span><?= $employer->coname ?></span></a></li>
            <div class="clear"></div>
        </ul>
    </div>
    <div class="grid-info">
    	<div class="infoComSigup">
            <div class="infoCom">
            	<p>
                	<strong class="f-left a-right">Tên nhà tuyển dụng:</strong>
                    <span class="f-right txtname"><?= $employer->coname ?></span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                	<strong class="f-left a-right">Địa chỉ:</strong>
                    <span class="f-right"><?= $employer->coaddress ?></span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                	<strong class="f-left a-right">Liên hệ:</strong>
                    <span class="f-right"><?= $employer->cocontact ?></span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                	<strong class="f-left a-right">Website:</strong>
                    <span class="f-right"><?= $employer->cowebsite ?></span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                	<strong class="f-left a-right">Giới thiệu:</strong>
                    <span class="f-right">
                        <?= nl2br($employer->codetails) ?>
                    </span>
                    </p><div class="clear"></div>
                <p></p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="tabs-content">
        <ul class="mega-tabs">
            <li class="addicon"><a class="active"><span>Tin tuyển dụng</span></a></li>
            <div class="clear"></div>
        </ul>
    </div>
    <div class="grid-info">
    	<ul class="list-job-table">
            <li class="title-table">
            	<p class="positions f-left" style="width: 85%!important">Chức danh / Vị trí</p>
                <p class="datetime f-left">Hạn chót</p>
                <div class="clear"></div>
        	</li>
            <?php foreach ($jobs as $job): ?>
        	<li>
            	<p class="positions f-left" style="width: 85%!important">
                    <?= Html::a($job['title'], Url::to('@web/viec-lam/chi-tiet/?id=' . $job['id'])) ?>
                	<br>
                    <small><?= BaseStringHelper::byteSubstr(strip_tags(html_entity_decode($job['body'])), 0, 160) ?></small>
                </p>
                <p class="datetime f-left"><?= date_format(date_create($job['deadline']), 'd-m-Y') ?></p>
                <div class="clear"></div>
        	</li>
            <?php endforeach ?>
        </ul>
    </div>
</div>
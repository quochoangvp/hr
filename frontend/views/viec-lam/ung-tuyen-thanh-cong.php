<?php

$this->title = 'Ứng tuyển thành công';
?>

<h1><?= $this->title ?></h1>
<p style="margin-top: 12px">
    Bạn đã ứng tuyển thành công! Hãy tiếp tục xem những công việc khác phù hợp với mình.
</p>
<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Danh sách các nhà tuyển dụng';
?>

<div class="employee-register">
	<h1>Danh sách nhà tuyển dụng</h1>
	<table class="resultTable">
		<thead>
			<tr class="title-table" style="background: none repeat scroll 0 0 #F7E6E6;">
				<th style="text-align: center;">STT</th>
				<th>Logo</th>
				<th>Tên nhà tuyển dụng</th>
				<th>Địa chỉ</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($employers as $key => $employer): ?>
			<tr>
				<td style="text-align: center;"><?= $key+1 ?></td>
				<td>
					<a href="<?= Url::to('@web/viec-lam/nha-tuyen-dung/?id=' . $employer->id) ?>">
						<?= Html::img('@web/images/uploads/' . $employer->cologo, ['width' => '50']); ?>
					</a>
				</td>
				<td>
					<?= Html::a($employer->coname, Url::to('@web/viec-lam/nha-tuyen-dung/?id=' . $employer->id)) ?>
				</td>
				<td><?= $employer->coaddress ?></td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
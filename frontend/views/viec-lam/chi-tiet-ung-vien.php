<?php

$this->title = 'Thông tin ứng viên: ' . $employee->fullname;
?>

<div class="box-grid">
	<div class="tabs-content">
        <ul class="mega-tabs">
            <li class="addicon"><a class="active"><span><?= $employee->fullname ?></span></a></li>
            <div class="clear"></div>
        </ul>
    </div>
    <div class="grid-info">
    	<div class="infoComSigup">
            <div class="infoCom">
            	<p>
                	<strong class="f-left a-right">Họ tên:</strong>
                    <span class="f-right txtname"><?= $employee->fullname ?></span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                	<strong class="f-left a-right">Giới tính:</strong>
                    <span class="f-right"><?= ($employee->gender === 1) ? 'Nam' : (($employee->gender === 2) ? 'Nữ' : 'Khác') ?></span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                	<strong class="f-left a-right">Email:</strong>
                    <span class="f-right"><?= $employee->email ?></span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                	<strong class="f-left a-right">Ngày sinh:</strong>
                    <span class="f-right"><?= $employee->birthday ?></span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                    <strong class="f-left a-right">Số điện thoại:</strong>
                    <span class="f-right">
                        <?= $employee->phone ?>
                    </span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                	<strong class="f-left a-right">Địa chỉ:</strong>
                    <span class="f-right">
                        <?= $employee->address ?>
                    </span>
                    </p><div class="clear"></div>
                <p></p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = 'Ứng tuyển vào vị trí ' . $job['title'];
?>

<div class="employee-register">
    <h1>Đăng nhập để tiếp tục</h1>
    <form method="post" name="frmLogin" id="employeeLoginForm" action="<?= Url::to('@web/site/do-employee-login') ?>">
        <div class="tb-register">
            <div class="err_register" style="display: none"></div>
            <div class="lb-right">
                <label>Địa chỉ email </label>
            </div>
            <div class="lb-value">
                <input class="ip-text email required" required="" type="email" name="email" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Mật khẩu </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required" type="password" required="" name="password" value="">
            </div>
            <div class="clear"></div>
            <div style="width: 99%; text-align: center;">
                (*) Nếu bạn chưa có tài khoản, hãy <a href="<?= Url::to('@web/site/dang-ky') ?>">đăng ký</a> tài khoản ứng viên.
                <br> Nếu bạn quên thông tin tài khoản của mình, vui lòng vào <a href="<?= Url::to('@web/site/request-password-reset') ?>">đây</a> để lấy lại.
            </div>
            <div class="lb-right">&nbsp;&nbsp;</div>
            <div class="lb-value">
                <input name="job_id" value="<?= $job['id'] ?>" type="hidden" />
                <input class="bt-submit bt-register" type="submit" value="Đăng nhập" name="btLogin"> &nbsp;
            </div>
        </div>
    </form>
</div>
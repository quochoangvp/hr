<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Danh sách các ứng viên';
?>

<div class="employee-register">
	<h1>Danh sách ứng viên</h1>
	<table class="resultTable">
		<thead>
			<tr class="title-table" style="background: none repeat scroll 0 0 #F7E6E6;">
				<th style="text-align: center;">STT</th>
				<th>Họ tên</th>
				<th>Giới tính</th>
				<th>Ngày sinh</th>
				<th>Địa chỉ</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($employees as $key => $employee): ?>
			<tr>
				<td style="text-align: center;"><?= $key+1 ?></td>
				<td>
					<?= Html::a($employee->fullname, Url::to('@web/viec-lam/ung-vien/?id=' . $employee->id)) ?>
				</td>
				<td>
					<?= ($employee->gender === 1) ? 'Nam' : (($employee->gender === 2) ? 'Nữ' : 'Khác') ?>
				</td>
				<td><?= date_format(date_create($employee->birthday), 'd/m/Y') ?></td>
				<td><?= $employee->address ?></td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<?php

use yii\helpers\BaseStringHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Việc làm của ngành: ' . $jobCate->name;
?>

<div class="box-grid">
	<div class="tabs-content">
		<ul class="mega-tabs">
			<li class="addicon"><a class="active"><span><?= $jobCate->name ?></span></a></li>
			<div class="clear"></div>
		</ul>
	</div>
	<div class="grid-info">
		<ul class="list-job-table">
			<li class="title-table">
				<p class="positions f-left">Chức danh / Vị trí</p>
				<p class="company f-left">Tổ chức / Công ty</p>
				<p class="datetime f-left">Hạn chót</p>
				<div class="clear"></div>
			</li>
			<?php foreach ($jobs as $job): ?>
				<li>
					<p class="positions f-left">
						<?= Html::a($job['title'], Url::to('@web/viec-lam/?id=' . $job['id'])) ?>
						<br>
						<small><?= BaseStringHelper::byteSubstr(strip_tags(html_entity_decode($job['body'])), 0, 160) ?></small>
					</p>
					<p class="company f-left">
            <?= Html::a($job['employer_name'], Url::to('@web/viec-lam/nha-tuyen-dung/?id=' . $job['employer_id'])) ?>
          </p>
					<p class="datetime f-left"><?= date_format(date_create($job['deadline']), 'd-m-Y') ?></p>
					<div class="clear"></div>
				</li>
			<?php endforeach ?>
		</ul>
	</div>
</div>
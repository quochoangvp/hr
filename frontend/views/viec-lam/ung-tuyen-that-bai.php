<?php

use yii\helpers\Url;
$this->title = 'Ứng tuyển thất bại';
?>

<h1><?= $this->title ?></h1>
<p style="margin-top: 12px">
    Lỗi hệ thống, không thể ứng tuyển, hãy <a href="<?= Url::to('@web/viec-lam/chi-tiet/?id=' . $job['id']) ?>">quay lại</a> và ứng tuyển lần nữa.
</p>
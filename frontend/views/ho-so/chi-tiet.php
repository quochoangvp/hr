<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Chi tiết hồ sơ: ' . $cv['title'];
?>

<div class="box-grid">
	<div class="tabs-content">
        <ul class="mega-tabs">
            <li class="addicon"><a class="active"><span><?= $cv['title'] ?></span></a></li>
            <div class="clear"></div>
        </ul>
    </div>
    <div class="grid-info">
    	<div class="infoComSigup">
            <div class="infoCom">
            	<p>
                	<strong class="f-left a-right">Tiêu đề:</strong>
                    <span class="f-right txtname"><?= $cv['title'] ?></span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                	<strong class="f-left a-right">Lượt xem:</strong>
                    <span class="f-right"><?= $cv['countview'] ?></span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                	<strong class="f-left a-right">Ngành nghề:</strong>
                    <span class="f-right">
                        <?= Html::a($cv['cate_name'], Url::to('@web/viec-lam/theo-nganh-nghe/?id=' . $cv['jobcat_id'])) ?>
                    </span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                	<strong class="f-left a-right">Ứng viên:</strong>
                    <span class="f-right">
                        <?= Html::a($cv['employee_name'], Url::to('@web/viec-lam/ung-vien/?id=' . $cv['employee_id'])) ?>
                    </span>
                    </p><div class="clear"></div>
                <p></p>
                <p>
                	<strong class="f-left a-right">Nội dung:</strong>
                    <div class="f-right">
                        <?= nl2br($cv['body']) ?>
                    </div>
                    </p><div class="clear"></div>
                <p></p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Danh sách hồ sơ';
?>

<div class="employee-register">
	<h1>Danh sách hồ sơ</h1>
	<div class="box-grid">
		<div class="grid-info" style="border-width: 1px; float: left; width: 97%;">
			<table class="resultTable">
				<thead>
					<tr class="title-table">
						<th style="text-align: center;">STT</th>
						<th>Tiêu đề</th>
						<th>Ứng viên</th>
						<th>Ngành nghề</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($cvs as $key => $cv): ?>
						<tr>
							<td style="text-align: center;"><?= $key+1 ?></td>
							<td><?= Html::a($cv['title'], Url::to('@web/ho-so/chi-tiet/?id=' . $cv['id'])) ?></td>
							<td><?= Html::a($cv['employee_name'], Url::to('@web/viec-lam/ung-vien/?id=' . $cv['employee_id'])) ?></td>
							<td><?= Html::a($cv['cate_name'], Url::to('@web/viec-lam/theo-nganh-nghe/?id=' . $cv['jobcat_id'])) ?></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
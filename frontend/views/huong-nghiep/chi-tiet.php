<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $vg->title;
?>

<p class="title-sin bor-org">
	<span class="news-path">
		<a style="background: none; padding-left: 0;" href="<?= Url::to('@web/huong-nghiep') ?>">Hướng nghiệp</a>
	</span>
</p>
<div class="grid-info">
	<div class="view-single">
		<h5>
			<a href=""><?= $vg->title ?></a>
			<br /><span class="date"><br>(<?= $vg->created_at ?>)</span>
		</h5>
	</div>
	<div class="view-content">
		<?= $vg->body ?>
	</div>
</div>
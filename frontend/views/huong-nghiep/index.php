<?php

use yii\helpers\Url;
use yii\helpers\BaseStringHelper;

$this->title = 'Tin hướng nghiệp';
?>
<p class="title-sin bor-org">
	<span class="news-path">
		<a style="background: none; padding-left: 0;" href="<?= Url::to('@web/huong-nghiep') ?>">Hướng nghiệp</a>
	</span>
</p>
<div class="grid-info">
	<div class="content">
		<?php foreach ($vgs as $vg): ?>
		<div class="view-single">
			<h2><a href="<?= Url::to('@web/huong-nghiep/?id=' . $vg->id) ?>"><?= $vg->title ?></a></h2>
			<label><?= $vg->created_at ?></label>
			<p style="text-align: justify;">
				<?= BaseStringHelper::byteSubstr(strip_tags($vg->body), 0, 240) ?>
			</p>
			<div class="clear"></div>
		</div>
		<?php endforeach ?>
	</div>
</div>
<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Đăng ký thông tin doanh nghiệp';
?>
<div class="employer-register">
    <h1><?= $this->title ?></h1>
    <form
        method="post"
        name="frmRegister"
        id="employerRegisterForm"
        action="<?= Url::to('@web/site/do-employer-register') ?>"
        enctype="multipart/form-data"
    >
        <div class="tb-register">
            <div class="err_register" style="display: none"></div>
            <div class="lb-right">
                <label>Địa chỉ email </label>
            </div>
            <div class="lb-value">
                <input class="ip-text email required" required="" type="email" name="Employer[email]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Mật khẩu </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required" type="password" required="" name="Employer[password]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Tên công ty </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required" type="text" required="" name="Employer[coname]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Logo công ty </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required" type="file" required="" name="Employer[cologo]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Địa chỉ công ty </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required" type="text" required="" name="Employer[coaddress]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Website công ty </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required" type="text" required="" name="Employer[cowebsite]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Liên hệ </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required" type="text" required="" name="Employer[cocontact]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Thông tin cty </label>
            </div>
            <div class="lb-value">
                <textarea class="ip-text required tinymce" name="Employer[codetails]"></textarea>
            </div>
            <div class="clear"></div>
            <div class="lb-right">&nbsp;&nbsp;</div>
            <div class="lb-value">
                <input class="bt-submit bt-register" type="submit" value="Đăng ký" name="btLogin"> &nbsp;
            </div>
        </div>
    </form>
</div>
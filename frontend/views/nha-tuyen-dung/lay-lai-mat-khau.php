<?php

use yii\helpers\Url;

$this->title = 'Lấy lại mật khẩu';
?>
<div class="employee-register">
    <h1>Nhà tuyển dụng lấy lại mật khẩu.</h1>

    <form method="post" name="frmEmpRegister">

        <div class="tb-register">
            <?php if (isset($message) && strlen($message) > 0): ?>
                <?php if ($status): ?>
                    <div class="success_register"><?= $message ?></div>
                <?php else: ?>
                    <div class="err_register"><?= $message ?></div>
                <?php endif ?>
            <?php endif ?>
            <div class="lb-right">
                <label>Email </label><label class="required">(*)</label>
            </div>
            <div class="lb-value">
                <input class="ip-text" type="email" name="email" value="" required="">
                <label class="lb-note">Bạn hãy điền chính xác email đã đăng ký! </label>
                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
            </div>
            <div class="lb-right">&nbsp;&nbsp;</div>
            <div class="lb-value">
                <input class="bt-submit bt-register" type="submit" value="Lấy lại mật khẩu" name="btLostpass" onclick="return checkEmployeeLostPass();"> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                <a href="<?= Url::to('@web/site/nha-tuyen-dung') ?>">Đăng nhập</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                <a href="<?= Url::to('@web/nha-tuyen-dung/dang-ky') ?>">Đăng ký</a>
            </div>
        </div>
    </form>
</div>
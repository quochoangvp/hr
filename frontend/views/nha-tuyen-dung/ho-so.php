<?php

$this->title = 'Hồ sơ';

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="box-grid">
    <p class="title-sin bor-org"><a class="contact_a" href=""><?= $this->title ?></a></p>
    <div class="grid-info contact">
        <div style="clear:both; height:15px"></div>
        <?php
            if (isset($success)) {
                echo '<div class="success_register">' . $success . '</div>';
            } elseif (isset($error)) {
                echo '<div class="err_register">' . $error . '</div>';
            }
        ?>
        <form action="<?= Url::to('@web/nha-tuyen-dung/ho-so') ?>" method="post" name="editProfile" id="editProfile" enctype="multipart/form-data">
            <div style="margin-bottom: 5px;">Địa chỉ email (*): </div>
            <div>
                <input name="Employer[email]" type="email" size="50" maxlength="50" class="adword-textbox" value="<?php echo $employer->email ?>" required="">
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="margin-bottom: 5px;">Mật khẩu (*): </div>
            <div>
                <input name="Employer[password]" type="password" size="50" maxlength="50" class="adword-textbox" value="" required="">
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="margin-bottom: 5px;">Tên công ty (*): </div>
            <div>
                <input name="Employer[coname]" type="text" size="50" maxlength="50" class="adword-textbox" value="<?php echo $employer->coname ?>" required="">
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="margin-bottom: 5px;">Logo (*): </div>
            <div>
                <?php if (strlen($employer->cologo)>0): ?>
                    <img src="<?php echo Url::to('@web/images/uploads/' . $employer->cologo) ?>" alt="" width="80px">
                <?php endif ?>
                <input name="Employer[cologo]" type="file" size="50" maxlength="50" class="adword-textbox">
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="margin-bottom: 5px;">Địa chỉ công ty (*): </div>
            <div>
                <input name="Employer[coaddress]" type="text" size="50" maxlength="50" class="adword-textbox" value="<?php echo $employer->coaddress ?>" required="">
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="margin-bottom: 5px;">Website công ty (*): </div>
            <div>
                <input name="Employer[cowebsite]" type="url" size="50" maxlength="50" class="adword-textbox" value="<?php echo $employer->cowebsite ?>" required="">
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="margin-bottom: 5px;">Liên hệ (*): </div>
            <div>
                <input name="Employer[cocontact]" type="text" size="50" maxlength="50" class="adword-textbox" value="<?php echo $employer->cocontact ?>" required="">
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="margin-bottom: 5px;">Thông tin về công ty (*): </div>
            <div>
                <textarea name="Employer[codetails]" rows="12" class="tinymce"><?php echo $employer->codetails ?></textarea>
            </div>
            <div style="clear:both; height:10px"></div>
            <div>
                <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>">
                <?= Html::submitButton('Gửi', ['class' => 'bt-register']) ?>
            </div>
        </form>
    </div>
</div>
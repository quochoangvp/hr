<?php

$this->title = 'Đăng việc làm mới';

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="box-grid">
    <p class="title-sin bor-org"><a class="contact_a" href=""><?= $this->title ?></a></p>
    <div class="grid-info contact">
        <div style="clear:both; height:15px"></div>
        <div class="err_register" style="display: none"></div>
        <div class="success_register" style="display: none"></div>
        <form action="<?= Url::to('@web/nha-tuyen-dung/gui-viec-moi') ?>" method="post" name="addNewJob" id="addNewJob">
            <div style="margin-bottom: 5px;">Chức danh/Vị trí: </div>
            <div>
                <input name="title" type="text" size="50" maxlength="50" class="adword-textbox" value="" required="">
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="margin-bottom: 5px;">Hạn nộp hồ sơ (*): </div>
            <div>
                <input name="deadline" type="text" size="50" maxlength="50" class="adword-textbox datepicker" required="">
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="margin-bottom: 5px;">Chuyên ngành (*): </div>
            <div>
                <select name="jobcat_id" class="adword-select" required="">
                    <option value="">Chọn chuyên ngành</option>
                    <?php foreach($jobCats as $cat): ?>
                        <option value="<?= $cat['id'] ?>"><?= $cat['name'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="margin-bottom: 5px;">Nội dung (*): </div>
            <div>
                <textarea name="body" rows="12" class="tinymce"></textarea>
            </div>
            <div style="clear:both; height:10px"></div>
            <div>
                <?= Html::submitButton('Gửi', ['class' => 'bt-register', 'name' => 'contact-button']) ?>
            </div>
        </form>
    </div>
</div>
<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Hồ sơ ứng viên ' . $employee['fullname'];
?>

<div class="box-grid">
    <div class="tabs-content">
        <ul class="mega-tabs">
            <li class="addicon"><a class="active"><span>Thông tin ứng viên</span></a></li>
            <div class="clear"></div>
        </ul>
    </div>
    <div class="grid-info">
        <div class="infoComSigup">
            <div class="infoCom">
                <p>
                    <strong class="f-left a-right">Họ tên:</strong>
                    <span class="f-right txtname"><?= $employee['fullname'] ?></span>
                </p><div class="clear"></div>
                <p></p>
                <p>
                    <strong class="f-left a-right">Giới tính:</strong>
                    <span class="f-right"><?= ($employee['gender']==1)?'Nam':(($employee['gender']==2)?'Nữ':'Khác') ?></span>
                </p><div class="clear"></div>
                <p></p>
                <p>
                    <strong class="f-left a-right">Email:</strong>
                    <span class="f-right"><?= $employee['email'] ?></span>
                </p><div class="clear"></div>
                <p></p>
                <p>
                    <strong class="f-left a-right">Ngày sinh:</strong>
                    <span class="f-right"><?= $employee['birthday'] ?></span>
                </p><div class="clear"></div>
                <p></p>
                <p>
                    <strong class="f-left a-right">Số điện thoại:</strong>
                    <span class="f-right"><?= $employee['phone'] ?></span>
                </p><div class="clear"></div>
                <p></p>
                <p>
                    <strong class="f-left a-right">Địa chỉ:</strong>
                    <span class="f-right"><?= $employee['address'] ?></span>
                </p><div class="clear"></div>
                <p></p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<div class="box-grid">
    <div class="tabs-content">
        <ul class="mega-tabs">
            <li class="addicon"><a class="active"><span>Hồ sơ</span></a></li>
            <div class="clear"></div>
        </ul>
    </div>
    <div class="grid-info">
        <div class="infoComSigup">
            <div class="infoCom">
                <p>
                    <strong class="f-left a-right">Tiêu đề:</strong>
                    <span class="f-right"><?= $cv['title'] ?></span>
                </p><div class="clear"></div>
                <p></p>
                <p>
                    <strong class="f-left a-right">Ngành nghề:</strong>
                    <span class="f-right"><?= \common\models\Jobcate::find()->where(['id' => $cv['jobcat_id']])->one()->name ?></span>
                </p><div class="clear"></div>
                <p></p>
                <p>
                    <strong class="f-left a-right">Lượt xem:</strong>
                    <span class="f-right"><?= $cv['countview'] ?></span>
                </p><div class="clear"></div>
                <p></p>
                <p>
                    <strong class="f-left a-right">Tệp đính kèm:</strong>
                    <span class="f-right"><a href="<?= Url::to('@web/uploads/cvs/' . $cv['attachments']) ?>">Tải xuống</a></span>
                </p><div class="clear"></div>
                <p></p>
                <p>
                    <strong class="f-left a-right">Chi tiết:</strong>
                    <div class="f-right"><?= $cv['body'] ?></div>
                </p><div class="clear"></div>
                <p></p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Liên hệ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box-grid">
    <p class="title-sin">
        <a class="contact_a">Thông tin liên hệ</a>
    </p>
    <div class="grid-com-hd">
        <div class="infoComSigup">
            <p>Chào mừng bạn đến với Tìm việc làm<br>
            <br>
            Bạn có thắc mắc cần Tìm việc làm giải đáp, bạn có thông tin cần cung cấp, hoặc chỉ đơn giản là bạn có băn khoăn muốn chia sẻ với chúng tôi, xin sử dụng form dưới đây và gửi nội dung cho chúng tôi. Xin chân thành cảm ơn bạn!</p>
        </div>
    </div>
    <p class="title-sin bor-org"><a class="contact_a" href="">Liên hệ</a></p>
    <div class="grid-info contact">
        <?php
            if (Yii::$app->session->getFlash('error')) {
                echo Yii::$app->session->getFlash('error');
            } else {
                echo Yii::$app->session->getFlash('success');
            }
        ?>
        <div style="clear:both; height:15px"></div>
        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
            <div style="width:25%; text-align:right; float:left; margin-right: 5px;">Họ và tên (*): </div>
            <div style="width:60%; float:left">
                <input name="ContactForm[name]" type="text" size="50" maxlength="50" class="adword-textbox">
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="width:25%; text-align:right; float:left; margin-right: 5px;">Địa chỉ email (*): </div>
            <div style="width:60%; float:left">
                <input name="ContactForm[email]" type="email" size="50" maxlength="50" class="adword-textbox">
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="width:25%; text-align:right; float:left; margin-right: 5px;">Nội dung (*): </div>
            <div style="width:60%; float:left">
                <textarea name="ContactForm[body]" rows="5" cols="38" class="SForm"></textarea>
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="width:100%; float:left"></div>
            <div style="width:25%; text-align:right; float:left; margin-right: 5px;">&nbsp; </div>
            <div style="width:60%;">
                <?= Html::submitButton('Gửi', ['class' => 'bt-register', 'name' => 'contact-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
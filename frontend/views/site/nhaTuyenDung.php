<?php

/* @var $this yii\web\View */
use yii\helpers\BaseStringHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Danh sách việc làm';
$session = Yii::$app->session;
?>
<div id="portlet_content_113887">
    <div id="Banner">
        <div class="viewCityHead">
            <div class="menu_all">
                <span class="menu-title"><?= $session['employer']['coname'] ?></span>
            </div>
            <div id="dialog-overlay"></div>
        </div>
    </div>
</div>
<div id="portlet_content_112835">
    <div class="grid-info" style="margin-top: 30px;">
        <ul class="list-job-table">
            <li class="title-table">
                <p class="positions f-left">Chức danh / Vị trí</p>
                <p class="datetime f-left">Hạn chót</p>
                <p class="datetime f-right">#</p>
                <div class="clear"></div>
            </li>
            <?php foreach ($jobs as $job): ?>
                <li>
                    <p class="positions f-left">
                        <?= Html::a($job['title'], Url::to('@web/viec-lam/chi-tiet/?id=' . $job['id'])) ?><br />
                        <small><?= BaseStringHelper::byteSubstr(strip_tags(html_entity_decode($job['body'])), 0, 160) ?></small>
                    </p>
                    <p class="datetime f-left"><?= date_format(date_create($job['deadline']), 'd-m-Y') ?></p>
                    <p class="datetime f-right">
                        <a href="<?= Url::to('@web/nha-tuyen-dung/sua-viec-lam/?id=' . $job['id']) ?>">Sửa</a>
                    </p>
                    <div class="clear"></div>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
</div>
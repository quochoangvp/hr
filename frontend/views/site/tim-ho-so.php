<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Tìm hồ sơ';
?>

<div class="employee-register">
	<h1>Tìm hồ sơ</h1>
	<form method="get" name="frmRegister" id="frmRegister" action="<?= Url::to('@web/site/tim-ho-so') ?>">
		<div class="tb-register">
			<div class="lb-right">
				<label>Từ khóa: </label>
			</div>
			<div class="lb-value">
				<input class="ip-text" type="text" name="keyword" value="<?= \Yii::$app->request->get('keyword') ?>">
			</div>
			<div class="clear"></div>
			<div class="lb-right">&nbsp;&nbsp;</div>
			<div class="lb-value">
				<input class="bt-submit bt-register" type="submit" value="Tìm kiếm">
			</div>
		</div>
	</form>
</div>

<?php if (strlen(\Yii::$app->request->get('keyword')) !== 0): ?>
<div class="employee-register" style="margin-top: 12px;">
	<h1>Kết quả tìm kiếm</h1>
	<?php if (count($cvs) === 0): ?>
	<div class="box-grid">
		<p style="padding-left: 10px;">Không tìm thấy kết quả nào phù hợp với yêu cầu của bạn</p>
	</div>
	<?php else: ?>
	<div class="box-grid">
		<p style="padding-left: 10px;">Tìm thấy <strong><?= count($cvs) ?></strong> hồ sơ phù hợp với yêu cầu tìm kiếm</p>
		<div class="grid-info" style="border-width: 1px; float: left; width: 97%;">
			<table class="resultTable">
				<thead>
					<tr class="title-table">
						<th style="text-align: center;">STT</th>
						<th>Tiêu đề</th>
						<th>Ứng viên</th>
						<th>Ngành nghề</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($cvs as $key => $cv): ?>
						<tr>
							<td style="text-align: center;"><?= $key+1 ?></td>
							<td><?= Html::a($cv['title'], Url::to('@web/ho-so/chi-tiet/?id=' . $cv['id'])) ?></td>
							<td><?= Html::a($cv['employee_name'], Url::to('@web/viec-lam/ung-vien/?id=' . $cv['employee_id'])) ?></td>
							<td><?= Html::a($cv['cate_name'], Url::to('@web/viec-lam/theo-nganh-nghe/?id=' . $cv['jobcat_id'])) ?></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
	<?php endif ?>
</div>
<?php endif ?>
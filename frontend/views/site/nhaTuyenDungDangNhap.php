<?php

$this->title = 'Nhà tuyển dụng đăng nhập';

use yii\helpers\Url;

?>

<div class="employee-register">
    <h1>Đăng nhập hệ thống</h1>
    <form method="post" name="frmLogin" id="employerLoginForm" action="<?= Url::to('@web/site/do-employer-login') ?>">
        <div class="tb-register">
            <div class="err_register" style="display: none"></div>
            <div class="lb-right">
                <label>Địa chỉ email </label>
            </div>
            <div class="lb-value">
                <input class="ip-text email required" required="" type="email" name="email" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Mật khẩu </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required" type="password" required="" name="password" value="">
            </div>
            <div class="clear"></div>
            <div style="width: 99%; text-align: center;">
                (*) Nếu bạn chưa có tài khoản, hãy <a href="<?= Url::to('@web/nha-tuyen-dung/dang-ky') ?>">đăng ký thông tin doanh nghiệp</a> của bạn.
                <br> Nếu bạn quên thông tin tài khoản của mình, vui lòng vào <a href="<?= Url::to('@web/nha-tuyen-dung/lay-lai-mat-khau') ?>">đây</a> để lấy lại.
            </div>
            <div class="lb-right">&nbsp;&nbsp;</div>
            <div class="lb-value">
                <input class="bt-submit bt-register" type="submit" value="Đăng nhập" name="btLogin"> &nbsp;
            </div>
        </div>
    </form>
</div>
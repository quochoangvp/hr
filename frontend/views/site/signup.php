<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Đăng ký';
?>

<div class="employee-register">
    <h1>Ứng viên đăng ký</h1>
    <?php $forms = ActiveForm::begin(['id' => 'form-signup']); ?>
        <div class="tb-register">
            <?php if (isset($message)): ?>
                <div class="err_register"><?= $message ?></div>
            <?php endif ?>
            <div class="lb-right">
                <label>Họ tên </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required" required="" type="text" name="Employee[fullname]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Giới tính </label>
            </div>
            <div class="lb-value">
                <select name="Employee[gender]" class="adword-select required" required="">
                    <option value="1">Nam</option>
                    <option value="2">Nữ</option>
                    <option value="3">Khác</option>
                </select>
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Ngày sinh </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required datepicker" required="" type="text" name="Employee[birthday]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Số điện thoại </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required" required="" type="text" name="Employee[phone]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Địa chỉ </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required" required="" type="text" name="Employee[address]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Địa chỉ email </label>
            </div>
            <div class="lb-value">
                <input class="ip-text email required" required="" type="email" name="Employee[email]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">
                <label>Mật khẩu </label>
            </div>
            <div class="lb-value">
                <input class="ip-text required" type="password" required="" name="Employee[password]" value="">
            </div>
            <div class="clear"></div>
            <div class="lb-right">&nbsp;&nbsp;</div>
            <div class="lb-value">
                <input class="bt-submit bt-register" type="submit" value="Đăng ký" name="btLogin"> &nbsp;
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
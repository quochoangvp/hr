<?php

/* @var $this yii\web\View */
use yii\helpers\BaseStringHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Tìm việc làm';
?>
<div id="portlet_content_113887">
  <div id="Banner">
    <div class="viewCityHead">
      <div class="menu_all">
        <span class="menu-title">Việc làm mới</span>
      </div>
      <div id="dialog-overlay"></div>
    </div>
  </div>
</div>
<div id="portlet_content_112835">
  <div class="grid-info" style="margin-top: 30px;">
    <ul class="list-job-table">
      <li class="title-table">
        <p class="positions f-left">Chức danh / Vị trí</p>
        <p class="company f-left">Tổ chức / Công ty</p>
        <p class="datetime f-left">Hạn chót</p>
        <div class="clear"></div>
      </li>
      <?php foreach ($jobs as $job): ?>
        <li>
          <p class="positions f-left">
            <?= Html::a($job['title'], Url::to('@web/viec-lam/chi-tiet/?id=' . $job['id'])) ?><br />
            <small><?= BaseStringHelper::byteSubstr(strip_tags(html_entity_decode($job['body'])), 0, 160) ?></small>
          </p>
          <p class="company f-left">
            <?= Html::a($job['employer_name'], Url::to('@web/viec-lam/nha-tuyen-dung/?id=' . $job['employer_id'])) ?>
          </p>
          <p class="datetime f-left"><?= date_format(date_create($job['deadline']), 'd-m-Y') ?></p>
          <div class="clear"></div>
        </li>
      <?php endforeach ?>
    </ul>
  </div>
</div>

<div class="clear"></div>
<div id="cat-field">
  <div id="portlet_content_113611">
    <p class="title-sin bor-org"><a class="nolink">Lĩnh vực</a></p>
    <div class="grid-info-hqd">
      <?php foreach ($jobCates as $cate): ?>
        <div class="cat-list-home">
          <?= Html::a($cate->name, Url::to('@web/viec-lam/theo-nganh-nghe/?id=' . $cate->id)) ?>
        </div>
      <?php endforeach ?>
    </div>
  </div>

</div>
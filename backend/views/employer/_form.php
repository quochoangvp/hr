<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Employer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employer-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'coname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cologo')->fileInput() ?>

    <?= $form->field($model, 'codetails')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'coaddress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cowebsite')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cocontact')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Thêm' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Employer */

$this->title = 'Cập nhật thông tin nhà tuyển dụng: ' . $model->coname;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách nhà tuyển', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->coname, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="employer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

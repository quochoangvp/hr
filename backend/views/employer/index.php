<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách nhà tuyển dụng';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Thêm nhà tuyển dụng mới', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'coname',
            'email:email',
            // 'password',
            // 'cologo',
            // 'codetails:ntext',
            'coaddress',
            'cowebsite',
            'cocontact',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Employer */

$this->title = 'Thêm nhà tuyển dụng mới';
$this->params['breadcrumbs'][] = ['label' => 'Danh sách nhà tuyển dụng', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

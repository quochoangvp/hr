<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Employer */

$this->title = $model->coname;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách nhà tuyển dụng', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Html::img(Url::to('@images/uploads/' . $model->cologo), ['width' => '120px']) ?></p>

    <p>
        <?= Html::a('Cập nhật', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Bạn có chắc chắn muốn xóa?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'email:email',
            // 'password',
            'coname',
            // 'cologo',
            'codetails:ntext',
            'coaddress',
            'cowebsite',
            'cocontact',
        ],
    ]) ?>

</div>

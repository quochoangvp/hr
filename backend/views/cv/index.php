<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Employee;
use common\models\JobCate;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách đơn ứng tuyển';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cv-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Thêm mới đơn ứng tuyển', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'title',
            [
                'attribute' => 'employee_id',
                'value' => function($model)
                {
                    return Employee::find()->where(['id' => $model->employee_id])->one()->fullname;
                }
            ],
            [
                'attribute' => 'jobcat_id',
                'value' => function($model)
                {
                    return JobCate::find()->where(['id' => $model->jobcat_id])->one()->name;
                }
            ],
            // 'body:ntext',
            // 'created_at',
            // 'updated_at',
            'countview',
            'attachments',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

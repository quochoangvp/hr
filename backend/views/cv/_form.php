<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Employee;
use common\models\JobCate;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\models\Cv */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cv-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php
        echo $form->field($model, 'employee_id')->dropdownList(
            Employee::find()->select(['fullname', 'id'])->orderBy('fullname')->indexBy('id')->column(),
            ['prompt'=>'Chọn ứng viên']
        );
    ?>

    <?php
        echo $form->field($model, 'jobcat_id')->dropdownList(
            JobCate::find()->select(['name', 'id'])->orderBy('name')->indexBy('id')->column(),
            ['prompt'=>'Chọn ngành nghề']
        );
    ?>

    <?= $form->field($model, 'body')->widget(TinyMce::className(), [
        'options' => ['rows' => 20],
        'language' => 'vi',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <?= $form->field($model, 'attachments')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Thêm' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

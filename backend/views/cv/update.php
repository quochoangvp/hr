<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cv */

$this->title = 'Cập nhật đơn ứng tuyển: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách đơn ứng tuyển', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="cv-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

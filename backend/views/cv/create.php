<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Cv */

$this->title = 'Thêm đơn ứng tuyển mới';
$this->params['breadcrumbs'][] = ['label' => 'Danh sách đơn ứng tuyển', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cv-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

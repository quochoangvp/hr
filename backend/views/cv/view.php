<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Employee;
use common\models\JobCate;

/* @var $this yii\web\View */
/* @var $model common\models\Cv */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách đơn ứng tuyển', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cv-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Sửa', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Bạn có chắc chắn muốn?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'title',
            [
                'attribute' => 'employee_id',
                'value' => Employee::find()->where(['id' => $model->employee_id])->one()->fullname
            ],
            [
                'attribute' => 'jobcat_id',
                'value' => JobCate::find()->where(['id' => $model->jobcat_id])->one()->name
            ],
            [
                'attribute' => 'body',
                'value' => html_entity_decode(strip_tags($model->body))
            ],
            'created_at',
            'updated_at',
            'countview',
            'attachments',
        ],
    ]) ?>

</div>

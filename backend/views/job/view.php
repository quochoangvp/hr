<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\JobCate;
use common\models\Employer;

/* @var $this yii\web\View */
/* @var $model common\models\Job */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách tin tuyển dụng', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Sửa', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Bạn chắc chắn muốn xóa?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'title',
            [
                'attribute' => 'cate_id',
                'value' => JobCate::find()->where(['id' => $model->jobcat_id])->one()->name
            ],
            [
                'attribute' => 'employer_id',
                'value' => Employer::find()->where(['id' => $model->employer_id])->one()->coname
            ],
            'deadline',
            'created_at',
            'updated_at',
            'apply_num',
            [
                'attribute' => 'body',
                'value' => strip_tags(html_entity_decode($model->body))
            ],
        ],
    ]) ?>

</div>

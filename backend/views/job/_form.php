<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use common\models\Employer;
use common\models\JobCate;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Job */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php
        echo $form->field($model, 'employer_id')->dropdownList(
            Employer::find()->select(['coname', 'id'])->orderBy('coname')->indexBy('id')->column(),
            ['prompt'=>'Chọn công ty']
        );
    ?>

    <?php
        echo $form->field($model, 'jobcat_id')->dropdownList(
            JobCate::find()->select(['name', 'id'])->orderBy('name')->indexBy('id')->column(),
            ['prompt'=>'Chọn ngành nghề']
        );
    ?>

    <?= $form->field($model, 'deadline')->widget(DatePicker::classname(), [
        'language' => 'vi',
        'dateFormat' => 'yyyy-MM-dd',
        'options' => ['class' => 'form-control'],
    ]) ?>

    <?= $form->field($model, 'body')->widget(TinyMce::className(), [
        'options' => ['rows' => 20],
        'language' => 'vi',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Thêm' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Jobcate;
use common\models\Employer;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách việc làm';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Thêm việc làm mới', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'title',
            [
                'attribute' => 'employer_id',
                'value' => function($model)
                {
                    return Employer::find()->where(['id' => $model->employer_id])->one()->coname;
                }
            ],
            [
                'attribute' => 'jobcat_id',
                'value' => function($model)
                {
                    return Jobcate::find()->where(['id' => $model->jobcat_id])->one()->name;
                }
            ],
            'deadline',
            // 'created_at',
            // 'updated_at',
            'apply_num',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

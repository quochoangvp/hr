<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\JobCate */

$this->title = 'Cập nhật ngành nghề: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ngành nghề', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="job-cate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

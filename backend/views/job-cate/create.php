<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\JobCate */

$this->title = 'Thêm mới ngành nghề';
$this->params['breadcrumbs'][] = ['label' => 'Danh sách ngành nghề', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-cate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

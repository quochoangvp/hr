<?php

/* @var $this yii\web\View */

$this->title = 'Quản trị tìm việc làm';

$employers = common\models\Employer::find()->limit(10)->asArray()->all();
$employees = common\models\Employee::find()->limit(10)->asArray()->all();
$jobs = common\models\Job::find()->orderBy('created_at', 'SORT_ASC')->limit(10)->asArray()->all();
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Nhà tuyển dụng mới</h2>
                <ul class="list-group">
                    <?php foreach ($employers as $employer): ?>
                        <li class="list-group-item"><?= $employer['coname'] ?></li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="col-lg-4">
                <h2>Ứng viên mới</h2>
                <ul class="list-group">
                    <?php foreach ($employees as $employee): ?>
                        <li class="list-group-item"><?= $employee['fullname'] ?></li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="col-lg-4">
                <h2>Tin tuyển dụng mới</h2>
                <ul class="list-group">
                    <?php foreach ($jobs as $job): ?>
                        <li class="list-group-item"><?= $job['title'] ?></li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>

    </div>
</div>
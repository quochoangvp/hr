<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách ứng viên';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Thêm mới ứng viên', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'fullname',
            [
                'attribute' => 'gender',
                'value' => function($model)
                {
                    switch ($model->gender) {
                        case 1:
                            return 'Nam';
                            break;
                        case 2:
                            return 'Nữ';
                            break;
                        default:
                            return 'Khác';
                            break;
                    }
                }
            ],
            'birthday',
            'email:email',
            // 'password',
            // 'phone',
            // 'address',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

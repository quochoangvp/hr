<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Employee */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách ứng viên', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Cập nhật', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Bạn có chắc chắn muốn xóa?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'fullname',
            [
                'attribute' => 'gender',
                'value' => ($model->gender === 1) ? 'Nam' : (($model->gender === 2) ? 'Nữ' : 'Khác')
            ],
            'birthday',
            'email:email',
            'password',
            'phone',
            'address',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Vg */

$this->title = 'Chỉnh sửa tin hướng nghiệp: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách tin hướng nghệp', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="vg-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

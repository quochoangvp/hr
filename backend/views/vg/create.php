<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Vg */

$this->title = 'Đăng tin hướng nghiệp mới';
$this->params['breadcrumbs'][] = ['label' => 'Danh sách tin hướng nghiệp', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vg-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

// find menu-item associated with this page and make current:
$('#w1-collapse a').each(function(index, value) {
  if ($(this).prop('href') === window.location.href || $(this).prop('href') + '/index' === window.location.href) {
    $(this).parent().addClass('active');
  }
});